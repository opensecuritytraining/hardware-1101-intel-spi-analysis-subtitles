1
00:00:00,060 --> 00:00:06,299
so the raw data when it's exported from

2
00:00:03,060 --> 00:00:08,760
logic looks something like this

3
00:00:06,299 --> 00:00:10,200
so we have you know QSPI and then its

4
00:00:08,760 --> 00:00:11,580
command is on one line where it tells

5
00:00:10,200 --> 00:00:13,799
you what the command was and then the

6
00:00:11,580 --> 00:00:15,719
address is on the next line and so we

7
00:00:13,799 --> 00:00:18,480
need to do some processing of that in

8
00:00:15,719 --> 00:00:20,880
order to turn this read from address hex

9
00:00:18,480 --> 00:00:23,400
10 into something that can be graphed by

10
00:00:20,880 --> 00:00:27,119
a data visualization tool so to do that

11
00:00:23,400 --> 00:00:29,539
I've provided you a script so python

12
00:00:27,119 --> 00:00:29,539
script

13
00:00:29,760 --> 00:00:35,760
format select QSPI you're going to type

14
00:00:33,239 --> 00:00:37,739
deck for decimal output because we want

15
00:00:35,760 --> 00:00:39,420
our graphing program to understand that

16
00:00:37,739 --> 00:00:41,100
these are decimal numbers for the

17
00:00:39,420 --> 00:00:43,559
addresses instead of hexadecimal numbers

18
00:00:41,100 --> 00:00:44,520
then we're going to provide the input

19
00:00:43,559 --> 00:00:46,680
file

20
00:00:44,520 --> 00:00:48,300
and then we're going to provide an

21
00:00:46,680 --> 00:00:51,480
output file name

22
00:00:48,300 --> 00:00:54,059
so I'm going to call this processed

23
00:00:51,480 --> 00:00:55,920
version of the same file

24
00:00:54,059 --> 00:00:58,079
once it's done processing you should

25
00:00:55,920 --> 00:00:59,579
have something that looks like a simple

26
00:00:58,079 --> 00:01:03,719
series

27
00:00:59,579 --> 00:01:07,439
like this so a point and a value so the

28
00:01:03,719 --> 00:01:09,720
x-axis 0 should show that the y-axis is

29
00:01:07,439 --> 00:01:12,540
16 so it's reading from address 16 or

30
00:01:09,720 --> 00:01:14,820
hex 10. so then we can go ahead and

31
00:01:12,540 --> 00:01:18,000
visualize this to do that I've

32
00:01:14,820 --> 00:01:19,619
recommended you use ParaView inside ParaView

33
00:01:18,000 --> 00:01:21,780
you're going to go ahead and

34
00:01:19,619 --> 00:01:23,460
select open file and then you're going

35
00:01:21,780 --> 00:01:25,920
to navigate your way to the file which

36
00:01:23,460 --> 00:01:27,840
I'm going to cut out quickly once you've

37
00:01:25,920 --> 00:01:31,020
selected the file you'll be given a

38
00:01:27,840 --> 00:01:33,840
prompt to select CSV reader or gdel

39
00:01:31,020 --> 00:01:35,700
select CSV reader and hit OK then you

40
00:01:33,840 --> 00:01:38,939
need to create a view that's appropriate

41
00:01:35,700 --> 00:01:41,820
for just 2D scatter plot so go ahead and

42
00:01:38,939 --> 00:01:44,759
click this to close the default view and

43
00:01:41,820 --> 00:01:47,159
then select the point chart view from

44
00:01:44,759 --> 00:01:49,140
the available options

45
00:01:47,159 --> 00:01:52,020
now you're going to click the little

46
00:01:49,140 --> 00:01:54,540
eyeball to the side of your process data

47
00:01:52,020 --> 00:01:57,180
to make it visualized and there you go

48
00:01:54,540 --> 00:02:00,000
we can now see all of the accesses that

49
00:01:57,180 --> 00:02:02,100
were made to this by flash tube just to

50
00:02:00,000 --> 00:02:04,560
make a little bit of a difference this

51
00:02:02,100 --> 00:02:05,820
point right here is unnecessary we can

52
00:02:04,560 --> 00:02:07,439
get rid of that that's just the

53
00:02:05,820 --> 00:02:09,539
ascending order thing it's not needed

54
00:02:07,439 --> 00:02:11,099
for this particular graphing program now

55
00:02:09,539 --> 00:02:12,599
the thing that I like about peer review

56
00:02:11,099 --> 00:02:14,580
and the reason why I wanted you to use

57
00:02:12,599 --> 00:02:17,400
this is because it allows for easy

58
00:02:14,580 --> 00:02:19,739
zooming so for instance if I scroll up

59
00:02:17,400 --> 00:02:22,379
on the scroll wheel then I can zoom in

60
00:02:19,739 --> 00:02:24,959
on things but it needs to be sort of

61
00:02:22,379 --> 00:02:28,500
centered before you zoom so you can

62
00:02:24,959 --> 00:02:31,200
scroll to zoom if you hold control while

63
00:02:28,500 --> 00:02:34,500
moving the mouse you will expand the

64
00:02:31,200 --> 00:02:36,720
axes so you can move the x or the y axis

65
00:02:34,500 --> 00:02:39,599
to make it bigger move diagonal to make

66
00:02:36,720 --> 00:02:40,860
it all bigger but the thing that I you

67
00:02:39,599 --> 00:02:43,080
know really want to use is I want to

68
00:02:40,860 --> 00:02:45,060
zoom in on some particular area and to

69
00:02:43,080 --> 00:02:47,160
do that unfortunately you need a mouse

70
00:02:45,060 --> 00:02:49,980
with a middle button and you need to

71
00:02:47,160 --> 00:02:52,379
click the middle button and then drag to

72
00:02:49,980 --> 00:02:54,480
create a bounding box so if you want to

73
00:02:52,379 --> 00:02:56,459
zoom you click a middle button on a

74
00:02:54,480 --> 00:02:59,160
scroll wheel capable mouse or just a

75
00:02:56,459 --> 00:03:01,080
three button Mouse and then you can zoom

76
00:02:59,160 --> 00:03:02,760
in on a specific area

77
00:03:01,080 --> 00:03:04,680
and so that's kind of how you can

78
00:03:02,760 --> 00:03:08,459
navigate your way around you can reset

79
00:03:04,680 --> 00:03:13,620
this and you know see the access see the

80
00:03:08,459 --> 00:03:17,040
axis is not axes the axis is to the data

81
00:03:13,620 --> 00:03:20,120
from your particular SPI flash on your

82
00:03:17,040 --> 00:03:20,120
particular Intel system

