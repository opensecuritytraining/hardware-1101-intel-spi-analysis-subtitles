1
00:00:00,000 --> 00:00:04,620
okay so here's how I've connected my

2
00:00:02,520 --> 00:00:06,600
adapter you can see that I plugged stuff

3
00:00:04,620 --> 00:00:08,460
into the breadboard and what I tried to

4
00:00:06,600 --> 00:00:10,800
do was make a rough correspondence

5
00:00:08,460 --> 00:00:12,960
between the pins the way they would be

6
00:00:10,800 --> 00:00:15,000
oriented on the clip and how they're

7
00:00:12,960 --> 00:00:16,320
oriented on the breadboard and also I

8
00:00:15,000 --> 00:00:18,420
tried to make sure the colors are the

9
00:00:16,320 --> 00:00:20,880
same so again you know orange to orange

10
00:00:18,420 --> 00:00:22,680
green to green purple to purple and then

11
00:00:20,880 --> 00:00:23,580
I'm using black instead of white for the

12
00:00:22,680 --> 00:00:25,800
ground

13
00:00:23,580 --> 00:00:28,859
so you can see here that I put them on

14
00:00:25,800 --> 00:00:30,779
the second to Outer row or column

15
00:00:28,859 --> 00:00:33,239
depending on your perspective of the

16
00:00:30,779 --> 00:00:35,880
breadboard and the reason for that is

17
00:00:33,239 --> 00:00:39,360
because the Dediprog has a nice little

18
00:00:35,880 --> 00:00:41,879
adapter like this that it basically

19
00:00:39,360 --> 00:00:44,760
plugs right into a breadboard so this

20
00:00:41,879 --> 00:00:46,260
particular adapter is meant for back in

21
00:00:44,760 --> 00:00:49,379
the day when you would have these sort

22
00:00:46,260 --> 00:00:51,600
of SPI flash chips that you know had the

23
00:00:49,379 --> 00:00:54,239
pins through the holes or which would

24
00:00:51,600 --> 00:00:55,920
plug into a socketed connector and so

25
00:00:54,239 --> 00:00:57,719
this would then plug into the same

26
00:00:55,920 --> 00:00:59,640
socketed connector so I actually have a

27
00:00:57,719 --> 00:01:01,440
machine off to the side here that does

28
00:00:59,640 --> 00:01:03,780
take this sort of socketed connector and

29
00:01:01,440 --> 00:01:04,619
so this can be useful for plugging into

30
00:01:03,780 --> 00:01:06,900
that

31
00:01:04,619 --> 00:01:08,760
so what I can do with this adapter is I

32
00:01:06,900 --> 00:01:11,400
can just go ahead and plug it into my

33
00:01:08,760 --> 00:01:13,380
Dediprog as normal and then I can plug

34
00:01:11,400 --> 00:01:15,840
it into the breadboard making sure that

35
00:01:13,380 --> 00:01:17,460
I have the orientation correct now

36
00:01:15,840 --> 00:01:19,979
thankfully Dediprog is using

37
00:01:17,460 --> 00:01:22,500
consistent coloring so if I look at this

38
00:01:19,979 --> 00:01:25,500
I can see that this one right here is

39
00:01:22,500 --> 00:01:28,920
white and on my original chip clip white

40
00:01:25,500 --> 00:01:30,840
was the ground pin so I'm just going to

41
00:01:28,920 --> 00:01:32,159
go ahead and plug that in in this

42
00:01:30,840 --> 00:01:34,799
orientation

43
00:01:32,159 --> 00:01:36,900
and it slots in there nicely and there

44
00:01:34,799 --> 00:01:39,060
we go now the Dediprog is correctly

45
00:01:36,900 --> 00:01:41,400
oriented so that it's essentially just a

46
00:01:39,060 --> 00:01:44,759
pass-through to this now at this point

47
00:01:41,400 --> 00:01:47,700
it would be prudent to take this and

48
00:01:44,759 --> 00:01:50,880
connect it to my SPI flash chip making

49
00:01:47,700 --> 00:01:54,000
sure to set the pin one where the dot is

50
00:01:50,880 --> 00:01:55,860
so connect it in that orientation and

51
00:01:54,000 --> 00:01:57,780
then I would run the Dediprog in order

52
00:01:55,860 --> 00:02:00,180
to make sure that everything is

53
00:01:57,780 --> 00:02:01,740
connected correctly and that I can

54
00:02:00,180 --> 00:02:03,299
correctly identify the chip which

55
00:02:01,740 --> 00:02:06,719
actually I can see that my plug

56
00:02:03,299 --> 00:02:09,420
unplugged off camera here so there we go

57
00:02:06,719 --> 00:02:11,459
and so now I should run Dediprog to

58
00:02:09,420 --> 00:02:13,260
make sure that it's all working

59
00:02:11,459 --> 00:02:15,660
now if you don't have this sort of

60
00:02:13,260 --> 00:02:17,760
adapter then you're going to have to do

61
00:02:15,660 --> 00:02:20,400
things the slightly harder way but not

62
00:02:17,760 --> 00:02:22,800
that hard so you're just going to again

63
00:02:20,400 --> 00:02:25,440
use adapting wires

64
00:02:22,800 --> 00:02:28,020
and you know for instance you can use

65
00:02:25,440 --> 00:02:31,800
the same Convention of black to ground

66
00:02:28,020 --> 00:02:35,760
so I can go one two three four five down

67
00:02:31,800 --> 00:02:38,160
one two three four five down

68
00:02:35,760 --> 00:02:40,560
plug this in there and then plug this

69
00:02:38,160 --> 00:02:41,879
into ground on my

70
00:02:40,560 --> 00:02:44,340
breadboard

71
00:02:41,879 --> 00:02:46,800
and same thing you know power and so

72
00:02:44,340 --> 00:02:49,319
forth so basically you would just you

73
00:02:46,800 --> 00:02:52,319
know take your adapter wires and connect

74
00:02:49,319 --> 00:02:54,540
everything up to the correct pin on the

75
00:02:52,319 --> 00:02:55,860
breadboard and then you know later on

76
00:02:54,540 --> 00:02:57,239
you're going to do the same thing with

77
00:02:55,860 --> 00:02:59,840
the Saleae connecting it to the

78
00:02:57,239 --> 00:02:59,840
appropriate thing

