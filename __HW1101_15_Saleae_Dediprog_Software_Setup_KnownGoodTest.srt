1
00:00:00,120 --> 00:00:04,860
so now you should have your logic

2
00:00:02,159 --> 00:00:06,720
analyzer open, your Saleae, and you can

3
00:00:04,860 --> 00:00:09,000
organize your settings however you want

4
00:00:06,720 --> 00:00:11,580
but as usual I have my chip select at

5
00:00:09,000 --> 00:00:14,519
the bottom my clock and then data quad 0

6
00:00:11,580 --> 00:00:17,039
1 2 3 so that I can read like that and

7
00:00:14,519 --> 00:00:18,900
then this time I have the VCC as well

8
00:00:17,039 --> 00:00:21,000
the power so that I can watch the Power

9
00:00:18,900 --> 00:00:23,100
for reasons that we'll see shortly

10
00:00:21,000 --> 00:00:25,140
okay so now we're going to start the

11
00:00:23,100 --> 00:00:27,720
logic analyzer and then we start the

12
00:00:25,140 --> 00:00:30,359
Dediprog so that we can see how it's

13
00:00:27,720 --> 00:00:32,880
going to actually identify the chip so

14
00:00:30,359 --> 00:00:34,980
it successfully seemed to find something

15
00:00:32,880 --> 00:00:37,500
and we know that it's this particular

16
00:00:34,980 --> 00:00:38,880
chip based on the physical markings so

17
00:00:37,500 --> 00:00:41,219
it's saying that there's ambiguity

18
00:00:38,880 --> 00:00:43,200
between all of these and it can't tell

19
00:00:41,219 --> 00:00:45,360
which of these it is because they all

20
00:00:43,200 --> 00:00:47,940
sort of identify the same for purposes

21
00:00:45,360 --> 00:00:49,440
of the identification that does so we're

22
00:00:47,940 --> 00:00:51,539
going to select the correct one and then

23
00:00:49,440 --> 00:00:52,440
let's go back and see how this actually

24
00:00:51,539 --> 00:00:55,020
work

25
00:00:52,440 --> 00:00:57,180
if we look at the overview and we're

26
00:00:55,020 --> 00:00:59,879
going to see just a few SPI transactions

27
00:00:57,180 --> 00:01:02,699
right here and go ahead and slice this

28
00:00:59,879 --> 00:01:05,159
down delete the data before the marker

29
00:01:02,699 --> 00:01:07,580
I'm going to delete the data after the

30
00:01:05,159 --> 00:01:07,580
marker

31
00:01:09,720 --> 00:01:14,700
all right and then this is what we see

32
00:01:12,060 --> 00:01:16,740
now I'm going to shortcut over here if I

33
00:01:14,700 --> 00:01:19,439
double click on this it'll zoom in

34
00:01:16,740 --> 00:01:21,900
directly on that middle thing here

35
00:01:19,439 --> 00:01:24,600
so I'm going to also so I've set up my Q

36
00:01:21,900 --> 00:01:25,560
 SPI analyzer so you can do the same if

37
00:01:24,600 --> 00:01:27,900
you'd like

38
00:01:25,560 --> 00:01:31,619
so as usual the enable is the chip

39
00:01:27,900 --> 00:01:35,040
select clock MOSI MISO write protect and

40
00:01:31,619 --> 00:01:37,320
hold for dq0123 I'm going to have it in

41
00:01:35,040 --> 00:01:39,240
extended mode this particular one I set

42
00:01:37,320 --> 00:01:41,460
it for eight dummy clock cycles it

43
00:01:39,240 --> 00:01:43,460
doesn't matter this is not having any

44
00:01:41,460 --> 00:01:45,600
dummy clock cycles in these particular

45
00:01:43,460 --> 00:01:48,600
sequence of commands that are being sent

46
00:01:45,600 --> 00:01:50,220
and so save that now I'm going to

47
00:01:48,600 --> 00:01:52,380
restart this I'm going to right click

48
00:01:50,220 --> 00:01:54,360
and restart so that it properly analyzes

49
00:01:52,380 --> 00:01:56,820
my data because I've done other

50
00:01:54,360 --> 00:01:58,799
sequences before this so it tells me

51
00:01:56,820 --> 00:02:01,860
that the First Command that is sent is

52
00:01:58,799 --> 00:02:03,600
9f which is called read ID so to figure

53
00:02:01,860 --> 00:02:06,299
out what that is we would of course have

54
00:02:03,600 --> 00:02:09,380
to read the fund manual and so this is

55
00:02:06,299 --> 00:02:12,480
the data sheet for that it is a

56
00:02:09,380 --> 00:02:15,180
w25q16dv and we search the datasheet for

57
00:02:12,480 --> 00:02:18,480
9f and we'll see that it says that this

58
00:02:15,180 --> 00:02:22,140
is how it works you send a 9f on the

59
00:02:18,480 --> 00:02:24,540
data in the IO0 and you expect back a

60
00:02:22,140 --> 00:02:27,300
manufacturer ID which for Winbond is

61
00:02:24,540 --> 00:02:30,840
always going to be EF and then some sort

62
00:02:27,300 --> 00:02:32,459
of memory type ID and capacity ID

63
00:02:30,840 --> 00:02:34,620
so if we wanted to know what

64
00:02:32,459 --> 00:02:37,860
specifically those values are we could

65
00:02:34,620 --> 00:02:40,440
search for 9f and then we'll find a

66
00:02:37,860 --> 00:02:43,500
table that has those for us

67
00:02:40,440 --> 00:02:45,140
so this is the table it says 9f for this

68
00:02:43,500 --> 00:02:50,040
chip

69
00:02:45,140 --> 00:02:54,060
w25q16dv should return 40 15.

70
00:02:50,040 --> 00:02:56,700
okay so we expect that we should see EF

71
00:02:54,060 --> 00:02:59,040
immediately followed by hex 40 and hex

72
00:02:56,700 --> 00:03:03,239
15. so do we see that

73
00:02:59,040 --> 00:03:07,260
let's go ahead and here's the data out 0

74
00:03:03,239 --> 00:03:10,980
or data quad 1. so let's go look at that

75
00:03:07,260 --> 00:03:13,140
and here we do indeed see EF but we

76
00:03:10,980 --> 00:03:16,379
expect three bytes not one so what's

77
00:03:13,140 --> 00:03:19,019
going on well here we actually see that

78
00:03:16,379 --> 00:03:21,420
the Dediprog has taken the chip select

79
00:03:19,019 --> 00:03:24,659
and moved it high so essentially it has

80
00:03:21,420 --> 00:03:27,300
terminated this three byte expected

81
00:03:24,659 --> 00:03:28,980
return after only a single byte my

82
00:03:27,300 --> 00:03:30,900
expectation is that Dediprog is

83
00:03:28,980 --> 00:03:32,760
probably doing this because all the

84
00:03:30,900 --> 00:03:34,440
various SPI flash chips behave slightly

85
00:03:32,760 --> 00:03:36,840
differently so it's probably first

86
00:03:34,440 --> 00:03:39,300
wanting to get the manufacturer ID EF

87
00:03:36,840 --> 00:03:41,580
for Winbond before it does some other

88
00:03:39,300 --> 00:03:44,040
particular logic that is perhaps wind

89
00:03:41,580 --> 00:03:46,019
Bond specific or for other vendors they

90
00:03:44,040 --> 00:03:49,799
maybe need to do something specific

91
00:03:46,019 --> 00:03:51,120
so after this EF is sent out then what

92
00:03:49,799 --> 00:03:53,519
happens next

93
00:03:51,120 --> 00:03:55,680
well then the Dediprog sends a 9f

94
00:03:53,519 --> 00:03:58,440
again and let's see the output of that

95
00:03:55,680 --> 00:04:00,480
okay this time that Dediprog let the

96
00:03:58,440 --> 00:04:02,819
chip select stay low long enough that we

97
00:04:00,480 --> 00:04:06,239
could get all three bytes so we have EF

98
00:04:02,819 --> 00:04:08,040
x40 hex 15 and that is exactly what we

99
00:04:06,239 --> 00:04:10,379
would expect to see based on the data

100
00:04:08,040 --> 00:04:13,799
sheet telling us we should see EF and

101
00:04:10,379 --> 00:04:16,919
then 1 by 2 bytes which elsewhere in the

102
00:04:13,799 --> 00:04:19,739
data sheet it says should be 40 and 15.

103
00:04:16,919 --> 00:04:21,959
so this is what the Dediprog does

104
00:04:19,739 --> 00:04:23,400
normally when you open it up and it's

105
00:04:21,959 --> 00:04:25,320
saying you know okay I'm trying to

106
00:04:23,400 --> 00:04:27,840
identify but then it says you know

107
00:04:25,320 --> 00:04:29,580
something is ambiguous that means that

108
00:04:27,840 --> 00:04:32,699
there's a bunch of other Winbond

109
00:04:29,580 --> 00:04:35,280
devices where they're also going to

110
00:04:32,699 --> 00:04:37,259
return 40 15. so we basically expect

111
00:04:35,280 --> 00:04:40,979
that all these sort of Winbond devices

112
00:04:37,259 --> 00:04:44,940
are all going to always return for 9f EF

113
00:04:40,979 --> 00:04:48,000
4015 so that is why the human then has

114
00:04:44,940 --> 00:04:49,620
to select and make it unambiguous once

115
00:04:48,000 --> 00:04:51,780
you select the device then it you know

116
00:04:49,620 --> 00:04:53,520
says that it's choosing the voltage to

117
00:04:51,780 --> 00:04:54,900
apply because it knows okay this

118
00:04:53,520 --> 00:04:58,139
particular Winbond that you've told me

119
00:04:54,900 --> 00:05:00,000
to select is going to run at 1.8 volts

120
00:04:58,139 --> 00:05:02,340
and actually strike what I just said

121
00:05:00,000 --> 00:05:04,259
that is a little bit misleading it looks

122
00:05:02,340 --> 00:05:07,139
like it's applying 1.8 volts but down

123
00:05:04,259 --> 00:05:09,660
here it says that the chip VCC is

124
00:05:07,139 --> 00:05:11,639
actually 3.8 volts and that's more

125
00:05:09,660 --> 00:05:13,740
accurate and more correct because if we

126
00:05:11,639 --> 00:05:16,680
look at the data sheet we will see that

127
00:05:13,740 --> 00:05:18,720
it says 3 volt and if we scroll down a

128
00:05:16,680 --> 00:05:21,120
little bit we'll see that the nominal

129
00:05:18,720 --> 00:05:23,580
voltage range that it can go between is

130
00:05:21,120 --> 00:05:27,300
2.7 to 3.6

131
00:05:23,580 --> 00:05:29,820
so here it says single 2.7 to 3.6 volt

132
00:05:27,300 --> 00:05:31,620
Supply so that is why the deadly prog

133
00:05:29,820 --> 00:05:33,960
actually is saying you know it's 3.3

134
00:05:31,620 --> 00:05:36,380
volts here I don't know why it says 1.8

135
00:05:33,960 --> 00:05:36,380
right there

136
00:05:44,220 --> 00:05:48,180
anyways let's move on to seeing what the

137
00:05:46,560 --> 00:05:50,100
behavior is on a chip where this

138
00:05:48,180 --> 00:05:51,720
detection failed actually before that

139
00:05:50,100 --> 00:05:53,639
let's just you know confirm for our own

140
00:05:51,720 --> 00:05:55,740
purposes that we can actually not just

141
00:05:53,639 --> 00:05:57,539
identify but read out the contents of

142
00:05:55,740 --> 00:05:59,699
 the flash chip and indeed these are the

143
00:05:57,539 --> 00:06:01,860
contents and based on this you know

144
00:05:59,699 --> 00:06:03,000
there's not the special magic signature

145
00:06:01,860 --> 00:06:05,340
I would expect to see at the beginning

146
00:06:03,000 --> 00:06:07,979
to say this is a Intel SPI flash

147
00:06:05,340 --> 00:06:10,860
descriptor interpretation but if I look

148
00:06:07,979 --> 00:06:15,060
all the way at the end I do see that I

149
00:06:10,860 --> 00:06:18,300
have a 90 90 E9 9B and I recognize this

150
00:06:15,060 --> 00:06:20,520
as x86 assembly for no-op no-op and jump

151
00:06:18,300 --> 00:06:22,740
some negative distance backwards so this

152
00:06:20,520 --> 00:06:25,199
is the typical reset vector that you

153
00:06:22,740 --> 00:06:27,360
would see from architecture 4001 class

154
00:06:25,199 --> 00:06:29,100
if we didn't see that then you know we

155
00:06:27,360 --> 00:06:31,199
might think well maybe this is just some

156
00:06:29,100 --> 00:06:34,080
other two megabit flash chip that's

157
00:06:31,199 --> 00:06:35,400
sitting off to the side that has some

158
00:06:34,080 --> 00:06:37,139
unknown thing and then we'd have to go

159
00:06:35,400 --> 00:06:38,759
do searching through strings or

160
00:06:37,139 --> 00:06:40,979
something to find out what exactly it's

161
00:06:38,759 --> 00:06:43,199
used for but no based on the fact that

162
00:06:40,979 --> 00:06:45,180
we see the reset Vector down here we can

163
00:06:43,199 --> 00:06:47,819
reasonably conclude that this particular

164
00:06:45,180 --> 00:06:49,680
system has an orientation where the

165
00:06:47,819 --> 00:06:52,560
eight megabyte chip that we can't read

166
00:06:49,680 --> 00:06:55,080
is used for the reset vector or sorry

167
00:06:52,560 --> 00:06:58,259
for the bottom eight megabits megabytes

168
00:06:55,080 --> 00:07:00,180
rather and then the two megabyte chip

169
00:06:58,259 --> 00:07:02,340
that we can read right now is probably

170
00:07:00,180 --> 00:07:06,060
the top two megabytes so it's probably

171
00:07:02,340 --> 00:07:07,979
requires a 10 megabyte worth of BIOS and

172
00:07:06,060 --> 00:07:10,020
it's split up between two chips

173
00:07:07,979 --> 00:07:11,819
okay so now let's go see what happens

174
00:07:10,020 --> 00:07:14,000
with that other chip when we try to read

175
00:07:11,819 --> 00:07:14,000
it

