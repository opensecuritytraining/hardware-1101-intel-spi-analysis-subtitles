1
00:00:00,000 --> 00:00:04,620
okay so the first thing we're going to

2
00:00:01,620 --> 00:00:06,420
do is we're going to map these pins to

3
00:00:04,620 --> 00:00:10,080
this connector so I'm going to start

4
00:00:06,420 --> 00:00:12,840
with ground and ground is found one two

5
00:00:10,080 --> 00:00:14,639
three four five down and when it's

6
00:00:12,840 --> 00:00:16,859
plugged in in this orientation it would

7
00:00:14,639 --> 00:00:18,300
be on this side so when we flip it like

8
00:00:16,859 --> 00:00:21,900
this it's going to be on the opposite

9
00:00:18,300 --> 00:00:24,539
side so we do one two three four five

10
00:00:21,900 --> 00:00:27,960
down and that's going to be around

11
00:00:24,539 --> 00:00:31,859
all right then we can do power VCC

12
00:00:27,960 --> 00:00:34,500
and so that is found one two down and

13
00:00:31,859 --> 00:00:38,160
again we have to flip the sides so this

14
00:00:34,500 --> 00:00:40,260
would be one two down here and that's

15
00:00:38,160 --> 00:00:42,420
going to be our power

16
00:00:40,260 --> 00:00:45,059
all right then for the other colors it's

17
00:00:42,420 --> 00:00:47,579
basically arbitrary I chose to make them

18
00:00:45,059 --> 00:00:50,940
roughly the same colors as I found on my

19
00:00:47,579 --> 00:00:53,520
clip so just go down the line chip

20
00:00:50,940 --> 00:00:56,340
select on this side is going to be

21
00:00:53,520 --> 00:00:58,680
orange because I can see from my clip

22
00:00:56,340 --> 00:01:01,140
that it's using orange over here this

23
00:00:58,680 --> 00:01:03,899
pin one right there corresponds to this

24
00:01:01,140 --> 00:01:06,540
orange so I go ahead and put that in on

25
00:01:03,899 --> 00:01:08,820
the opposite side from VCC

26
00:01:06,540 --> 00:01:10,619
so that's going to be orange and then

27
00:01:08,820 --> 00:01:13,140
the next color down that I have on my

28
00:01:10,619 --> 00:01:14,100
particular chip is MISO and that was

29
00:01:13,140 --> 00:01:17,299
Green

30
00:01:14,100 --> 00:01:17,299
so I put that in

31
00:01:18,420 --> 00:01:23,220
and then the next one down from that is

32
00:01:20,880 --> 00:01:25,200
right protector data quad 2 which on my

33
00:01:23,220 --> 00:01:28,680
clip is sort of purplish

34
00:01:25,200 --> 00:01:31,680
so I put that in as well

35
00:01:28,680 --> 00:01:34,860
and now I just have the other side to do

36
00:01:31,680 --> 00:01:36,060
so we've got hold clock and MOSI on the

37
00:01:34,860 --> 00:01:37,860
other side

38
00:01:36,060 --> 00:01:41,640
so again I just kind of went roughly

39
00:01:37,860 --> 00:01:43,680
with what's here so my hold is going to

40
00:01:41,640 --> 00:01:46,939
be blue

41
00:01:43,680 --> 00:01:46,939
to put glue in

42
00:01:49,020 --> 00:01:54,240
my clock was sort of a grayish so I'm

43
00:01:51,899 --> 00:01:58,500
going to put that in

44
00:01:54,240 --> 00:02:00,180
and then finally the write protect well

45
00:01:58,500 --> 00:02:01,920
the ground was set to White on this

46
00:02:00,180 --> 00:02:03,659
thing but I already am using black for

47
00:02:01,920 --> 00:02:06,360
ground so I'm just going to use white

48
00:02:03,659 --> 00:02:08,280
for write protect

49
00:02:06,360 --> 00:02:10,800
or sorry I'm on the other side not clock

50
00:02:08,280 --> 00:02:13,020
uh the MOSI so the MOSI is going to be

51
00:02:10,800 --> 00:02:16,260
white on this all right so now I've got

52
00:02:13,020 --> 00:02:19,319
my little adapter set and I can use this

53
00:02:16,260 --> 00:02:20,640
and plug it into the breadboard so now

54
00:02:19,319 --> 00:02:22,319
I'm going to you know select some

55
00:02:20,640 --> 00:02:25,319
locations plug it into the breadboard

56
00:02:22,319 --> 00:02:26,760
and based on that I'm then going to sort

57
00:02:25,319 --> 00:02:28,319
of arrange it in the same arrangement

58
00:02:26,760 --> 00:02:29,819
that you would see on a SPI flash chip

59
00:02:28,319 --> 00:02:31,860
so that we have all the pins in the

60
00:02:29,819 --> 00:02:33,660
right order so that it makes sense and

61
00:02:31,860 --> 00:02:38,060
you can then connect it back to the

62
00:02:33,660 --> 00:02:38,060
Dediprog as well as the logic analyzer

