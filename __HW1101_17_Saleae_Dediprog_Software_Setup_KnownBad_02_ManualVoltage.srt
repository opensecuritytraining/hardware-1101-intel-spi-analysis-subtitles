1
00:00:00,000 --> 00:00:04,620
so to test my theory here's what I want

2
00:00:01,920 --> 00:00:07,500
to do I want to actually use a desktop

3
00:00:04,620 --> 00:00:10,440
power supply which you can see here and

4
00:00:07,500 --> 00:00:12,240
I'm going to use this to provide 3.3

5
00:00:10,440 --> 00:00:14,940
volts that's in the middle of the range

6
00:00:12,240 --> 00:00:17,340
that it expects and I'm limiting it to

7
00:00:14,940 --> 00:00:19,740
300 milliamps and so I'm going to

8
00:00:17,340 --> 00:00:22,859
provide this directly to the system

9
00:00:19,740 --> 00:00:25,019
without the Dediprog even attached and

10
00:00:22,859 --> 00:00:27,000
I want to see with the logic analyzer is

11
00:00:25,019 --> 00:00:29,279
that going to cause the system to

12
00:00:27,000 --> 00:00:32,099
actually power up so I want to go ahead

13
00:00:29,279 --> 00:00:34,620
and I just disconnect my Dediprog I'm

14
00:00:32,099 --> 00:00:36,719
going to overly disconnect it there but

15
00:00:34,620 --> 00:00:37,860
then I need to connect my power to my

16
00:00:36,719 --> 00:00:42,420
power

17
00:00:37,860 --> 00:00:47,480
so I'll put this right here

18
00:00:42,420 --> 00:00:47,480
power to power and ground to ground

19
00:00:47,700 --> 00:00:52,200
and now I've successfully connected

20
00:00:49,500 --> 00:00:54,420
power and ground and I want to see on

21
00:00:52,200 --> 00:00:56,399
the logic analyzer you know what do I

22
00:00:54,420 --> 00:00:59,640
see if I just go ahead and turn the

23
00:00:56,399 --> 00:01:00,600
power on and there's no Dediprog there

24
00:00:59,640 --> 00:01:02,760
whatsoever

25
00:01:00,600 --> 00:01:05,899
so I'll go ahead and hit start now I'm

26
00:01:02,760 --> 00:01:05,899
pressing the power button

27
00:01:07,799 --> 00:01:12,420
and what I see is a whole bunch of

28
00:01:10,560 --> 00:01:15,780
traffic on the system

29
00:01:12,420 --> 00:01:17,040
so that ultimately looks like normal SPI

30
00:01:15,780 --> 00:01:18,900
reading to me

31
00:01:17,040 --> 00:01:21,560
 SPI accesses rather that you would see

32
00:01:18,900 --> 00:01:21,560
at boot time

33
00:01:24,479 --> 00:01:28,320
so let's go ahead and dig into that and

34
00:01:26,820 --> 00:01:30,360
see whether we can actually you know

35
00:01:28,320 --> 00:01:32,100
confirm or refute that

36
00:01:30,360 --> 00:01:33,900
well we can see that there's proper

37
00:01:32,100 --> 00:01:36,720
parsing going on over here and what we

38
00:01:33,900 --> 00:01:39,360
see is a dual output fast read so let's

39
00:01:36,720 --> 00:01:42,180
go ahead and jump to that

40
00:01:39,360 --> 00:01:45,420
okay so here we have our 3B which is the

41
00:01:42,180 --> 00:01:48,840
Dual output fast read and so then we've

42
00:01:45,420 --> 00:01:50,579
got next up the address three bytes all

43
00:01:48,840 --> 00:01:53,159
zero so it's basically saying it's

44
00:01:50,579 --> 00:01:55,020
reading from address zero now we want to

45
00:01:53,159 --> 00:01:57,420
move on to see the data we've got a few

46
00:01:55,020 --> 00:01:59,280
dummy cycles on this sort of command and

47
00:01:57,420 --> 00:02:00,840
so you know I would have to go back to

48
00:01:59,280 --> 00:02:02,460
the data sheet to make sure I'm putting

49
00:02:00,840 --> 00:02:04,560
in the right number of dummy cycles but

50
00:02:02,460 --> 00:02:05,640
I know from looking it up it is 8 for

51
00:02:04,560 --> 00:02:07,560
this

52
00:02:05,640 --> 00:02:12,300
all right and then now I see the data

53
00:02:07,560 --> 00:02:14,400
and what I'm seeing is ffff and so forth

54
00:02:12,300 --> 00:02:16,140
and ultimately what makes me know that

55
00:02:14,400 --> 00:02:18,420
I'm looking at something that's valid is

56
00:02:16,140 --> 00:02:22,640
because then eventually I see the magic

57
00:02:18,420 --> 00:02:26,099
 SPI flash descriptor signature 5A A5 F0

58
00:02:22,640 --> 00:02:28,620
and this should be zero f

59
00:02:26,099 --> 00:02:30,360
but I think there's some parsing error

60
00:02:28,620 --> 00:02:33,540
going on right here it's probably a

61
00:02:30,360 --> 00:02:35,340
spurious spurious transaction uh yeah

62
00:02:33,540 --> 00:02:37,379
there we go we've got a missing clock

63
00:02:35,340 --> 00:02:39,599
cycle here so the logic analyzer Mystic

64
00:02:37,379 --> 00:02:41,940
clock cycle and that's leading to

65
00:02:39,599 --> 00:02:44,580
incorrect data interpretation but that's

66
00:02:41,940 --> 00:02:47,280
fine the main point here is that you

67
00:02:44,580 --> 00:02:49,379
know we are seeing expected data that

68
00:02:47,280 --> 00:02:52,500
would be appropriate for being at

69
00:02:49,379 --> 00:02:54,660
address 0 on a Intel SPI flash chip 16

70
00:02:52,500 --> 00:02:57,000
bytes of F's followed by the magic

71
00:02:54,660 --> 00:02:59,819
signature at offset 16.

72
00:02:57,000 --> 00:03:01,860
so cool that's you know good in some

73
00:02:59,819 --> 00:03:04,860
sense but it's bad in another sense

74
00:03:01,860 --> 00:03:06,959
right it's bad in the sense that we now

75
00:03:04,860 --> 00:03:10,080
know that the Dediprog providing power

76
00:03:06,959 --> 00:03:13,379
to the system seems to be causing the

77
00:03:10,080 --> 00:03:15,360
PCH and or CPU to power up and start

78
00:03:13,379 --> 00:03:18,120
reading from the SPI flash to do its

79
00:03:15,360 --> 00:03:20,340
normal boot the normal BIOS type boot of

80
00:03:18,120 --> 00:03:22,319
the system so that's obviously a problem

81
00:03:20,340 --> 00:03:23,819
for us because we just want to read this

82
00:03:22,319 --> 00:03:26,159
by flash chip we don't want to be

83
00:03:23,819 --> 00:03:27,780
causing the system to boot up and you

84
00:03:26,159 --> 00:03:29,879
know give us all of this you know

85
00:03:27,780 --> 00:03:31,140
conflicting data and causing trouble so

86
00:03:29,879 --> 00:03:34,019
my first thought when I was looking at

87
00:03:31,140 --> 00:03:35,940
this was well okay why don't I let the

88
00:03:34,019 --> 00:03:37,800
system get to steady state where it's

89
00:03:35,940 --> 00:03:39,720
just doing nothing and the system is

90
00:03:37,800 --> 00:03:41,940
booted and you know maybe nothing's try

91
00:03:39,720 --> 00:03:44,099
to read UEFI variables or anything like

92
00:03:41,940 --> 00:03:45,959
that if I just let it get to steady

93
00:03:44,099 --> 00:03:48,540
state then I've got you know voltage

94
00:03:45,959 --> 00:03:50,220
I've got the chip select low and so it

95
00:03:48,540 --> 00:03:52,980
seems like I should be able to use the

96
00:03:50,220 --> 00:03:55,739
Dediprog to actually you know do the

97
00:03:52,980 --> 00:03:57,959
the read at that point well let's see

98
00:03:55,739 --> 00:03:59,879
what the result of that is

99
00:03:57,959 --> 00:04:01,500
so then I need to make sure that my

100
00:03:59,879 --> 00:04:03,420
Dediprog is actually connected I

101
00:04:01,500 --> 00:04:05,700
accidentally unplugged it before so I

102
00:04:03,420 --> 00:04:08,340
make sure it's connected again I start

103
00:04:05,700 --> 00:04:10,920
the logic analyzer I turn on the power

104
00:04:08,340 --> 00:04:13,379
supply and then I wait for it to be done

105
00:04:10,920 --> 00:04:15,540
reading data off the SPI flash chip that

106
00:04:13,379 --> 00:04:17,940
looks like it's done to me then I start

107
00:04:15,540 --> 00:04:20,579
the Dediprog it attempts the

108
00:04:17,940 --> 00:04:22,260
identification but unfortunately it is

109
00:04:20,579 --> 00:04:24,660
going to fail again

110
00:04:22,260 --> 00:04:26,580
and we'll see what we actually see from

111
00:04:24,660 --> 00:04:29,000
the Dediprog here in a second after it

112
00:04:26,580 --> 00:04:29,000
fails

113
00:04:29,220 --> 00:04:35,160
okay so we can clearly see this sort of

114
00:04:33,060 --> 00:04:36,840
legitimate looking thing where we have

115
00:04:35,160 --> 00:04:39,419
some initial read followed by a whole

116
00:04:36,840 --> 00:04:41,340
bunch of reads and then we have again

117
00:04:39,419 --> 00:04:42,900
sort of garbage looking stuff where

118
00:04:41,340 --> 00:04:44,940
there's I mean you know it's not

119
00:04:42,900 --> 00:04:47,340
necessarily garbage but we have to you

120
00:04:44,940 --> 00:04:49,380
know zoom in on something to find you

121
00:04:47,340 --> 00:04:52,080
know is this actually legit well we've

122
00:04:49,380 --> 00:04:54,720
got you know some reasonable uh we've

123
00:04:52,080 --> 00:04:57,300
got some reasonable Clock movement here

124
00:04:54,720 --> 00:05:00,180
but we've just got zero data there

125
00:04:57,300 --> 00:05:02,100
so I'm going to add back in my data

126
00:05:00,180 --> 00:05:04,919
analyzer I removed it for a second

127
00:05:02,100 --> 00:05:08,720
because I thought there was an error so

128
00:05:04,919 --> 00:05:08,720
putting the QSPI back in

129
00:05:09,720 --> 00:05:14,820
telling it to analyze so you can see

130
00:05:12,540 --> 00:05:17,340
here that it's saying yes it sees an INF

131
00:05:14,820 --> 00:05:19,080
but the data immediately after that is

132
00:05:17,340 --> 00:05:20,280
zeros and that's not what we're looking

133
00:05:19,080 --> 00:05:23,400
for

134
00:05:20,280 --> 00:05:25,740
so back here we've got some more zeros

135
00:05:23,400 --> 00:05:28,440
so I'm gonna zoom out and try to find

136
00:05:25,740 --> 00:05:31,740
the very first instance of stuff after

137
00:05:28,440 --> 00:05:33,539
the system is starting to attempt

138
00:05:31,740 --> 00:05:35,400
identification again okay there's

139
00:05:33,539 --> 00:05:36,419
nothing there so we move forward in the

140
00:05:35,400 --> 00:05:40,440
clock

141
00:05:36,419 --> 00:05:42,720
and we've got F7 that doesn't seem like

142
00:05:40,440 --> 00:05:44,100
legitimate value that we're expecting

143
00:05:42,720 --> 00:05:46,740
right

144
00:05:44,100 --> 00:05:48,060
well we have 9f here and we can see

145
00:05:46,740 --> 00:05:50,400
there's a little bit of a glitch right

146
00:05:48,060 --> 00:05:53,280
there so we have the Dediprog sending

147
00:05:50,400 --> 00:05:55,199
9f but then after that we're seeing you

148
00:05:53,280 --> 00:05:57,300
know something invalid we're seeing this

149
00:05:55,199 --> 00:06:01,020
is still high and so the parser is

150
00:05:57,300 --> 00:06:03,660
interpreting that as F7 and moving on

151
00:06:01,020 --> 00:06:05,039
now in reality that's not what's

152
00:06:03,660 --> 00:06:07,259
actually happening here what we're

153
00:06:05,039 --> 00:06:09,960
seeing is it turns out that this chip

154
00:06:07,259 --> 00:06:12,720
select line is being forced Low by the

155
00:06:09,960 --> 00:06:15,539
currently powered on CPU PCH circuitry

156
00:06:12,720 --> 00:06:18,240
so the Dediprog wants to do 9f and

157
00:06:15,539 --> 00:06:20,639
then raise the and then it wants to get

158
00:06:18,240 --> 00:06:23,160
the em back and then it wants to raise

159
00:06:20,639 --> 00:06:25,500
the chip select but it's never actually

160
00:06:23,160 --> 00:06:27,000
able to do that so here we've got some

161
00:06:25,500 --> 00:06:29,400
clocking again

162
00:06:27,000 --> 00:06:31,440
and if we eyeball parse this rather than

163
00:06:29,400 --> 00:06:36,000
believe the parser we could believe that

164
00:06:31,440 --> 00:06:39,539
this is EF right we got one one one zero

165
00:06:36,000 --> 00:06:42,479
so that's an e and then one one one F so

166
00:06:39,539 --> 00:06:44,280
that's an F but then you know actually

167
00:06:42,479 --> 00:06:46,080
right there I didn't see this in my

168
00:06:44,280 --> 00:06:48,300
previous things but you can see that

169
00:06:46,080 --> 00:06:50,940
actually the Dediprog tried to bring

170
00:06:48,300 --> 00:06:53,280
the chip select line high but there's

171
00:06:50,940 --> 00:06:55,080
some circuitry that's holding it low so

172
00:06:53,280 --> 00:06:57,720
it's not able to do that right remember

173
00:06:55,080 --> 00:06:59,400
low is like ground so you can put some

174
00:06:57,720 --> 00:07:02,280
voltage in but it's just going to be

175
00:06:59,400 --> 00:07:06,060
grounded back out so then the Dediprog

176
00:07:02,280 --> 00:07:09,240
once again tries to send in the 9f and

177
00:07:06,060 --> 00:07:12,120
then again it doesn't try to raise the

178
00:07:09,240 --> 00:07:14,580
chip select but instead this particular

179
00:07:12,120 --> 00:07:17,220
 SPI chip is just sending nothing it's

180
00:07:14,580 --> 00:07:19,680
sending out garbage so zeros all along

181
00:07:17,220 --> 00:07:22,919
so that's not what we're expecting we've

182
00:07:19,680 --> 00:07:24,900
got some weird transaction here one

183
00:07:22,919 --> 00:07:26,940
suggestion that I got at this point from

184
00:07:24,900 --> 00:07:29,160
folks online was well maybe the system

185
00:07:26,940 --> 00:07:31,020
is you know powered down and so it can't

186
00:07:29,160 --> 00:07:33,539
actually send or apply until it's

187
00:07:31,020 --> 00:07:35,639
powered back up and so that sent me

188
00:07:33,539 --> 00:07:37,800
looking to the manual to find you know

189
00:07:35,639 --> 00:07:40,800
what sort of things it had for powering

190
00:07:37,800 --> 00:07:42,479
down versus waking up and so there's a

191
00:07:40,800 --> 00:07:44,639
particular command here

192
00:07:42,479 --> 00:07:47,520
which is B9

193
00:07:44,639 --> 00:07:49,560
and if that is sent this power down so

194
00:07:47,520 --> 00:07:52,080
we've already powered up the CPU so the

195
00:07:49,560 --> 00:07:54,900
CPU may be then powered down the chip to

196
00:07:52,080 --> 00:07:57,479
basically say save some save some

197
00:07:54,900 --> 00:07:59,400
current so if the system powered it down

198
00:07:57,479 --> 00:08:01,380
it would send a B9 and then it would

199
00:07:59,400 --> 00:08:03,960
bring chip select high and then the

200
00:08:01,380 --> 00:08:06,000
system would be at a power down mode but

201
00:08:03,960 --> 00:08:08,039
that seems probably not to be the case

202
00:08:06,000 --> 00:08:09,840
because it should keep the chip select

203
00:08:08,039 --> 00:08:12,240
high and basically not be selecting the

204
00:08:09,840 --> 00:08:13,979
chip ever after but we can see based on

205
00:08:12,240 --> 00:08:16,979
our logic analyzer that it is low

206
00:08:13,979 --> 00:08:18,900
forever but just in case I tried to you

207
00:08:16,979 --> 00:08:20,759
know send the equivalent power back up

208
00:08:18,900 --> 00:08:22,979
or release from power down command

209
00:08:20,759 --> 00:08:24,900
because it turns out the Dediprog does

210
00:08:22,979 --> 00:08:27,419
have a way to just send commands

211
00:08:24,900 --> 00:08:29,580
directly so if I wanted to send an A B

212
00:08:27,419 --> 00:08:32,159
to try to wake the device up that

213
00:08:29,580 --> 00:08:35,339
Dediprog under config

214
00:08:32,159 --> 00:08:37,620
and Engineering mode you can send bytes

215
00:08:35,339 --> 00:08:39,300
so I could just say a b and then send

216
00:08:37,620 --> 00:08:41,940
single command and then I would be

217
00:08:39,300 --> 00:08:43,500
attempting a wake up now what I saw was

218
00:08:41,940 --> 00:08:46,380
that didn't do anything you know I sent

219
00:08:43,500 --> 00:08:48,660
an A B so I could do a b and then I

220
00:08:46,380 --> 00:08:51,060
could send a 9f to try to do

221
00:08:48,660 --> 00:08:52,680
identification as well but ultimately

222
00:08:51,060 --> 00:08:55,140
what I saw was these didn't do anything

223
00:08:52,680 --> 00:08:56,760
they still kept getting back garbage and

224
00:08:55,140 --> 00:08:58,440
I think this is ultimately down to the

225
00:08:56,760 --> 00:09:01,440
fact that the Dediprog cannot bring

226
00:08:58,440 --> 00:09:03,420
chip select High and in the manual it

227
00:09:01,440 --> 00:09:05,940
says that after you send the a b the

228
00:09:03,420 --> 00:09:07,260
chip select needs to be made high so

229
00:09:05,940 --> 00:09:09,660
basically I think it's not doing

230
00:09:07,260 --> 00:09:11,100
anything so maybe the CPU itself could

231
00:09:09,660 --> 00:09:14,040
power it back up if it was actually

232
00:09:11,100 --> 00:09:15,720
powered down don't really know but this

233
00:09:14,040 --> 00:09:17,760
is just to show you kind of what

234
00:09:15,720 --> 00:09:21,300
Behavior we're seeing on the chip

235
00:09:17,760 --> 00:09:23,339
so again the idea was maybe if I just

236
00:09:21,300 --> 00:09:25,260
let it get to steady state after it's

237
00:09:23,339 --> 00:09:27,180
done with this then maybe I could go

238
00:09:25,260 --> 00:09:29,820
ahead and you know send in commands and

239
00:09:27,180 --> 00:09:31,800
do the reads do the identification but

240
00:09:29,820 --> 00:09:34,019
the result was no Dediprog still

241
00:09:31,800 --> 00:09:36,120
fails here and it seems to largely be

242
00:09:34,019 --> 00:09:38,580
down to the fact that it can't bring the

243
00:09:36,120 --> 00:09:40,680
chip select high or possibly the device

244
00:09:38,580 --> 00:09:42,180
has been put into a sleep mode although

245
00:09:40,680 --> 00:09:45,560
that doesn't seem to be likely because

246
00:09:42,180 --> 00:09:45,560
of the low chip select

