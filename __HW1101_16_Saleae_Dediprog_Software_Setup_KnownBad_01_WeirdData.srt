1
00:00:00,060 --> 00:00:04,200
so now all I'm going to do is I'm going

2
00:00:02,159 --> 00:00:06,120
to remove the chip clip from the 2

3
00:00:04,200 --> 00:00:08,639
megabyte chip that I can successfully

4
00:00:06,120 --> 00:00:10,260
read and move it over to the 8 megabyte

5
00:00:08,639 --> 00:00:13,139
chip that I cannot read

6
00:00:10,260 --> 00:00:15,540
so I go ahead and clip that on

7
00:00:13,139 --> 00:00:18,660
and now I'm going to once again start

8
00:00:15,540 --> 00:00:22,640
the logic analyzer and then start the

9
00:00:18,660 --> 00:00:22,640
Dediprog and see what it sees

10
00:00:24,900 --> 00:00:29,400
so I start up the Dediprog and it's

11
00:00:27,119 --> 00:00:31,500
trying to detect the chip but in this

12
00:00:29,400 --> 00:00:33,540
case it will actually fail

13
00:00:31,500 --> 00:00:35,880
so we need to understand why that is

14
00:00:33,540 --> 00:00:37,500
because we've got some chip we want to

15
00:00:35,880 --> 00:00:40,620
read it we need to know how to work

16
00:00:37,500 --> 00:00:42,540
around that so if we zoom out and see

17
00:00:40,620 --> 00:00:45,780
what we can see

18
00:00:42,540 --> 00:00:47,640
what we see is a whole bunch of stuff

19
00:00:45,780 --> 00:00:49,440
that doesn't look right not certainly

20
00:00:47,640 --> 00:00:52,140
compared to what we saw on the other two

21
00:00:49,440 --> 00:00:54,239
megabyte chip in particular we see the

22
00:00:52,140 --> 00:00:56,579
voltage is like spiking up and down it's

23
00:00:54,239 --> 00:00:59,100
not just you know Staying High and using

24
00:00:56,579 --> 00:01:01,320
the chip we don't see any sort of data

25
00:00:59,100 --> 00:01:03,359
analysis exceeding for this QSPI to

26
00:01:01,320 --> 00:01:06,060
fight despite the fact that it seems to

27
00:01:03,359 --> 00:01:09,360
have processed everything so it's not

28
00:01:06,060 --> 00:01:11,820
properly handling it so let's focus on

29
00:01:09,360 --> 00:01:14,220
specifically where the clock was

30
00:01:11,820 --> 00:01:17,100
actually running so let's zoom in on

31
00:01:14,220 --> 00:01:19,619
that and well that is just a single blip

32
00:01:17,100 --> 00:01:22,799
so that is not very helpful

33
00:01:19,619 --> 00:01:25,860
so the question is you know why is this

34
00:01:22,799 --> 00:01:28,619
data all looking so weird and bad

35
00:01:25,860 --> 00:01:30,360
and the hypothesis my first order

36
00:01:28,619 --> 00:01:32,700
hypothesis here you know the short

37
00:01:30,360 --> 00:01:34,080
answer is I didn't know like I I had

38
00:01:32,700 --> 00:01:35,579
encountered this kind of thing many

39
00:01:34,080 --> 00:01:37,920
times in the past but I didn't have a

40
00:01:35,579 --> 00:01:39,780
logic analyzer so I couldn't dig into it

41
00:01:37,920 --> 00:01:42,000
further now that I do have a logic

42
00:01:39,780 --> 00:01:43,619
analyzer I can and so in this particular

43
00:01:42,000 --> 00:01:46,259
case I'll show you how I worked through

44
00:01:43,619 --> 00:01:48,780
uh trying to find the likely cause of

45
00:01:46,259 --> 00:01:51,540
this and then finding a workaround so my

46
00:01:48,780 --> 00:01:53,579
hypothesis here was that perhaps the

47
00:01:51,540 --> 00:01:55,860
Dediprog while it was providing power

48
00:01:53,579 --> 00:01:57,540
to the chip was powering up other

49
00:01:55,860 --> 00:02:00,119
components on the chip which then we're

50
00:01:57,540 --> 00:02:02,340
doing other SPI transactions on the SPI

51
00:02:00,119 --> 00:02:04,619
bus which was conflicting with the deady

52
00:02:02,340 --> 00:02:06,600
prague's ability to actually identify

53
00:02:04,619 --> 00:02:07,740
the chip you can certainly see that you

54
00:02:06,600 --> 00:02:09,959
know it doesn't look like it was

55
00:02:07,740 --> 00:02:12,000
succeeding and doing any clock cycles so

56
00:02:09,959 --> 00:02:13,920
perhaps this was you know something was

57
00:02:12,000 --> 00:02:16,020
holding the the clock low so that it

58
00:02:13,920 --> 00:02:17,400
couldn't properly signal perhaps

59
00:02:16,020 --> 00:02:19,200
something was messing with the voltage

60
00:02:17,400 --> 00:02:21,720
but even beyond that you can see there's

61
00:02:19,200 --> 00:02:25,980
all sorts of signaling on this data quad

62
00:02:21,720 --> 00:02:27,720
3 data quad 2 data quad 0 so it's just

63
00:02:25,980 --> 00:02:29,819
very suspicious and weird that you'd see

64
00:02:27,720 --> 00:02:31,260
all this traffic that does not seem to

65
00:02:29,819 --> 00:02:33,540
be the kind of traffic that would be

66
00:02:31,260 --> 00:02:36,060
caused by the Dediprog itself

67
00:02:33,540 --> 00:02:39,420
so in order to test this Theory what I

68
00:02:36,060 --> 00:02:42,720
said was okay if I've got the Dediprog

69
00:02:39,420 --> 00:02:44,940
and if it's providing 3.3 volts then I

70
00:02:42,720 --> 00:02:46,680
want to see whether 3.3 volts is

71
00:02:44,940 --> 00:02:48,360
actually going to cause something to

72
00:02:46,680 --> 00:02:50,040
power up on the system

73
00:02:48,360 --> 00:02:52,080
now in this particular case because I

74
00:02:50,040 --> 00:02:54,480
had closed out that eddieprogen reopened

75
00:02:52,080 --> 00:02:57,300
it it wouldn't necessarily be providing

76
00:02:54,480 --> 00:02:59,280
any voltage up front or you know the it

77
00:02:57,300 --> 00:03:01,500
may not be providing 3.3 volts certainly

78
00:02:59,280 --> 00:03:03,360
because the configuration had been

79
00:03:01,500 --> 00:03:05,340
dropped from the previous chip we were

80
00:03:03,360 --> 00:03:06,599
reading so to understand like what it's

81
00:03:05,340 --> 00:03:08,940
actually providing we have to look at

82
00:03:06,599 --> 00:03:12,000
its configuration and here you can see

83
00:03:08,940 --> 00:03:14,220
that it is defaulting to 1.8 volts in

84
00:03:12,000 --> 00:03:16,080
this particular case and I know from the

85
00:03:14,220 --> 00:03:18,239
data sheet that this other chip that I'm

86
00:03:16,080 --> 00:03:22,379
trying to read but not succeeding in

87
00:03:18,239 --> 00:03:26,159
this is another 3 Volt or 2.7 to 3.6

88
00:03:22,379 --> 00:03:28,680
volt SPI flash chip so 1.8 first of all

89
00:03:26,159 --> 00:03:30,720
just isn't going to get the job done so

90
00:03:28,680 --> 00:03:34,019
the first thing we might try to do is

91
00:03:30,720 --> 00:03:35,640
like bump the voltage up to well 2.5 is

92
00:03:34,019 --> 00:03:39,300
too low because the datasheet says it

93
00:03:35,640 --> 00:03:41,280
only goes as low as 2.7 and 3.5 okay

94
00:03:39,300 --> 00:03:44,159
that's within the range you know up to

95
00:03:41,280 --> 00:03:47,519
3.6 so we could use that now I'm using

96
00:03:44,159 --> 00:03:50,760
the Dediprog sf600 plus so I have this

97
00:03:47,519 --> 00:03:52,799
full voltage image more granular sort of

98
00:03:50,760 --> 00:03:55,080
adjustment you if you're using something

99
00:03:52,799 --> 00:03:57,060
like a SF100 may not actually have that

100
00:03:55,080 --> 00:04:00,000
so for now let's just go ahead and try

101
00:03:57,060 --> 00:04:01,980
this 3.5 volts and let's just try it

102
00:04:00,000 --> 00:04:04,080
again and see if we see any different or

103
00:04:01,980 --> 00:04:06,440
better results

104
00:04:04,080 --> 00:04:08,940
so we start the logic analyzer as before

105
00:04:06,440 --> 00:04:11,159
and then we go back to the ready prog

106
00:04:08,940 --> 00:04:13,080
and we hit detect

107
00:04:11,159 --> 00:04:15,420
we see there's some sort of activity in

108
00:04:13,080 --> 00:04:17,340
the background looks like errors

109
00:04:15,420 --> 00:04:19,079
and then ultimately this is going to

110
00:04:17,340 --> 00:04:21,299
time out and that's not going to work

111
00:04:19,079 --> 00:04:24,180
either

112
00:04:21,299 --> 00:04:27,600
okay so what do we see here well we see

113
00:04:24,180 --> 00:04:29,400
a whole bunch of more garbage we see uh

114
00:04:27,600 --> 00:04:31,380
stuff that can't properly decode but

115
00:04:29,400 --> 00:04:33,540
actually we do see some stuff that does

116
00:04:31,380 --> 00:04:35,699
decode so let's you know zoom in on that

117
00:04:33,540 --> 00:04:37,800
maybe it somehow successfully got

118
00:04:35,699 --> 00:04:39,000
something through well first of all I

119
00:04:37,800 --> 00:04:41,460
can see that there's you know a little

120
00:04:39,000 --> 00:04:43,440
bit too much up and down signaling going

121
00:04:41,460 --> 00:04:46,080
on here yes it is ultimately

122
00:04:43,440 --> 00:04:49,380
successfully encoding 9f but there's

123
00:04:46,080 --> 00:04:50,400
some spurious some sort of signaling

124
00:04:49,380 --> 00:04:53,580
within there

125
00:04:50,400 --> 00:04:56,040
so if I go on to the next thing well I

126
00:04:53,580 --> 00:04:58,320
would want to see something like the EF

127
00:04:56,040 --> 00:05:01,139
right but I don't actually see that

128
00:04:58,320 --> 00:05:02,460
same thing here again there's a 9f and

129
00:05:01,139 --> 00:05:05,220
then there's something right after it

130
00:05:02,460 --> 00:05:07,560
but that something is zero so that's not

131
00:05:05,220 --> 00:05:09,840
helpful again we're just seemingly

132
00:05:07,560 --> 00:05:11,820
pretty much getting garbage and so my

133
00:05:09,840 --> 00:05:13,860
theory is the power being presented to

134
00:05:11,820 --> 00:05:17,340
the system by the Dediprog in this

135
00:05:13,860 --> 00:05:20,040
case explicitly 3.5 volts explicitly

136
00:05:17,340 --> 00:05:21,960
configured explicitly applied is not

137
00:05:20,040 --> 00:05:24,020
actually seemed to be doing what we

138
00:05:21,960 --> 00:05:24,020
expect

