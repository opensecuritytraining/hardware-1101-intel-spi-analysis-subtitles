1
00:00:00,120 --> 00:00:04,980
this is a quick video to show you how to

2
00:00:02,520 --> 00:00:08,099
connect a Saleae logic analyzer to an

3
00:00:04,980 --> 00:00:10,740
example Intel systems SPI flash storage

4
00:00:08,099 --> 00:00:12,960
chip so you need to have an Intel system

5
00:00:10,740 --> 00:00:15,240
and in this case I've chosen this

6
00:00:12,960 --> 00:00:17,940
example development board by AAEON it's

7
00:00:15,240 --> 00:00:19,920
called the UP Squared it originally had

8
00:00:17,940 --> 00:00:21,960
a heatsink on the other side which I've

9
00:00:19,920 --> 00:00:24,119
removed to make it easier to try to find

10
00:00:21,960 --> 00:00:25,439
this by flash chip here so the first

11
00:00:24,119 --> 00:00:27,539
thing you always have to do is try to

12
00:00:25,439 --> 00:00:28,920
identify the SPI flash chip and to do

13
00:00:27,539 --> 00:00:30,840
that you're going to look around for

14
00:00:28,920 --> 00:00:33,899
things on the board that maybe have

15
00:00:30,840 --> 00:00:36,120
eight pins in our particular form factor

16
00:00:33,899 --> 00:00:38,640
so the stuff on this side seems to be a

17
00:00:36,120 --> 00:00:40,920
little bit too small so we check the

18
00:00:38,640 --> 00:00:42,780
other side and over here we've got this

19
00:00:40,920 --> 00:00:44,100
one potentially could be it this one

20
00:00:42,780 --> 00:00:46,020
potentially could be it that one

21
00:00:44,100 --> 00:00:47,879
potentially could be it so those are all

22
00:00:46,020 --> 00:00:50,700
about the right size maybe this one

23
00:00:47,879 --> 00:00:52,500
eight pins on each of them but I'll just

24
00:00:50,700 --> 00:00:54,180
skip to the Chase and tell you that it's

25
00:00:52,500 --> 00:00:56,039
actually this one here and the way I

26
00:00:54,180 --> 00:00:58,920
know that is by looking at the chip

27
00:00:56,039 --> 00:01:03,420
markings and by seeing that it says that

28
00:00:58,920 --> 00:01:04,619
it's a Winbond 25Q128JW and if you

29
00:01:03,420 --> 00:01:06,900
look up that particular part number

30
00:01:04,619 --> 00:01:08,939
you'll see that it is a SPI flash chip

31
00:01:06,900 --> 00:01:11,640
so now we know where this by flash chip

32
00:01:08,939 --> 00:01:14,280
is and now we need to connect to it so

33
00:01:11,640 --> 00:01:16,979
the Saleae comes with the following probes

34
00:01:14,280 --> 00:01:19,619
and you can see that these particular

35
00:01:16,979 --> 00:01:21,659
probes have colors on one side and they

36
00:01:19,619 --> 00:01:23,640
have all black on the other side so the

37
00:01:21,659 --> 00:01:27,060
black side is ground and the colored

38
00:01:23,640 --> 00:01:29,100
side is the probes furthermore they have

39
00:01:27,060 --> 00:01:31,380
some numbering on them which is not

40
00:01:29,100 --> 00:01:32,700
strictly required to line up the

41
00:01:31,380 --> 00:01:34,680
numbering but it can make things a

42
00:01:32,700 --> 00:01:37,380
little bit simpler for you later on so

43
00:01:34,680 --> 00:01:39,960
the first one is labeled zero the next

44
00:01:37,380 --> 00:01:42,780
one is going to be labeled one and so

45
00:01:39,960 --> 00:01:44,700
forth so doing you know making this

46
00:01:42,780 --> 00:01:46,259
correlate with what you plug into can

47
00:01:44,700 --> 00:01:48,720
just make things easier for your life

48
00:01:46,259 --> 00:01:50,520
for later so I'm going to take this I'm

49
00:01:48,720 --> 00:01:52,860
going to plug it into the Saleae logic

50
00:01:50,520 --> 00:01:54,360
analyzer I'm going to take my next one

51
00:01:52,860 --> 00:01:56,399
I'm going to plug that in as well

52
00:01:54,360 --> 00:01:58,680
because we're going to need to

53
00:01:56,399 --> 00:02:00,420
potentially connect up to eight pins but

54
00:01:58,680 --> 00:02:01,680
in practice we don't actually have to do

55
00:02:00,420 --> 00:02:03,780
that now what are we going to do with

56
00:02:01,680 --> 00:02:06,719
this well the first thing that I'm going

57
00:02:03,780 --> 00:02:08,819
to do I usually skip this first probe

58
00:02:06,719 --> 00:02:10,860
the one labeled zero because it's black

59
00:02:08,819 --> 00:02:13,440
as if it were a ground and so just to

60
00:02:10,860 --> 00:02:15,480
avoid confusing myself I put that one

61
00:02:13,440 --> 00:02:17,459
off to the side and don't use it and so

62
00:02:15,480 --> 00:02:20,520
I start with the one labeled one and

63
00:02:17,459 --> 00:02:23,819
then I'm going to connect it to the pin

64
00:02:20,520 --> 00:02:25,680
number one on the SPI flash chip so if

65
00:02:23,819 --> 00:02:27,959
you remember your data sheets you need

66
00:02:25,680 --> 00:02:31,200
to you know first figure out which thing

67
00:02:27,959 --> 00:02:33,000
is pin one so let's zoom in here we can

68
00:02:31,200 --> 00:02:35,040
see the dot there that indicates that

69
00:02:33,000 --> 00:02:36,540
pin one is going to be the one that's

70
00:02:35,040 --> 00:02:38,940
furthest to this side

71
00:02:36,540 --> 00:02:42,599
so then we just need to you know connect

72
00:02:38,940 --> 00:02:45,840
to the probe to pin one and so I'm going

73
00:02:42,599 --> 00:02:47,340
to cut between these so that you don't

74
00:02:45,840 --> 00:02:49,560
have to watch me do the whole thing but

75
00:02:47,340 --> 00:02:51,720
the key point is that

76
00:02:49,560 --> 00:02:55,260
these particular probes have a little

77
00:02:51,720 --> 00:02:57,660
grabber thing there so you press down

78
00:02:55,260 --> 00:03:00,980
and then you can grab and so you're

79
00:02:57,660 --> 00:03:03,540
going to press down and connect it to

80
00:03:00,980 --> 00:03:05,220
this pin one

81
00:03:03,540 --> 00:03:08,340
and then we're going to do the same

82
00:03:05,220 --> 00:03:11,700
thing again for all the rest of them

83
00:03:08,340 --> 00:03:15,300
we're doing probe one two pin one

84
00:03:11,700 --> 00:03:18,720
probe 2 to pin 2 probe three to pin

85
00:03:15,300 --> 00:03:21,060
three and then pin four is the ground so

86
00:03:18,720 --> 00:03:24,239
I always tend to use the first ground

87
00:03:21,060 --> 00:03:26,040
wire from my Saleae logic analyzer and I

88
00:03:24,239 --> 00:03:27,599
use that for the ground so I connect

89
00:03:26,040 --> 00:03:29,519
that to

90
00:03:27,599 --> 00:03:31,620
pin four and then we're going to

91
00:03:29,519 --> 00:03:33,780
continue that with pins five six and

92
00:03:31,620 --> 00:03:36,360
seven we don't actually need to connect

93
00:03:33,780 --> 00:03:38,519
to pin 8 because that's just the voltage

94
00:03:36,360 --> 00:03:41,580
and it'll always just be high so there's

95
00:03:38,519 --> 00:03:45,420
nothing really to see there pin five pin

96
00:03:41,580 --> 00:03:48,180
six and pin seven so now we have all of

97
00:03:45,420 --> 00:03:51,180
our probes connected from the Saleae to

98
00:03:48,180 --> 00:03:52,440
the processor and ultimately what we're

99
00:03:51,180 --> 00:03:54,540
going to need to do then is just make

100
00:03:52,440 --> 00:03:57,060
sure this Saleae is plugged into USB to

101
00:03:54,540 --> 00:03:58,799
our analysis computer and then once we

102
00:03:57,060 --> 00:04:00,840
want to analyze it we're going to start

103
00:03:58,799 --> 00:04:03,720
the Saleae and then we're going to power

104
00:04:00,840 --> 00:04:05,819
on the board and watch the SPI traffic

105
00:04:03,720 --> 00:04:08,959
that occurs at boot and later on we'll

106
00:04:05,819 --> 00:04:08,959
do more complicated things

