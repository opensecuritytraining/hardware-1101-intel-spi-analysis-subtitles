1
00:00:00,120 --> 00:00:05,279
now one simple thing you can do when you

2
00:00:02,520 --> 00:00:08,400
have the capability to visualize the SPI

3
00:00:05,279 --> 00:00:10,800
accesses at boot time is you can compare

4
00:00:08,400 --> 00:00:13,019
differences of behavior of the system

5
00:00:10,800 --> 00:00:15,299
based on you know changing configuration

6
00:00:13,019 --> 00:00:17,279
options and things like that does the

7
00:00:15,299 --> 00:00:19,260
system access more flash does it access

8
00:00:17,279 --> 00:00:22,020
less flash and then of course if it's

9
00:00:19,260 --> 00:00:23,640
more or less you can go look into those

10
00:00:22,020 --> 00:00:25,380
particular regions that are accessed or

11
00:00:23,640 --> 00:00:27,539
not accessed and see what the nature of

12
00:00:25,380 --> 00:00:29,699
them is through reverse engineering so

13
00:00:27,539 --> 00:00:32,579
I'd mentioned that my example board was

14
00:00:29,699 --> 00:00:35,219
selected because it also is vulnerable

15
00:00:32,579 --> 00:00:38,880
to an old Intel management engine

16
00:00:35,219 --> 00:00:41,399
exploit and so here I have a graph of

17
00:00:38,880 --> 00:00:44,760
the normal boot of the system and I'm

18
00:00:41,399 --> 00:00:46,200
going to change this to be in blue

19
00:00:44,760 --> 00:00:48,600
so now you can do that by double

20
00:00:46,200 --> 00:00:50,520
clicking on the color and changing some

21
00:00:48,600 --> 00:00:52,980
different color and I want to compare

22
00:00:50,520 --> 00:00:55,860
that against how the system behaves when

23
00:00:52,980 --> 00:00:58,260
the actual txe or management engine

24
00:00:55,860 --> 00:01:00,420
exploit is executed on the system at

25
00:00:58,260 --> 00:01:02,760
boot time now the nature of the exploit

26
00:01:00,420 --> 00:01:05,400
is that it exploits the management

27
00:01:02,760 --> 00:01:08,400
engine which runs first before the x86

28
00:01:05,400 --> 00:01:10,439
system runs before the main UEFI BIOS

29
00:01:08,400 --> 00:01:12,000
firmware runs and then basically the

30
00:01:10,439 --> 00:01:14,460
exploited management engine firmware

31
00:01:12,000 --> 00:01:15,360
just kind of stops and waits for a JTAG

32
00:01:14,460 --> 00:01:18,420
connection

33
00:01:15,360 --> 00:01:20,280
so if I click that on you'll see that

34
00:01:18,420 --> 00:01:22,080
this is the red axis versus the blue

35
00:01:20,280 --> 00:01:24,840
axis and I can kind of you know flip

36
00:01:22,080 --> 00:01:27,960
back and forth between them but the

37
00:01:24,840 --> 00:01:29,580
noteworthy thing here is essentially the

38
00:01:27,960 --> 00:01:31,860
differences down here these are the

39
00:01:29,580 --> 00:01:33,840
regions for management engine this low

40
00:01:31,860 --> 00:01:36,900
region is where the management engine

41
00:01:33,840 --> 00:01:38,280
data itself is so let's get rid of that

42
00:01:36,900 --> 00:01:40,259
data

43
00:01:38,280 --> 00:01:42,119
and so you can see that these basically

44
00:01:40,259 --> 00:01:44,040
look the same like they're literally the

45
00:01:42,119 --> 00:01:45,960
same thing just sort of shifted in time

46
00:01:44,040 --> 00:01:47,460
and not really sure why that time

47
00:01:45,960 --> 00:01:49,140
shifting occurred it could have just

48
00:01:47,460 --> 00:01:51,240
been because this thing took you know a

49
00:01:49,140 --> 00:01:52,200
little longer to fiddle around with

50
00:01:51,240 --> 00:01:53,640
something it could have been something

51
00:01:52,200 --> 00:01:56,520
to do with like you know I just

52
00:01:53,640 --> 00:01:58,799
reflashed the system and for one data

53
00:01:56,520 --> 00:02:00,540
point versus the other of course you

54
00:01:58,799 --> 00:02:01,920
know you could skew these things you

55
00:02:00,540 --> 00:02:03,780
know just change the the data point

56
00:02:01,920 --> 00:02:05,820
values to skew them and force them to

57
00:02:03,780 --> 00:02:08,580
overlap but the point I want to make

58
00:02:05,820 --> 00:02:10,619
here is that on the system in red on the

59
00:02:08,580 --> 00:02:13,500
boot flow in red which is when it was

60
00:02:10,619 --> 00:02:15,840
exploited we don't see this additional

61
00:02:13,500 --> 00:02:18,000
accesses to the SPI flash at these other

62
00:02:15,840 --> 00:02:19,800
ranges if we looked at these ranges we

63
00:02:18,000 --> 00:02:21,720
would see that those are other areas of

64
00:02:19,800 --> 00:02:23,520
the management engine firmware so the

65
00:02:21,720 --> 00:02:25,860
point is the firmware gets a certain way

66
00:02:23,520 --> 00:02:28,260
in it gets exploited and then it just

67
00:02:25,860 --> 00:02:29,819
sort of sits in a infinite Loop waiting

68
00:02:28,260 --> 00:02:31,920
for a debug connection and so it never

69
00:02:29,819 --> 00:02:34,560
bothers to read in the rest of this

70
00:02:31,920 --> 00:02:36,660
but at the same time we do see the

71
00:02:34,560 --> 00:02:39,000
accesses to the rest of this by flash

72
00:02:36,660 --> 00:02:41,640
chip and that has to do with it you know

73
00:02:39,000 --> 00:02:43,379
trying to read in the information for

74
00:02:41,640 --> 00:02:45,239
ultimately loading the BIOS and

75
00:02:43,379 --> 00:02:47,040
launching the BIOS furthermore you can

76
00:02:45,239 --> 00:02:49,200
see that this is sort of a nice linear

77
00:02:47,040 --> 00:02:51,420
thing and this kind of is like linear

78
00:02:49,200 --> 00:02:53,640
but it's got a kink in it and that is

79
00:02:51,420 --> 00:02:55,739
essentially down to the fact that there

80
00:02:53,640 --> 00:02:57,660
were these parallel axises so this

81
00:02:55,739 --> 00:02:59,400
doesn't go up as fast because it's sort

82
00:02:57,660 --> 00:03:01,019
of like access here then X is here then

83
00:02:59,400 --> 00:03:03,060
X is here then X is here and it's sort

84
00:03:01,019 --> 00:03:05,640
of jumping back and forth and that sort

85
00:03:03,060 --> 00:03:07,560
of limits the slope of this whereas the

86
00:03:05,640 --> 00:03:10,500
system where it just completely stopped

87
00:03:07,560 --> 00:03:13,620
accessing the management engine it you

88
00:03:10,500 --> 00:03:16,440
know had a higher slope in order to just

89
00:03:13,620 --> 00:03:18,720
read with no other stuff in parallel so

90
00:03:16,440 --> 00:03:20,280
again that's just one example of how you

91
00:03:18,720 --> 00:03:22,980
could you know see differences in

92
00:03:20,280 --> 00:03:24,599
Behavior commonalities or differences

93
00:03:22,980 --> 00:03:27,000
you know if we force this thing to

94
00:03:24,599 --> 00:03:29,700
overlap and line up and you know for

95
00:03:27,000 --> 00:03:31,319
instance we would see in this range you

96
00:03:29,700 --> 00:03:33,300
know what's accessed exactly the same

97
00:03:31,319 --> 00:03:35,040
versus what it's slightly different and

98
00:03:33,300 --> 00:03:36,900
so forth and this can be one way that

99
00:03:35,040 --> 00:03:39,780
you do a little bit of dynamic analysis

100
00:03:36,900 --> 00:03:43,159
even on a closed system where you

101
00:03:39,780 --> 00:03:43,159
perhaps don't have debugging access

