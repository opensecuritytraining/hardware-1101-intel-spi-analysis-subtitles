1
00:00:00,120 --> 00:00:05,460
Okay cool so Chipsec can read stuff but

2
00:00:03,300 --> 00:00:07,980
can we read stuff and how do we do that

3
00:00:05,460 --> 00:00:10,679
well we learned when we learned to read

4
00:00:07,980 --> 00:00:13,440
the fund manuals in architecture 4001

5
00:00:10,679 --> 00:00:16,440
that Intel specifies in their manuals

6
00:00:13,440 --> 00:00:18,660
how to access these memory mapped IO

7
00:00:16,440 --> 00:00:20,760
registers so to do that you always have

8
00:00:18,660 --> 00:00:23,460
to identify your particular chipset and

9
00:00:20,760 --> 00:00:25,380
then identify a specific data sheet that

10
00:00:23,460 --> 00:00:28,439
goes along with it in my case I know

11
00:00:25,380 --> 00:00:30,840
that this is a Intel Atom Processor that

12
00:00:28,439 --> 00:00:32,520
this particular development board is and

13
00:00:30,840 --> 00:00:35,160
this is the right data sheet so if you

14
00:00:32,520 --> 00:00:37,620
took architecture 4001 then you're going

15
00:00:35,160 --> 00:00:39,239
to look this up yourself and if you're

16
00:00:37,620 --> 00:00:41,160
taking this as part of something like a

17
00:00:39,239 --> 00:00:42,899
workshop then I'm going to help you find

18
00:00:41,160 --> 00:00:43,860
the right data sheet for your particular

19
00:00:42,899 --> 00:00:46,680
system

20
00:00:43,860 --> 00:00:48,120
but for this particular system what it

21
00:00:46,680 --> 00:00:49,680
says right at the beginning is these

22
00:00:48,120 --> 00:00:51,719
registers are mapped in a host street

23
00:00:49,680 --> 00:00:55,379
address space the address offsets are

24
00:00:51,719 --> 00:00:58,500
relative to SPIBAR 0. see section 24.2

25
00:00:55,379 --> 00:01:00,360
so we need to find SPIBAR 0 first find

26
00:00:58,500 --> 00:01:03,059
where that is in memory mapped IO space

27
00:01:00,360 --> 00:01:06,119
to understand where these offset zero

28
00:01:03,059 --> 00:01:09,720
offset 4 offset 8 Etc are so we go to

29
00:01:06,119 --> 00:01:12,659
this section 24.2 we come down here we

30
00:01:09,720 --> 00:01:15,659
see thing called SPIBAR 0 at offset 10

31
00:01:12,659 --> 00:01:18,240
in this particular PCI configuration

32
00:01:15,659 --> 00:01:19,439
address space again we learn about PCI

33
00:01:18,240 --> 00:01:22,380
configuration address space and

34
00:01:19,439 --> 00:01:24,240
architecture 4001 but what this says is

35
00:01:22,380 --> 00:01:26,520
that this is because it's an Intel

36
00:01:24,240 --> 00:01:29,939
internal component we know that it's PCI

37
00:01:26,520 --> 00:01:32,880
bus zero it's device 13 because it tells

38
00:01:29,939 --> 00:01:38,280
us right here device 13 and function two

39
00:01:32,880 --> 00:01:40,500
so BDF plus device function 0 13 2. so

40
00:01:38,280 --> 00:01:43,740
once we have that then we can use this

41
00:01:40,500 --> 00:01:45,240
member thing to find the base address in

42
00:01:43,740 --> 00:01:47,100
memory mapped IO where those other

43
00:01:45,240 --> 00:01:49,439
registers can be found so now we're

44
00:01:47,100 --> 00:01:51,240
going to use chipsex PCI reading

45
00:01:49,439 --> 00:01:54,420
capability or PCI configuration address

46
00:01:51,240 --> 00:01:59,220
space reading capability to go to bus 0

47
00:01:54,420 --> 00:02:00,899
device 13 function to offset 10 and

48
00:01:59,220 --> 00:02:02,939
we're going to pull out these bits to

49
00:02:00,899 --> 00:02:05,280
figure out what is the address and

50
00:02:02,939 --> 00:02:08,399
memory where we can find memory map dial

51
00:02:05,280 --> 00:02:12,959
so to do that we need to use the Chipsec

52
00:02:08,399 --> 00:02:15,720
util PCI command and so PCI and then

53
00:02:12,959 --> 00:02:16,500
help to figure out what the right format

54
00:02:15,720 --> 00:02:19,020
is

55
00:02:16,500 --> 00:02:21,000
okay so we could do enumerations and so

56
00:02:19,020 --> 00:02:24,239
forth but we already know the bus device

57
00:02:21,000 --> 00:02:29,760
function that we want so it's PCI read

58
00:02:24,239 --> 00:02:31,200
bus 0 device 13 function 2 offset 10 and

59
00:02:29,760 --> 00:02:33,420
then how big do we want to read well

60
00:02:31,200 --> 00:02:35,520
this is a 32-bit register so we're going

61
00:02:33,420 --> 00:02:38,099
to read four bytes and see that they

62
00:02:35,520 --> 00:02:40,980
have mnemonics for the sizes of bytes or

63
00:02:38,099 --> 00:02:43,500
DWORDs so DWORD would be four bytes so

64
00:02:40,980 --> 00:02:50,160
let's do this we're going to do a

65
00:02:43,500 --> 00:02:52,800
Chipsec util PCI read bus 0 device 13 so

66
00:02:50,160 --> 00:02:57,300
13 and hex is D this is expecting

67
00:02:52,800 --> 00:03:00,840
everything in HEX so D function 2 offset

68
00:02:57,300 --> 00:03:03,900
hex 10 and we want to read a b word

69
00:03:00,840 --> 00:03:06,959
worth of information so this should

70
00:03:03,900 --> 00:03:11,400
ultimately give us this member field so

71
00:03:06,959 --> 00:03:15,239
it says fed01000 is where we can find

72
00:03:11,400 --> 00:03:17,280
this SPIBAR 0. so starting at memory

73
00:03:15,239 --> 00:03:20,640
quote unquote memory but really memory

74
00:03:17,280 --> 00:03:22,500
mapped IO address fed01000 that's

75
00:03:20,640 --> 00:03:25,260
where we can find these other registers

76
00:03:22,500 --> 00:03:27,420
like the primary region flash address

77
00:03:25,260 --> 00:03:30,120
discrete address Etc

78
00:03:27,420 --> 00:03:32,099
now unfortunately this particular data

79
00:03:30,120 --> 00:03:34,680
sheet is messed up because you can see

80
00:03:32,099 --> 00:03:36,000
that it says starting at offset four we

81
00:03:34,680 --> 00:03:37,379
have this thing that it says is at

82
00:03:36,000 --> 00:03:39,120
offset 8 and then there's this other

83
00:03:37,379 --> 00:03:41,760
thing at offset 8 and that starts at

84
00:03:39,120 --> 00:03:44,400
offset 8 for real so this offset fourth

85
00:03:41,760 --> 00:03:45,959
thing is actually messed up and if we

86
00:03:44,400 --> 00:03:47,580
you know click on that it doesn't

87
00:03:45,959 --> 00:03:49,860
actually have it there so so

88
00:03:47,580 --> 00:03:52,319
unfortunately we need to go look at a

89
00:03:49,860 --> 00:03:54,000
different data sheet that is similar

90
00:03:52,319 --> 00:03:56,879
enough that it should behave the same

91
00:03:54,000 --> 00:03:59,099
way so this is now a 200 series chipset

92
00:03:56,879 --> 00:04:00,420
it's just something that I grabbed

93
00:03:59,099 --> 00:04:03,239
randomly

94
00:04:00,420 --> 00:04:04,560
uh so you can see here at offset 4 it

95
00:04:03,239 --> 00:04:06,540
has this thing that it calls Hardware

96
00:04:04,560 --> 00:04:08,280
sequencing flash status and control

97
00:04:06,540 --> 00:04:12,239
register you can see the default value

98
00:04:08,280 --> 00:04:15,299
is 2000 just like in this other one

99
00:04:12,239 --> 00:04:17,340
so 2000 so that seems to all Jive so

100
00:04:15,299 --> 00:04:20,459
we're going to need to use this thing's

101
00:04:17,340 --> 00:04:22,260
definition of that register so the

102
00:04:20,459 --> 00:04:23,639
registers that matter here like just at

103
00:04:22,260 --> 00:04:25,199
a high level

104
00:04:23,639 --> 00:04:27,660
the registers that matter are these

105
00:04:25,199 --> 00:04:29,699
flash data registers starting at offset

106
00:04:27,660 --> 00:04:31,740
10 these are where you're going to get

107
00:04:29,699 --> 00:04:33,780
data back out of the flash chip so when

108
00:04:31,740 --> 00:04:35,699
you issue a retransaction it's going to

109
00:04:33,780 --> 00:04:38,460
land and you know if you read four bytes

110
00:04:35,699 --> 00:04:40,800
it's going to land in the four bytes of

111
00:04:38,460 --> 00:04:43,500
flash data zero and then the next four

112
00:04:40,800 --> 00:04:46,440
bytes will land in flash data one and so

113
00:04:43,500 --> 00:04:47,940
forth so flash data registers are

114
00:04:46,440 --> 00:04:49,560
important registers because if you're

115
00:04:47,940 --> 00:04:50,880
writing data you would write data there

116
00:04:49,560 --> 00:04:52,860
and then you would cause a transaction

117
00:04:50,880 --> 00:04:55,139
and stuff would be pulled out of this

118
00:04:52,860 --> 00:04:57,240
and sent on the SPI bus and written to

119
00:04:55,139 --> 00:04:59,400
the flash chip but we're reading data so

120
00:04:57,240 --> 00:05:01,320
this all we need to know is that if we

121
00:04:59,400 --> 00:05:03,360
read data in it's going to land in these

122
00:05:01,320 --> 00:05:05,460
registers and if we you know dump this

123
00:05:03,360 --> 00:05:07,080
data out of these registers that will be

124
00:05:05,460 --> 00:05:09,840
the data that came from the flash chip

125
00:05:07,080 --> 00:05:11,820
 the flash address register holds the

126
00:05:09,840 --> 00:05:15,120
actual address slash linear address

127
00:05:11,820 --> 00:05:16,979
where you want to actually read or write

128
00:05:15,120 --> 00:05:18,540
from so this is something you're going

129
00:05:16,979 --> 00:05:20,880
to fill in with either the address to

130
00:05:18,540 --> 00:05:23,580
read or the address to write okay so

131
00:05:20,880 --> 00:05:25,560
that's good and then the thing that

132
00:05:23,580 --> 00:05:27,840
matters the most here is the one that is

133
00:05:25,560 --> 00:05:29,699
unfortunately missing from you know the

134
00:05:27,840 --> 00:05:31,800
actual data sheet I'm using that is the

135
00:05:29,699 --> 00:05:33,300
hardware sequencing flash status and

136
00:05:31,800 --> 00:05:37,380
control register

137
00:05:33,300 --> 00:05:41,280
so this is basically divided it's these

138
00:05:37,380 --> 00:05:45,060
four bytes or sorry these two bytes are

139
00:05:41,280 --> 00:05:46,620
the status values and these two bytes

140
00:05:45,060 --> 00:05:48,900
are the most significant two bytes are

141
00:05:46,620 --> 00:05:51,360
the actual control bytes so starting

142
00:05:48,900 --> 00:05:53,340
from this F go which is the actual like

143
00:05:51,360 --> 00:05:55,440
let's go and start a flash transaction

144
00:05:53,340 --> 00:05:57,600
these are the bytes that we need to

145
00:05:55,440 --> 00:05:59,340
program in order to manually cause a

146
00:05:57,600 --> 00:06:01,199
transaction to occur and these are the

147
00:05:59,340 --> 00:06:02,940
bytes that would normally be checked to

148
00:06:01,199 --> 00:06:04,979
you know know whether or not a

149
00:06:02,940 --> 00:06:07,080
transaction is done or whether there's a

150
00:06:04,979 --> 00:06:08,820
cycle in progress and so we should be

151
00:06:07,080 --> 00:06:10,500
checking these before we do a flash

152
00:06:08,820 --> 00:06:12,479
transaction if we don't want to screw

153
00:06:10,500 --> 00:06:14,520
things up but we're just gonna you know

154
00:06:12,479 --> 00:06:16,259
risk it and take a chance and read

155
00:06:14,520 --> 00:06:18,180
because we're not worried about you know

156
00:06:16,259 --> 00:06:19,620
reading isn't going to cause a problem I

157
00:06:18,180 --> 00:06:21,419
mean it could because something

158
00:06:19,620 --> 00:06:24,900
somewhere could be trying to read you

159
00:06:21,419 --> 00:06:26,460
know a nvram Regis nvram variable behind

160
00:06:24,900 --> 00:06:28,020
the scenes like Windows operating system

161
00:06:26,460 --> 00:06:30,060
might try to read it and then we

162
00:06:28,020 --> 00:06:31,740
collaborate with our data so it's

163
00:06:30,060 --> 00:06:33,240
totally unsafe what I'm doing but I'm

164
00:06:31,740 --> 00:06:36,139
gonna do it anyways because I don't

165
00:06:33,240 --> 00:06:36,139
about this system

166
00:06:42,419 --> 00:06:47,759
we need to program these upper 16 bits

167
00:06:46,139 --> 00:06:50,100
and so now we need to figure out you

168
00:06:47,759 --> 00:06:51,840
know what we need to program them with

169
00:06:50,100 --> 00:06:54,780
so I'm going to have a little cheat

170
00:06:51,840 --> 00:06:56,220
sheet here to help me figure out what I

171
00:06:54,780 --> 00:06:59,759
need to write

172
00:06:56,220 --> 00:07:01,979
so we've got bit 31 so I'm going to I'm

173
00:06:59,759 --> 00:07:03,780
going to put like a b C d e for some

174
00:07:01,979 --> 00:07:06,840
bits and then I'm going to put a

175
00:07:03,780 --> 00:07:10,500
particular value for each of these

176
00:07:06,840 --> 00:07:13,740
so uh bit 31

177
00:07:10,500 --> 00:07:16,800
is the fsmie ebit which we learn in

178
00:07:13,740 --> 00:07:18,720
architecture 4001 is a nice little bit

179
00:07:16,800 --> 00:07:20,340
that every time a flash transaction

180
00:07:18,720 --> 00:07:23,039
occurs it causes a system management

181
00:07:20,340 --> 00:07:24,479
interrupt and this is a great way for an

182
00:07:23,039 --> 00:07:26,460
attacker whose compromise system

183
00:07:24,479 --> 00:07:29,280
management mode to men in the middle

184
00:07:26,460 --> 00:07:30,900
flash transactions and someone recently

185
00:07:29,280 --> 00:07:32,819
actually you know wrote a weaponized

186
00:07:30,900 --> 00:07:35,160
version of that proof of concept thing I

187
00:07:32,819 --> 00:07:36,900
made for that a long time ago but we

188
00:07:35,160 --> 00:07:38,940
don't want that to occur right now it's

189
00:07:36,900 --> 00:07:42,419
not going to be useful in any way so we

190
00:07:38,940 --> 00:07:45,660
are going to set bit 31 to zero then a

191
00:07:42,419 --> 00:07:48,780
bit 30 is reserved so we're going to put

192
00:07:45,660 --> 00:07:53,160
that as zero and then we've got 24 to 29

193
00:07:48,780 --> 00:07:57,419
so this is inclusive so 24 25 26 27 28

194
00:07:53,160 --> 00:08:00,599
29 that is six bits so going to write

195
00:07:57,419 --> 00:08:02,580
six bits right there and this is the

196
00:08:00,599 --> 00:08:05,460
number of bytes that we want to actually

197
00:08:02,580 --> 00:08:08,160
read or write so we're reading here

198
00:08:05,460 --> 00:08:10,080
and it says that the value here is

199
00:08:08,160 --> 00:08:14,400
effectively the value of this field plus

200
00:08:10,080 --> 00:08:16,500
one so if it was 3F that would be 63 but

201
00:08:14,400 --> 00:08:19,259
plus one means it's going to transfer 64

202
00:08:16,500 --> 00:08:22,139
bytes so whatever we put here so if we

203
00:08:19,259 --> 00:08:25,680
put you know all ones like that that

204
00:08:22,139 --> 00:08:30,180
would be F and that would be three right

205
00:08:25,680 --> 00:08:33,180
so that would be a 64 byte read so we're

206
00:08:30,180 --> 00:08:35,700
going to start with just a 16 byte read

207
00:08:33,180 --> 00:08:39,120
so we're going to set that like that and

208
00:08:35,700 --> 00:08:41,880
so that's 15 plus 1 is 16 bytes that are

209
00:08:39,120 --> 00:08:44,640
going to be read next we have bits 22

210
00:08:41,880 --> 00:08:46,380
and 23 so two bits that are reserved and

211
00:08:44,640 --> 00:08:49,440
they should be zero so let's set those

212
00:08:46,380 --> 00:08:51,660
to zero then we have write enable type

213
00:08:49,440 --> 00:08:54,060
and you know this may or may not be

214
00:08:51,660 --> 00:08:55,500
actually accurate to our other thing but

215
00:08:54,060 --> 00:08:58,080
we're not going to care about this we're

216
00:08:55,500 --> 00:09:00,600
just going to set it to zero so single

217
00:08:58,080 --> 00:09:03,300
bit of 0 there

218
00:09:00,600 --> 00:09:05,580
and then flash cycle this is a

219
00:09:03,300 --> 00:09:07,320
interesting number of bits because this

220
00:09:05,580 --> 00:09:09,600
is going to say what kind of flash

221
00:09:07,320 --> 00:09:11,880
operation is going to occur is it a read

222
00:09:09,600 --> 00:09:15,000
operation is it a write operation is it

223
00:09:11,880 --> 00:09:16,800
an erase so we want a read so that's

224
00:09:15,000 --> 00:09:19,740
nice and easy we just need to set the

225
00:09:16,800 --> 00:09:24,660
flash cycle to zero so how many bits is

226
00:09:19,740 --> 00:09:27,899
it well it's 17 18 19 20 so it's four

227
00:09:24,660 --> 00:09:29,940
bits and we want to set them all to zero

228
00:09:27,899 --> 00:09:32,040
in order to cause a read if we set it to

229
00:09:29,940 --> 00:09:33,600
one then it would cost sorry two we

230
00:09:32,040 --> 00:09:35,399
would cause a right but we don't want

231
00:09:33,600 --> 00:09:37,620
that

232
00:09:35,399 --> 00:09:39,660
and then finally we have this flash

233
00:09:37,620 --> 00:09:42,000
cycle go bit and this is the one that

234
00:09:39,660 --> 00:09:46,680
actually causes a flash transaction to

235
00:09:42,000 --> 00:09:48,360
occur so we want to set that EFG we want

236
00:09:46,680 --> 00:09:51,180
to set that to one to cause our

237
00:09:48,360 --> 00:09:54,660
transaction to occur right so what is

238
00:09:51,180 --> 00:09:58,260
this binary in hexadecimal well zero

239
00:09:54,660 --> 00:09:59,820
zero zero four zeros is zero four ones

240
00:09:58,260 --> 00:10:03,959
IS F

241
00:09:59,820 --> 00:10:07,339
four zeros is zero and zero zero zero

242
00:10:03,959 --> 00:10:10,560
one is one so that is hex

243
00:10:07,339 --> 00:10:14,459
0f01 okay so that is the value that we

244
00:10:10,560 --> 00:10:16,920
need to write at offset 6 because this

245
00:10:14,459 --> 00:10:19,200
overall register starts at offset four

246
00:10:16,920 --> 00:10:21,959
but that offset four would be down here

247
00:10:19,200 --> 00:10:24,899
so we need to skip one two bytes forward

248
00:10:21,959 --> 00:10:29,399
right two bytes starting here at offset

249
00:10:24,899 --> 00:10:32,040
six so offset six from The Base address

250
00:10:29,399 --> 00:10:35,399
that we figured out for memory map.io

251
00:10:32,040 --> 00:10:36,540
which is

252
00:10:35,399 --> 00:10:39,360
fed01000

253
00:10:36,540 --> 00:10:45,839
so in terms of registers that we need we

254
00:10:39,360 --> 00:10:49,440
have you know SPI SPIBAR 0 equals 

255
00:10:45,839 --> 00:10:52,620
hex fed01000 the

256
00:10:49,440 --> 00:10:54,720
flash status and control register

257
00:10:52,620 --> 00:10:57,300
so we're just going to call it you know

258
00:10:54,720 --> 00:11:03,300
F control even though that's not

259
00:10:57,300 --> 00:11:05,940
accurate is that hex fed 0 1 0 0 6 and

260
00:11:03,300 --> 00:11:10,740
we want to do two bytes

261
00:11:05,940 --> 00:11:12,420
and then the flash address register is

262
00:11:10,740 --> 00:11:14,820
going to be the other important register

263
00:11:12,420 --> 00:11:17,399
right so we need to set the address that

264
00:11:14,820 --> 00:11:20,940
we want to read from so that is at

265
00:11:17,399 --> 00:11:24,320
offset eight so offset 8 should be the

266
00:11:20,940 --> 00:11:24,320
address so we're going to

267
00:11:25,019 --> 00:11:31,140
set that equal to

268
00:11:27,660 --> 00:11:35,640
hex 10 so that we can read from that

269
00:11:31,140 --> 00:11:41,120
magic signature at offset 10. so X fed

270
00:11:35,640 --> 00:11:41,120
0 1 0 1 0.

271
00:11:48,600 --> 00:11:56,940
that's the address and that's four bytes

272
00:11:52,620 --> 00:11:58,560
and we want to set that equal to X10 and

273
00:11:56,940 --> 00:12:00,480
we want to set that control register

274
00:11:58,560 --> 00:12:06,060
equal to hex

275
00:12:00,480 --> 00:12:08,279
0 f what was it again 0 f 0 1.

276
00:12:06,060 --> 00:12:09,660
all right let's go shrink it down so we

277
00:12:08,279 --> 00:12:11,880
can see it all

278
00:12:09,660 --> 00:12:14,279
okay so this is what we have in order to

279
00:12:11,880 --> 00:12:16,560
cause a manual memory mapped IO flash

280
00:12:14,279 --> 00:12:18,800
transaction we only have the following

281
00:12:16,560 --> 00:12:22,140
things we need to set the flash address

282
00:12:18,800 --> 00:12:26,100
at this memory mapped address to 10 and

283
00:12:22,140 --> 00:12:27,660
then we set this value at fed 0 1006 and

284
00:12:26,100 --> 00:12:30,959
that will say we want to do a

285
00:12:27,660 --> 00:12:34,500
retransaction these bits for a read

286
00:12:30,959 --> 00:12:36,959
transaction this bit to go and these

287
00:12:34,500 --> 00:12:40,560
bits to say what the size is so 15 plus

288
00:12:36,959 --> 00:12:43,079
one so a 16-bit read okay so let's see

289
00:12:40,560 --> 00:12:45,180
if we can go ahead and do a manual right

290
00:12:43,079 --> 00:12:47,880
of these values and cause a flash

291
00:12:45,180 --> 00:12:50,940
transaction that is same so before that

292
00:12:47,880 --> 00:12:54,680
let's go ahead and do a a Chipsec

293
00:12:50,940 --> 00:12:57,240
command in order to get rid of the

294
00:12:54,680 --> 00:12:59,100
values that are currently there so that

295
00:12:57,240 --> 00:13:01,380
we don't so that we can confirm that

296
00:12:59,100 --> 00:13:05,639
we've actually overwritten them so

297
00:13:01,380 --> 00:13:09,480
instead let's do a SPI flash read from

298
00:13:05,639 --> 00:13:11,880
address 0 of 16 bits

299
00:13:09,480 --> 00:13:13,920
16 bytes rather

300
00:13:11,880 --> 00:13:16,260
so we're going to read

301
00:13:13,920 --> 00:13:19,860
flash address zero right flash address

302
00:13:16,260 --> 00:13:22,019
zero and we're reading 16 bytes so let's

303
00:13:19,860 --> 00:13:24,480
look at our test.bin

304
00:13:22,019 --> 00:13:28,980
okay all F's exactly like we would

305
00:13:24,480 --> 00:13:30,300
expect because offset 0 has 16 bytes of

306
00:13:28,980 --> 00:13:33,600
F's

307
00:13:30,300 --> 00:13:35,519
so now let's do our transaction so now

308
00:13:33,600 --> 00:13:38,160
we need to actually be doing memory map

309
00:13:35,519 --> 00:13:41,160
dial and while there is a dedicated

310
00:13:38,160 --> 00:13:43,139
command for memory mapped IO in Chipsec

311
00:13:41,160 --> 00:13:46,320
it's for if it you know knows the Base

312
00:13:43,139 --> 00:13:47,820
address for like a bar like SPIBAR but

313
00:13:46,320 --> 00:13:49,740
that doesn't work on this particular

314
00:13:47,820 --> 00:13:52,860
system so that's why I have to use you

315
00:13:49,740 --> 00:13:55,380
know the manual addresses so I need to

316
00:13:52,860 --> 00:13:57,420
go to Chipsec util and now I just want

317
00:13:55,380 --> 00:13:58,860
to use the memory command I just want to

318
00:13:57,420 --> 00:14:01,620
figure out how do I read and write from

319
00:13:58,860 --> 00:14:06,500
memory which will in fact be Memory map

320
00:14:01,620 --> 00:14:06,500
diode so memory help

321
00:14:06,540 --> 00:14:13,500
and the form is chips like you tell mem

322
00:14:09,660 --> 00:14:16,500
read a physical address a length and a

323
00:14:13,500 --> 00:14:18,240
value or a file so file when we're

324
00:14:16,500 --> 00:14:20,940
reading to a file and value when we're

325
00:14:18,240 --> 00:14:24,300
writing to it so I'm going to use this

326
00:14:20,940 --> 00:14:28,079
write value form here and I'm going to

327
00:14:24,300 --> 00:14:31,560
write this value so adjusting that

328
00:14:28,079 --> 00:14:33,240
slightly so I'm going to write 0f so

329
00:14:31,560 --> 00:14:35,700
first I need to write the address into

330
00:14:33,240 --> 00:14:38,760
the F adder and then I will write this

331
00:14:35,700 --> 00:14:42,540
because this will cause it to go so I'm

332
00:14:38,760 --> 00:14:45,360
going to do chips like util mem

333
00:14:42,540 --> 00:14:49,920
right Bell

334
00:14:45,360 --> 00:14:53,899
physical address is hex fed 0 1

335
00:14:49,920 --> 00:14:53,899
0 1 0.

336
00:14:56,120 --> 00:15:02,279
and then the length is going to be four

337
00:15:00,360 --> 00:15:04,380
bytes

338
00:15:02,279 --> 00:15:06,899
and I guess in this form I need to give

339
00:15:04,380 --> 00:15:08,820
it as DWORD instead of four

340
00:15:06,899 --> 00:15:10,500
and then I need a specific value that

341
00:15:08,820 --> 00:15:13,920
I'm going to write which is going to be

342
00:15:10,500 --> 00:15:15,980
hex 10. this is the flash address that I

343
00:15:13,920 --> 00:15:20,180
want to read from

344
00:15:15,980 --> 00:15:20,180
all right so I write that value

345
00:15:20,399 --> 00:15:28,500
and then I'm going to write the value

346
00:15:23,779 --> 00:15:31,320
0f01 to offset 6. so change this to

347
00:15:28,500 --> 00:15:37,699
offset 6 change this from a DWORD to a

348
00:15:31,320 --> 00:15:37,699
word and change this to zero f 0 1.

349
00:15:45,120 --> 00:15:50,459
let's end in a classic simple mistake I

350
00:15:47,940 --> 00:15:53,279
messed up the address of the flash

351
00:15:50,459 --> 00:15:56,459
address field so that's out of set 8 not

352
00:15:53,279 --> 00:16:00,180
offset 10. so therefore let's go ahead

353
00:15:56,459 --> 00:16:03,480
and fix that well that should be 0 8 and

354
00:16:00,180 --> 00:16:06,600
now this should work instead so

355
00:16:03,480 --> 00:16:09,180
that's why one has to be careful with

356
00:16:06,600 --> 00:16:11,699
doing manual memory map dial

357
00:16:09,180 --> 00:16:15,180
all right so wrote that now I'm going to

358
00:16:11,699 --> 00:16:19,019
write my flash go and I want to see that

359
00:16:15,180 --> 00:16:21,740
that actually gets caught over here so I

360
00:16:19,019 --> 00:16:21,740
want to do it again

361
00:16:22,920 --> 00:16:28,980
and you can see it triggered correctly

362
00:16:24,839 --> 00:16:32,339
this time and it did a fast read at

363
00:16:28,980 --> 00:16:36,540
address 10 with the dummy cycles and 5A

364
00:16:32,339 --> 00:16:38,759
A5 F 0 0 f so successfully caused a

365
00:16:36,540 --> 00:16:40,620
flash transaction to occur and now we

366
00:16:38,759 --> 00:16:45,720
know that you know behind the scenes the

367
00:16:40,620 --> 00:16:47,639
Intel system is doing a fast read when

368
00:16:45,720 --> 00:16:49,740
you poke that memory mapped IO in order

369
00:16:47,639 --> 00:16:51,600
to cause a wreath so this is basically

370
00:16:49,740 --> 00:16:54,120
what we wanted to show in the

371
00:16:51,600 --> 00:16:55,740
architecture 4001 class you know we

372
00:16:54,120 --> 00:16:57,420
learn about these flash data registers

373
00:16:55,740 --> 00:17:00,060
and we just treat them as a black box

374
00:16:57,420 --> 00:17:02,579
you know we just say you know set some

375
00:17:00,060 --> 00:17:04,740
particular thing in the control register

376
00:17:02,579 --> 00:17:07,679
set some particular thing in the data

377
00:17:04,740 --> 00:17:09,419
register and that will all lead to it

378
00:17:07,679 --> 00:17:11,819
doing a flash transaction and we just

379
00:17:09,419 --> 00:17:13,799
treated black box now you can see behind

380
00:17:11,819 --> 00:17:15,959
the scenes when you you know write into

381
00:17:13,799 --> 00:17:18,540
these things it will actually cause

382
00:17:15,959 --> 00:17:21,839
flange transactions that you can sniff

383
00:17:18,540 --> 00:17:23,880
with a logic analyzer and see how the

384
00:17:21,839 --> 00:17:25,500
system is actually behaving so at this

385
00:17:23,880 --> 00:17:27,540
point now you could start to do things

386
00:17:25,500 --> 00:17:29,640
like software sequencing which is

387
00:17:27,540 --> 00:17:31,200
something we don't cover at all in the

388
00:17:29,640 --> 00:17:33,540
architecture 4001 would you say it

389
00:17:31,200 --> 00:17:35,160
exists and hand wave past it so the

390
00:17:33,540 --> 00:17:36,660
Intel systems have the notion of

391
00:17:35,160 --> 00:17:39,120
Hardware sequencing or software

392
00:17:36,660 --> 00:17:41,580
sequencing and Hardware sequencing is

393
00:17:39,120 --> 00:17:43,799
where you just use these exactly as is

394
00:17:41,580 --> 00:17:45,660
and you poke them and you let the

395
00:17:43,799 --> 00:17:48,179
hardware figure out you know Hardware

396
00:17:45,660 --> 00:17:50,340
sequencing what kind of flash command

397
00:17:48,179 --> 00:17:51,840
it's going to send behind the scenes but

398
00:17:50,340 --> 00:17:54,240
there's also the notion of software

399
00:17:51,840 --> 00:17:57,179
sequencing where it will actually send

400
00:17:54,240 --> 00:17:59,340
exactly the type of flash command that

401
00:17:57,179 --> 00:18:01,760
you want when you want more control over

402
00:17:59,340 --> 00:18:05,039
things so there you go I hope that we've

403
00:18:01,760 --> 00:18:07,700
demystified and gone below this memory

404
00:18:05,039 --> 00:18:07,700
mapped IO

