1
00:00:00,179 --> 00:00:05,640
so again I'm only using a serial port on

2
00:00:03,179 --> 00:00:07,200
my system because I want to record on a

3
00:00:05,640 --> 00:00:08,760
single screen so you can see everything

4
00:00:07,200 --> 00:00:12,000
as I'm flipping back and forth between

5
00:00:08,760 --> 00:00:14,099
documentation you should boot into your

6
00:00:12,000 --> 00:00:16,500
system under test whether it's booting

7
00:00:14,099 --> 00:00:18,480
into Windows or Linux or an EFI shell on

8
00:00:16,500 --> 00:00:20,580
your own system and then just use either

9
00:00:18,480 --> 00:00:22,439
an external Monitor and keyboard or if

10
00:00:20,580 --> 00:00:25,140
it's a laptop use the built-in

11
00:00:22,439 --> 00:00:27,539
so on my system the USB serial port

12
00:00:25,140 --> 00:00:30,779
shows up as com5 so I connect to that

13
00:00:27,539 --> 00:00:33,780
via Putty so I load the settings com5

14
00:00:30,779 --> 00:00:37,800
at 11 5200

15
00:00:33,780 --> 00:00:39,420
open that up now it is connected but the

16
00:00:37,800 --> 00:00:41,460
device is not powered so I'm going to go

17
00:00:39,420 --> 00:00:45,600
ahead and power it on and then you'll

18
00:00:41,460 --> 00:00:48,300
see that it will boot to a UEFI shell

19
00:00:45,600 --> 00:00:52,440
and because I have that USB stick

20
00:00:48,300 --> 00:00:55,020
installed I can go to that

21
00:00:52,440 --> 00:00:57,719
and specifically I'm going to go in and

22
00:00:55,020 --> 00:00:59,100
use Chipsec I just had an old USB stick

23
00:00:57,719 --> 00:01:00,719
lying around that already has it on

24
00:00:59,100 --> 00:01:01,980
there so I didn't bother to upgrade to

25
00:01:00,719 --> 00:01:04,019
the latest version

26
00:01:01,980 --> 00:01:06,720
what Chipsec is going to allow us to do

27
00:01:04,019 --> 00:01:09,299
is first do some sanity checking to

28
00:01:06,720 --> 00:01:11,520
confirm information about the capability

29
00:01:09,299 --> 00:01:13,619
to dump flash and what we see on flash

30
00:01:11,520 --> 00:01:16,140
and so forth and then we'll use

31
00:01:13,619 --> 00:01:18,600
information derived from Chipsec to find

32
00:01:16,140 --> 00:01:20,159
the specific memory map IO ranges for

33
00:01:18,600 --> 00:01:23,580
our particular system

34
00:01:20,159 --> 00:01:25,140
so to do this you need a UEFI shell with

35
00:01:23,580 --> 00:01:28,340
the python and Chipsec and all the

36
00:01:25,140 --> 00:01:28,340
prerequisites installed

37
00:01:31,439 --> 00:01:35,640
again architecture 4001 talks about

38
00:01:33,479 --> 00:01:37,079
other ways to poke the memory map diode

39
00:01:35,640 --> 00:01:38,040
what we're going to do it this way for

40
00:01:37,079 --> 00:01:42,900
this class

41
00:01:38,040 --> 00:01:44,579
so we use Python and then Chipsec util

42
00:01:42,900 --> 00:01:46,439
and then you can always kind of just

43
00:01:44,579 --> 00:01:48,540
throw a help on the end of anything if

44
00:01:46,439 --> 00:01:50,700
you need help so if we just run Chipsec

45
00:01:48,540 --> 00:01:53,340
util help it'll first give us sort of

46
00:01:50,700 --> 00:01:57,380
all the available commands so we've got

47
00:01:53,340 --> 00:02:01,200
ACPI commands IO commands iommu memory

48
00:01:57,380 --> 00:02:04,200
PCI and so forth so we're going to start

49
00:02:01,200 --> 00:02:06,119
with the SPI capability and we're just

50
00:02:04,200 --> 00:02:08,459
going to you know confirm that we can

51
00:02:06,119 --> 00:02:10,619
manually caused by transactions with

52
00:02:08,459 --> 00:02:13,739
Chipsec and we can watch those

53
00:02:10,619 --> 00:02:18,360
transactions occur in the logic analyzer

54
00:02:13,739 --> 00:02:20,819
so to do that we do Chipsec util SPI and

55
00:02:18,360 --> 00:02:22,739
then help to find out what the command

56
00:02:20,819 --> 00:02:24,900
usage is

57
00:02:22,739 --> 00:02:27,180
and so we could dump the entire flash

58
00:02:24,900 --> 00:02:29,580
chip with this command but we want to

59
00:02:27,180 --> 00:02:32,040
instead just do a single read of a

60
00:02:29,580 --> 00:02:33,720
particular region and see what sort of

61
00:02:32,040 --> 00:02:36,660
transaction gets created on the back end

62
00:02:33,720 --> 00:02:39,000
so Chipsec util behind the scenes is

63
00:02:36,660 --> 00:02:41,940
ultimately going to be causing memory

64
00:02:39,000 --> 00:02:43,800
mapped IO transactions but let's just

65
00:02:41,940 --> 00:02:45,360
go ahead and run one of these commands

66
00:02:43,800 --> 00:02:49,500
and see what we get

67
00:02:45,360 --> 00:02:51,900
so we're going to do SPI read

68
00:02:49,500 --> 00:02:54,120
the first thing is the flash address to

69
00:02:51,900 --> 00:02:55,379
read from and the next thing is the

70
00:02:54,120 --> 00:02:57,840
length and then we're going to need to

71
00:02:55,379 --> 00:02:59,819
Output it to a file so the flash address

72
00:02:57,840 --> 00:03:03,300
we want to read from let's say that we

73
00:02:59,819 --> 00:03:05,819
want to read from offset 16. and so that

74
00:03:03,300 --> 00:03:10,200
is where we said is the Magic flash

75
00:03:05,819 --> 00:03:12,239
descriptor value that the PCH that the

76
00:03:10,200 --> 00:03:14,760
Intel Hardware expects to see if it's

77
00:03:12,239 --> 00:03:17,519
going to behave as a descriptor mode

78
00:03:14,760 --> 00:03:19,980
flash descriptor mode system

79
00:03:17,519 --> 00:03:21,659
so let's go ahead and read from that and

80
00:03:19,980 --> 00:03:24,060
we expect to see that particular magic

81
00:03:21,659 --> 00:03:27,000
signature there let's read a total of

82
00:03:24,060 --> 00:03:30,420
four bytes and let's output it to a file

83
00:03:27,000 --> 00:03:33,900
we're just going to call test.bin

84
00:03:30,420 --> 00:03:35,700
so we do that Chipsec goes we're then

85
00:03:33,900 --> 00:03:39,080
going to you have to use the hex edit

86
00:03:35,700 --> 00:03:40,980
command to look at our file so exit it

87
00:03:39,080 --> 00:03:47,099
test.bin

88
00:03:40,980 --> 00:03:48,659
and indeed we see 5A A5 F 0 0 f great so

89
00:03:47,099 --> 00:03:50,580
that confirms that we can successfully

90
00:03:48,659 --> 00:03:52,200
use Chipsec to read but we really want

91
00:03:50,580 --> 00:03:54,060
to use you know memory mapped IO at the

92
00:03:52,200 --> 00:03:56,040
end of the day so we'll get there but

93
00:03:54,060 --> 00:03:58,500
first let's actually confirm that we can

94
00:03:56,040 --> 00:04:01,260
see in the logic analyzer that this read

95
00:03:58,500 --> 00:04:04,500
command is something that we can observe

96
00:04:01,260 --> 00:04:06,780
as just a singular read command

97
00:04:04,500 --> 00:04:09,420
so to do that we just go ahead and start

98
00:04:06,780 --> 00:04:11,220
our logic analyzer actually let me

99
00:04:09,420 --> 00:04:12,840
change some configuration right now so

100
00:04:11,220 --> 00:04:15,120
I'm going to use the the looping that we

101
00:04:12,840 --> 00:04:17,100
used before so I'm going to start the

102
00:04:15,120 --> 00:04:19,979
logic analyzer I'm going to go to back

103
00:04:17,100 --> 00:04:22,019
to Chipsec I'm going to hit enter I'm

104
00:04:19,979 --> 00:04:24,240
going to zoom out here in the logic

105
00:04:22,019 --> 00:04:27,060
analyzer and there we see that you know

106
00:04:24,240 --> 00:04:29,880
some sort of transaction occurred and I

107
00:04:27,060 --> 00:04:32,759
specifically added an analyzer to this I

108
00:04:29,880 --> 00:04:34,500
added the QSPI analyzer because I

109
00:04:32,759 --> 00:04:36,780
basically want to you know see there's

110
00:04:34,500 --> 00:04:38,220
all these little blips but the QSPI

111
00:04:36,780 --> 00:04:40,560
analyzer will only kind of show us

112
00:04:38,220 --> 00:04:43,080
something where there's a real and valid

113
00:04:40,560 --> 00:04:45,060
transaction so that helps me you know

114
00:04:43,080 --> 00:04:47,400
narrow in on aware exactly this is

115
00:04:45,060 --> 00:04:52,199
amongst all the other little blips here

116
00:04:47,400 --> 00:04:55,740
and so indeed we can see that this is if

117
00:04:52,199 --> 00:04:58,740
we zoom in all the way this is a dual I

118
00:04:55,740 --> 00:05:02,520
o fast read command BB and then the

119
00:04:58,740 --> 00:05:04,680
address is going to be 10. so 10 this

120
00:05:02,520 --> 00:05:07,080
because it's a fast read it's 10 in

121
00:05:04,680 --> 00:05:09,300
parallel here and then there's dummy

122
00:05:07,080 --> 00:05:11,100
cycles and the actual data coming back

123
00:05:09,300 --> 00:05:16,620
and that's exactly the data we expect

124
00:05:11,100 --> 00:05:19,620
you know it's exactly this 5A A5 F 0 0 f

125
00:05:16,620 --> 00:05:22,320
that we expect to see so cool this

126
00:05:19,620 --> 00:05:25,080
confirms that using Chipsec we can cause

127
00:05:22,320 --> 00:05:27,360
a flash transaction and see it in the

128
00:05:25,080 --> 00:05:29,160
analyzer so now I want to do one more

129
00:05:27,360 --> 00:05:32,039
test I want to change that from Reading

130
00:05:29,160 --> 00:05:34,500
4 bytes to 16 bytes and I want to change

131
00:05:32,039 --> 00:05:36,000
the way that we capture the data to a

132
00:05:34,500 --> 00:05:38,100
triggered capture instead of a looping

133
00:05:36,000 --> 00:05:39,620
track capture so looping just kind of

134
00:05:38,100 --> 00:05:43,020
sits there and watches stuff forever

135
00:05:39,620 --> 00:05:45,780
triggered capture can be only triggering

136
00:05:43,020 --> 00:05:48,660
the capture after it sees something like

137
00:05:45,780 --> 00:05:50,820
a rising edge or a falling edge Etc so

138
00:05:48,660 --> 00:05:52,919
because we know that this is a active

139
00:05:50,820 --> 00:05:55,020
low chip select thing and it's always

140
00:05:52,919 --> 00:05:58,979
going to go low before these clock

141
00:05:55,020 --> 00:06:02,039
 cycles start yielding valid valid data

142
00:05:58,979 --> 00:06:04,080
we can trigger based on the chip select

143
00:06:02,039 --> 00:06:06,539
going low and that'll make it so that it

144
00:06:04,080 --> 00:06:08,759
only captures the data after that and we

145
00:06:06,539 --> 00:06:12,419
can set a capture duration

146
00:06:08,759 --> 00:06:13,919
can also trim the pre-trigger data some

147
00:06:12,419 --> 00:06:15,900
reason I don't think that actually works

148
00:06:13,919 --> 00:06:18,600
but I'm going to set it anyways just to

149
00:06:15,900 --> 00:06:22,620
see what happens so go ahead and do that

150
00:06:18,600 --> 00:06:25,979
go back to Chipsec change this to be a

151
00:06:22,620 --> 00:06:28,440
16 byte read instead of a four byte read

152
00:06:25,979 --> 00:06:31,800
cause the transaction to occur

153
00:06:28,440 --> 00:06:34,800
behind the scenes boom this successfully

154
00:06:31,800 --> 00:06:36,840
saw the capture and then it did

155
00:06:34,800 --> 00:06:38,400
successfully trim so good it was just my

156
00:06:36,840 --> 00:06:40,560
other system that that wasn't working on

157
00:06:38,400 --> 00:06:42,600
so there we go this just jumped directly

158
00:06:40,560 --> 00:06:45,060
to only the data we care about this time

159
00:06:42,600 --> 00:06:48,479
you can see it's still address 10.

160
00:06:45,060 --> 00:06:51,539
foreign but we actually only see four

161
00:06:48,479 --> 00:06:53,220
bytes of data here so what gives well if

162
00:06:51,539 --> 00:06:55,620
we go back and see what this is saying

163
00:06:53,220 --> 00:06:58,979
it's saying that it's reading 16 bytes

164
00:06:55,620 --> 00:07:02,039
in four byte chunks so that's completely

165
00:06:58,979 --> 00:07:03,660
unnecessary and wrong in some sense but

166
00:07:02,039 --> 00:07:05,699
that just happens to be the way that

167
00:07:03,660 --> 00:07:07,319
Chipsec does it you notice that's not

168
00:07:05,699 --> 00:07:10,220
the way that it behaves when the system

169
00:07:07,319 --> 00:07:10,220
is actually booting

170
00:07:21,120 --> 00:07:26,400
so let's go ahead and try a capture

171
00:07:24,240 --> 00:07:29,099
without the trigger where we just go

172
00:07:26,400 --> 00:07:32,280
ahead and capture forever and see if we

173
00:07:29,099 --> 00:07:34,759
can see four four byte values being read

174
00:07:32,280 --> 00:07:34,759
instead

175
00:07:35,819 --> 00:07:39,660
create Zuma

176
00:07:47,759 --> 00:07:52,680
and there we go we've got four four by

177
00:07:50,280 --> 00:07:55,020
two reads although you know they really

178
00:07:52,680 --> 00:07:57,180
should have just done 16 byte read but

179
00:07:55,020 --> 00:07:58,860
for whatever reason this is done as four

180
00:07:57,180 --> 00:08:00,780
flow byte rates some of those are the

181
00:07:58,860 --> 00:08:02,280
first four bytes we can follow to the

182
00:08:00,780 --> 00:08:04,680
next trigger

183
00:08:02,280 --> 00:08:08,160
and these are the next four bytes zero

184
00:08:04,680 --> 00:08:11,699
three zero zero zero four zero zero

185
00:08:08,160 --> 00:08:14,639
so looking at that in the hex dump

186
00:08:11,699 --> 00:08:18,080
so threes or zeros or four zero zero so

187
00:08:14,639 --> 00:08:18,080
that's exactly the data we expect

