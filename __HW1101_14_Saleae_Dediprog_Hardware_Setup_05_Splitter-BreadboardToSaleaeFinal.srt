1
00:00:00,000 --> 00:00:04,140
okay so after all of that you should

2
00:00:01,860 --> 00:00:05,759
have this nice big Tangled mess of wires

3
00:00:04,140 --> 00:00:08,000
but what you should ultimately have is

4
00:00:05,759 --> 00:00:11,160
your Saleae connected to the breadboard

5
00:00:08,000 --> 00:00:14,099
connected to the Dediprog connected to

6
00:00:11,160 --> 00:00:16,740
the chip clip or whatever SPI flash chip

7
00:00:14,099 --> 00:00:18,119
you're connected to so the important

8
00:00:16,740 --> 00:00:20,520
thing here is that if you tried to use

9
00:00:18,119 --> 00:00:23,220
the convention from previously in the

10
00:00:20,520 --> 00:00:25,380
class where you made this pin this

11
00:00:23,220 --> 00:00:28,140
connector one from Saleae connector to

12
00:00:25,380 --> 00:00:29,820
connector three skip connector 4 and

13
00:00:28,140 --> 00:00:31,859
connect it to ground that's important

14
00:00:29,820 --> 00:00:33,420
make sure you're connected to the proper

15
00:00:31,859 --> 00:00:35,640
ground that runs underneath these

16
00:00:33,420 --> 00:00:38,579
connectors and then connector five to

17
00:00:35,640 --> 00:00:40,680
five six to six seven to seven then if

18
00:00:38,579 --> 00:00:42,180
you've only got two of these connected

19
00:00:40,680 --> 00:00:44,579
then you're going to have run out of

20
00:00:42,180 --> 00:00:47,100
things when you want to connect to pin 8

21
00:00:44,579 --> 00:00:48,539
which is the power so you can resolve

22
00:00:47,100 --> 00:00:50,219
that however you want you could plug in

23
00:00:48,539 --> 00:00:52,620
another thing and connect eight to eight

24
00:00:50,219 --> 00:00:54,420
I chose to just reuse the zero that I

25
00:00:52,620 --> 00:00:56,460
wasn't using before so I've got zero to

26
00:00:54,420 --> 00:00:58,079
eight but again you can do whatever you

27
00:00:56,460 --> 00:00:59,760
want here then of course after getting

28
00:00:58,079 --> 00:01:02,640
all of this connected you want to make

29
00:00:59,760 --> 00:01:04,799
sure that your Dediprog is still able

30
00:01:02,640 --> 00:01:07,320
to read the known good SPI flash chip

31
00:01:04,799 --> 00:01:09,119
and as long as it is then you've got all

32
00:01:07,320 --> 00:01:12,240
your electrical connections good and so

33
00:01:09,119 --> 00:01:14,220
then we can use the Saleae to watch and

34
00:01:12,240 --> 00:01:16,439
monitor the SPI flash traffic as the

35
00:01:14,220 --> 00:01:18,600
Dediprog is actually attempting to

36
00:01:16,439 --> 00:01:20,900
identify the chip before it starts

37
00:01:18,600 --> 00:01:20,900
reading it

