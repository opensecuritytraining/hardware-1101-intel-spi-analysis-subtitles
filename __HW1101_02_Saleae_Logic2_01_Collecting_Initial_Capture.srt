1
00:00:00,299 --> 00:00:04,680
now once you have your logic probes

2
00:00:02,159 --> 00:00:07,859
connected up to your device under test

3
00:00:04,680 --> 00:00:10,080
and your Saleae you're going to open the

4
00:00:07,859 --> 00:00:12,360
Logic 2 application on whichever

5
00:00:10,080 --> 00:00:14,219
operating system you're using first

6
00:00:12,360 --> 00:00:18,660
you're going to click on the device

7
00:00:14,219 --> 00:00:21,060
settings and recall that I skipped the

8
00:00:18,660 --> 00:00:22,260
probe 0 because it was black and I

9
00:00:21,060 --> 00:00:24,119
didn't want to confuse myself with

10
00:00:22,260 --> 00:00:26,519
ground so the things that I'm going to

11
00:00:24,119 --> 00:00:29,220
want enabled are one which was connected

12
00:00:26,519 --> 00:00:31,920
to one two which is connected to two

13
00:00:29,220 --> 00:00:34,920
three I skipped four because it was

14
00:00:31,920 --> 00:00:38,160
connected to ground and then I used 5 6

15
00:00:34,920 --> 00:00:40,140
and 7. so we don't need zero here and we

16
00:00:38,160 --> 00:00:42,600
just need one two three five six and

17
00:00:40,140 --> 00:00:45,480
seven and then what you need to do is

18
00:00:42,600 --> 00:00:47,340
you need to name these so by default

19
00:00:45,480 --> 00:00:49,980
these are going to be you know ordered

20
00:00:47,340 --> 00:00:52,559
correctly for you and you need to find

21
00:00:49,980 --> 00:00:54,600
the corresponding names in your data

22
00:00:52,559 --> 00:00:55,500
sheet so we need to open up your data

23
00:00:54,600 --> 00:00:57,360
sheet

24
00:00:55,500 --> 00:00:58,820
so my data sheet for this device under

25
00:00:57,360 --> 00:01:02,100
test is the

26
00:00:58,820 --> 00:01:06,479
 Winbond 25Q128JW and notice that it says

27
00:01:02,100 --> 00:01:09,299
here it's a 1.8 volt part so typical SPI

28
00:01:06,479 --> 00:01:12,180
flash chip voltages are either 1.8 volts

29
00:01:09,299 --> 00:01:14,760
or 3.3 volts so make sure that the

30
00:01:12,180 --> 00:01:16,619
appropriate voltage is selected in your

31
00:01:14,760 --> 00:01:18,360
device settings depending on which chip

32
00:01:16,619 --> 00:01:20,640
you're using and which data sheet you're

33
00:01:18,360 --> 00:01:24,000
looking at and then also right here you

34
00:01:20,640 --> 00:01:27,360
select how often the system is sampled

35
00:01:24,000 --> 00:01:29,939
so 500 million samples per second 250

36
00:01:27,360 --> 00:01:31,979
million and so forth 250 should be fine

37
00:01:29,939 --> 00:01:34,680
for the speed we're operating at if you

38
00:01:31,979 --> 00:01:36,240
need you can up it to you know 500 but

39
00:01:34,680 --> 00:01:39,659
typically that's not going to be

40
00:01:36,240 --> 00:01:41,400
necessary so again we need to name these

41
00:01:39,659 --> 00:01:43,680
channels which will you know default to

42
00:01:41,400 --> 00:01:45,540
just channel one channel two Etc we need

43
00:01:43,680 --> 00:01:48,780
to name them according to our data

44
00:01:45,540 --> 00:01:49,979
sheets so we need to go see what this

45
00:01:48,780 --> 00:01:52,079
was set to

46
00:01:49,979 --> 00:01:54,720
I'm gonna scroll down until you find the

47
00:01:52,079 --> 00:01:56,460
physical configuration and so again I

48
00:01:54,720 --> 00:01:59,340
you know specifically connected probe

49
00:01:56,460 --> 00:02:01,020
one to channel one to pin one just so

50
00:01:59,340 --> 00:02:03,479
that I would know exactly and you know

51
00:02:01,020 --> 00:02:05,460
make it easier to not confuse myself so

52
00:02:03,479 --> 00:02:07,320
this is the chip select this is the

53
00:02:05,460 --> 00:02:09,360
active low that's what the little slash

54
00:02:07,320 --> 00:02:11,580
means it means it's an active low signal

55
00:02:09,360 --> 00:02:14,220
so this is chip select and the chip is

56
00:02:11,580 --> 00:02:17,099
selected when it is at low voltage we've

57
00:02:14,220 --> 00:02:23,160
got data out we've got write protect

58
00:02:17,099 --> 00:02:24,300
ground data in clock and hold or IO3 so

59
00:02:23,160 --> 00:02:27,060
we said that you know there's many

60
00:02:24,300 --> 00:02:28,560
different names for these data in data

61
00:02:27,060 --> 00:02:31,860
out and so forth

62
00:02:28,560 --> 00:02:35,040
so the Cs I'm going to name that slash

63
00:02:31,860 --> 00:02:36,420
CS for active low chip select I'm also

64
00:02:35,040 --> 00:02:38,879
going to put enable here because later

65
00:02:36,420 --> 00:02:41,459
on there's going to be an analyzer that

66
00:02:38,879 --> 00:02:42,959
uses the term enable so this just makes

67
00:02:41,459 --> 00:02:43,980
it quick and easy for me to find the

68
00:02:42,959 --> 00:02:46,500
enable line

69
00:02:43,980 --> 00:02:49,500
then we have peripheral out controller

70
00:02:46,500 --> 00:02:53,220
in so this was called so D2 is called

71
00:02:49,500 --> 00:02:56,220
 data out or IO1

72
00:02:53,220 --> 00:02:59,459
and so we're calling it IO1 it's also

73
00:02:56,220 --> 00:03:02,459
known as peripheral out controller in or

74
00:02:59,459 --> 00:03:04,440
the Legacy term of MISO master in slave

75
00:03:02,459 --> 00:03:06,900
out so we've had that terms deprecated

76
00:03:04,440 --> 00:03:09,060
but the analyzers again we'll call it

77
00:03:06,900 --> 00:03:11,099
 MISO and so I'm putting it there just to

78
00:03:09,060 --> 00:03:13,440
make it easier to find it when I'm asked

79
00:03:11,099 --> 00:03:16,200
to select different lines so the slash

80
00:03:13,440 --> 00:03:21,360
WP the write protect line also known as

81
00:03:16,200 --> 00:03:23,580
IO2 then we have D5 this was data in so

82
00:03:21,360 --> 00:03:27,480
that data in would be peripheral in

83
00:03:23,580 --> 00:03:32,280
controller out or Master out slave in so

84
00:03:27,480 --> 00:03:37,140
MOSI or PICO or IO0 and D6 is sorry

85
00:03:32,280 --> 00:03:40,860
Channel 6 is clock and pin 7 is hold

86
00:03:37,140 --> 00:03:43,140
active low or IO3 so now that we've got

87
00:03:40,860 --> 00:03:45,420
all of those selected what we can do is

88
00:03:43,140 --> 00:03:47,400
we can just go ahead and Power on our

89
00:03:45,420 --> 00:03:50,099
device we're going to see the data start

90
00:03:47,400 --> 00:03:52,260
streaming in here so to do that I hit

91
00:03:50,099 --> 00:03:54,360
this start button and then you'll see

92
00:03:52,260 --> 00:03:57,120
the lines start analyzing and I scrolled

93
00:03:54,360 --> 00:03:58,440
out to see that this data is not

94
00:03:57,120 --> 00:04:00,840
changing right now because the device is

95
00:03:58,440 --> 00:04:03,840
not powered on so I'm going to power it

96
00:04:00,840 --> 00:04:05,519
on and there you can immediately see all

97
00:04:03,840 --> 00:04:07,440
sorts of data coming in and it's

98
00:04:05,519 --> 00:04:10,140
oscillating between high and low so

99
00:04:07,440 --> 00:04:13,439
quickly then it just looks like a you

100
00:04:10,140 --> 00:04:14,640
know blob so then we go ahead and hit

101
00:04:13,439 --> 00:04:16,859
stop

102
00:04:14,640 --> 00:04:18,479
and then we can trim our data so I'm

103
00:04:16,859 --> 00:04:21,180
going to go ahead and click at the top

104
00:04:18,479 --> 00:04:23,580
here I'm going to right click and say

105
00:04:21,180 --> 00:04:25,560
delete data before the marker because

106
00:04:23,580 --> 00:04:27,660
this is all just nothing going on there

107
00:04:25,560 --> 00:04:29,160
and I'm going to click after the last

108
00:04:27,660 --> 00:04:32,340
data and then I'm going to right click

109
00:04:29,160 --> 00:04:34,800
and say delete data after the marker and

110
00:04:32,340 --> 00:04:37,620
so now this is restricted just to the

111
00:04:34,800 --> 00:04:40,320
range of information that looks valid

112
00:04:37,620 --> 00:04:42,419
and so if I scroll in on this you know I

113
00:04:40,320 --> 00:04:44,220
could scroll in on the very first thing

114
00:04:42,419 --> 00:04:45,900
sorry my scroll is too fast here I'm

115
00:04:44,220 --> 00:04:47,699
going to switch to my other mouse that

116
00:04:45,900 --> 00:04:50,460
Scrolls more slowly

117
00:04:47,699 --> 00:04:52,560
if I scroll in on this one

118
00:04:50,460 --> 00:04:54,660
we can see that there is not actually

119
00:04:52,560 --> 00:04:57,660
any real or valid data going on it's

120
00:04:54,660 --> 00:04:59,520
just level transitioning so I scroll

121
00:04:57,660 --> 00:05:01,740
back out and then I scroll in on this

122
00:04:59,520 --> 00:05:04,020
next one and here we can see that it

123
00:05:01,740 --> 00:05:06,419
looked like a flat line there but it's

124
00:05:04,020 --> 00:05:08,639
actually some data that's just

125
00:05:06,419 --> 00:05:11,100
compressed down

126
00:05:08,639 --> 00:05:13,800
if I scroll in on that

127
00:05:11,100 --> 00:05:16,020
and what I see is

128
00:05:13,800 --> 00:05:18,419
system we've got the clock oscillating

129
00:05:16,020 --> 00:05:20,940
as expected we've got some data coming

130
00:05:18,419 --> 00:05:23,520
in on a peripheral in controller out so

131
00:05:20,940 --> 00:05:25,860
data coming in there first and then data

132
00:05:23,520 --> 00:05:27,900
going out from peripheral out controller

133
00:05:25,860 --> 00:05:29,699
in on this line later

134
00:05:27,900 --> 00:05:31,620
you can also see that you know there's a

135
00:05:29,699 --> 00:05:33,300
little bit of a potential glitch here

136
00:05:31,620 --> 00:05:35,220
and there's just some little quick

137
00:05:33,300 --> 00:05:37,440
transition that seems smaller than all

138
00:05:35,220 --> 00:05:39,360
the other transitions so that's okay

139
00:05:37,440 --> 00:05:41,100
that one in this case shouldn't actually

140
00:05:39,360 --> 00:05:43,680
affect our data but that can

141
00:05:41,100 --> 00:05:45,960
occasionally affect your data and if it

142
00:05:43,680 --> 00:05:47,220
is you may need to use this glitch

143
00:05:45,960 --> 00:05:49,560
filter option

144
00:05:47,220 --> 00:05:52,740
so glitches will occur you know due to

145
00:05:49,560 --> 00:05:54,600
for instance just a bad wiring or that

146
00:05:52,740 --> 00:05:57,419
kind of thing so okay at this point

147
00:05:54,600 --> 00:05:58,620
we've got some data it looks to be you

148
00:05:57,419 --> 00:06:01,080
know valid but we're not going to really

149
00:05:58,620 --> 00:06:02,340
know until we analyze it so I'm going to

150
00:06:01,080 --> 00:06:05,160
want to analyze this on a different

151
00:06:02,340 --> 00:06:07,979
system where I can use the quad SPI

152
00:06:05,160 --> 00:06:10,259
analyzer so I'm going to go ahead and

153
00:06:07,979 --> 00:06:12,840
save this capture so that I can analyze

154
00:06:10,259 --> 00:06:15,000
it later so go ahead and do that get

155
00:06:12,840 --> 00:06:18,979
your initial capture save it and then

156
00:06:15,000 --> 00:06:18,979
we'll come back and do the analysis next

