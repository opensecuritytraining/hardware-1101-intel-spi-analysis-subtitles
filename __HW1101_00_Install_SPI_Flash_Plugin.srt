1
00:00:01,920 --> 00:00:06,899
initially in logic you will not have the

2
00:00:04,860 --> 00:00:09,360
 SPI flash analyzer you will only have

3
00:00:06,899 --> 00:00:12,000
the SPI analyzer so under the analyzers

4
00:00:09,360 --> 00:00:15,179
tab if you hit plus and you type SPI

5
00:00:12,000 --> 00:00:16,740
you'd only see SPI not SPI flash

6
00:00:15,179 --> 00:00:19,560
and while that's helpful for the

7
00:00:16,740 --> 00:00:21,720
purposes of this area we need this by

8
00:00:19,560 --> 00:00:25,080
flash analyzer so click on the

9
00:00:21,720 --> 00:00:27,800
extensions Tab and then scroll down to

10
00:00:25,080 --> 00:00:27,800
SPI flash

11
00:00:28,019 --> 00:00:34,079
and click the install button

12
00:00:31,380 --> 00:00:36,600
now at this point under analyzers if you

13
00:00:34,079 --> 00:00:40,219
hit the plus u and type SPI you should

14
00:00:36,600 --> 00:00:40,219
see both SPI and SPI flash

