1
00:00:00,000 --> 00:00:06,060
here I have a setup consisting of a

2
00:00:02,580 --> 00:00:09,179
disconnected logic board a breadboard a

3
00:00:06,060 --> 00:00:11,040
Dediprog SPI flash reader writer and the

4
00:00:09,179 --> 00:00:13,860
Saleae logic analyzer

5
00:00:11,040 --> 00:00:17,160
so in this particular motherboard if I

6
00:00:13,860 --> 00:00:20,640
attempt to connect to this SPI flash

7
00:00:17,160 --> 00:00:22,859
chip it the Dediprog is not capable of

8
00:00:20,640 --> 00:00:25,980
reading the contents up as opposed to

9
00:00:22,859 --> 00:00:28,560
this by flash chip right here it is able

10
00:00:25,980 --> 00:00:31,439
to read the contents up so in this video

11
00:00:28,560 --> 00:00:33,840
I'm going to show you how to connect to

12
00:00:31,439 --> 00:00:35,399
these using the breadboard as basically

13
00:00:33,840 --> 00:00:37,440
a splitter which allows you to connect

14
00:00:35,399 --> 00:00:39,480
it to the Dediprog and the logic

15
00:00:37,440 --> 00:00:41,340
analyzer at the same time so we can see

16
00:00:39,480 --> 00:00:44,340
what the logic analog what the Dediprog

17
00:00:41,340 --> 00:00:45,960
is doing from the logic analyzer so

18
00:00:44,340 --> 00:00:47,820
the first thing we're going to do is

19
00:00:45,960 --> 00:00:50,160
we're going to take a look at this

20
00:00:47,820 --> 00:00:52,620
Dediprog and right on the surface we

21
00:00:50,160 --> 00:00:56,280
can see it indicates what lines are what

22
00:00:52,620 --> 00:00:59,940
so it has chip select one VCC MISO or

23
00:00:56,280 --> 00:01:04,440
data quad 1 hold write protect clock

24
00:00:59,940 --> 00:01:08,460
ground MOSI or data quad 0. So based on

25
00:01:04,440 --> 00:01:10,200
that we can take this connector and we

26
00:01:08,460 --> 00:01:13,560
can map you know which of those pins

27
00:01:10,200 --> 00:01:15,600
there correspond to which pins here and

28
00:01:13,560 --> 00:01:18,119
then we're ultimately going to take this

29
00:01:15,600 --> 00:01:21,299
and we're going to connect it to the

30
00:01:18,119 --> 00:01:22,979
logic board and we need to basically say

31
00:01:21,299 --> 00:01:25,439
that right there that little white

32
00:01:22,979 --> 00:01:29,100
marker on the chip clip is indicating

33
00:01:25,439 --> 00:01:31,680
where pin one should be and then down on

34
00:01:29,100 --> 00:01:34,560
these chips they have a marking that

35
00:01:31,680 --> 00:01:37,380
basically indicates where pin one is in

36
00:01:34,560 --> 00:01:40,860
this particular case it is up in this

37
00:01:37,380 --> 00:01:44,040
corner and up in this corner so we're

38
00:01:40,860 --> 00:01:47,340
going to basically connect the clip

39
00:01:44,040 --> 00:01:48,960
to the SPI flash that is working right

40
00:01:47,340 --> 00:01:51,540
now and then we're going to basically

41
00:01:48,960 --> 00:01:53,520
make an adapter and connect these wires

42
00:01:51,540 --> 00:01:57,259
to the breadboard and then those wires

43
00:01:53,520 --> 00:01:57,259
to the Dediprog in the logic analyzer

