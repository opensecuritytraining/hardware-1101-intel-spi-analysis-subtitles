1
00:00:00,060 --> 00:00:06,359
so at this point I had another idea so I

2
00:00:02,700 --> 00:00:08,940
know that the CPU or PCH is powering on

3
00:00:06,359 --> 00:00:11,040
at this point based on the 3.3 volts

4
00:00:08,940 --> 00:00:13,740
that is being provided manually from my

5
00:00:11,040 --> 00:00:16,920
benchtop power supply I also know that I

6
00:00:13,740 --> 00:00:18,960
don't need to select 3.3 specifically we

7
00:00:16,920 --> 00:00:24,300
said that the data sheet says this thing

8
00:00:18,960 --> 00:00:26,939
can handle between 2.7 and 3.6 volts

9
00:00:24,300 --> 00:00:29,039
so 3.3 was chosen because that's like a

10
00:00:26,939 --> 00:00:30,840
common voltage and so the question was

11
00:00:29,039 --> 00:00:33,719
what if I set it down to the lowest

12
00:00:30,840 --> 00:00:37,380
voltage of 2.7 this by flash chip says

13
00:00:33,719 --> 00:00:39,899
it'll work with 2.7 volts but if the CPU

14
00:00:37,380 --> 00:00:43,739
or PCH complex is expecting somewhere

15
00:00:39,899 --> 00:00:46,140
order of three volts 3.3 volts then we

16
00:00:43,739 --> 00:00:48,360
would be able to power the flash chip

17
00:00:46,140 --> 00:00:50,820
without powering the CPU

18
00:00:48,360 --> 00:00:53,840
so again as I said my Dediprog does

19
00:00:50,820 --> 00:00:56,520
have a nice granular version of the

20
00:00:53,840 --> 00:01:01,079
voltage select so I can manually adjust

21
00:00:56,520 --> 00:01:04,559
that and I could set it to 2.7 all right

22
00:01:01,079 --> 00:01:05,820
so I set that I hit OK you know so again

23
00:01:04,559 --> 00:01:07,619
you know if you're using a different one

24
00:01:05,820 --> 00:01:09,600
if you only have the 2.5 you wouldn't be

25
00:01:07,619 --> 00:01:12,540
able to do this kind of experiment but

26
00:01:09,600 --> 00:01:15,060
so I set it to 2.7 and then I said okay

27
00:01:12,540 --> 00:01:17,400
let's make sure that my bench top power

28
00:01:15,060 --> 00:01:19,920
supply is turned off and let's go ahead

29
00:01:17,400 --> 00:01:23,939
and capture what happens if I only Power

30
00:01:19,920 --> 00:01:26,939
it at 2.7 so do that do this for

31
00:01:23,939 --> 00:01:28,920
detection and what do I see well

32
00:01:26,939 --> 00:01:31,979
unfortunately it looks like I'm still

33
00:01:28,920 --> 00:01:33,900
seeing garbage so that is weird and

34
00:01:31,979 --> 00:01:36,000
worrying and disheartening and

35
00:01:33,900 --> 00:01:39,360
unfortunately that's just the way it is

36
00:01:36,000 --> 00:01:41,159
so moving it down to 2.7 still does not

37
00:01:39,360 --> 00:01:43,439
yield a notification

38
00:01:41,159 --> 00:01:46,619
and then finally at this point I had

39
00:01:43,439 --> 00:01:48,479
like one last idea and I said well maybe

40
00:01:46,619 --> 00:01:50,640
for some reason you know we've we've

41
00:01:48,479 --> 00:01:52,200
clearly got the voltage here does not

42
00:01:50,640 --> 00:01:54,479
look like there's a lot going on like

43
00:01:52,200 --> 00:01:56,340
something is up with this voltage you

44
00:01:54,479 --> 00:01:57,659
know I can unplug that Dediprog plug it

45
00:01:56,340 --> 00:01:59,820
back in you know maybe I'll see

46
00:01:57,659 --> 00:02:01,500
different results maybe the Dediprog

47
00:01:59,820 --> 00:02:03,240
is just sort of misconfigured so it's

48
00:02:01,500 --> 00:02:05,280
not getting adequate voltage and

49
00:02:03,240 --> 00:02:07,320
consequently we're just seeing all sorts

50
00:02:05,280 --> 00:02:09,119
of garbage so then in situations like

51
00:02:07,320 --> 00:02:11,039
this I don't necessarily trust my daddy

52
00:02:09,119 --> 00:02:14,160
prog so I'm going to go ahead and

53
00:02:11,039 --> 00:02:15,660
physically unplug it going to close down

54
00:02:14,160 --> 00:02:17,459
the application

55
00:02:15,660 --> 00:02:19,620
now the application closed anyways

56
00:02:17,459 --> 00:02:22,319
because it's all the thing disconnected

57
00:02:19,620 --> 00:02:24,420
going to physically connect it again

58
00:02:22,319 --> 00:02:26,760
go and you tell it yes this is going to

59
00:02:24,420 --> 00:02:28,319
use a sf600 plus it thought the hardware

60
00:02:26,760 --> 00:02:30,000
wasn't there but I just plugged it back

61
00:02:28,319 --> 00:02:31,440
in it's going to try some detection

62
00:02:30,000 --> 00:02:33,900
there's no power connector right now

63
00:02:31,440 --> 00:02:35,459
it's going to fail just like usual then

64
00:02:33,900 --> 00:02:38,099
I'm going to configure it make sure it's

65
00:02:35,459 --> 00:02:40,140
at 2.7 volts then I'm going to try that

66
00:02:38,099 --> 00:02:42,900
again quick just so I can see yeah so

67
00:02:40,140 --> 00:02:47,900
you can see it defaulted back to 3.5 so

68
00:02:42,900 --> 00:02:47,900
I'm going to set that to 2.7 volts

69
00:02:48,540 --> 00:02:55,440
hit OK going to start the logic analyzer

70
00:02:53,340 --> 00:02:56,580
I'm going to go back to Dediprog hit

71
00:02:55,440 --> 00:02:59,400
detect

72
00:02:56,580 --> 00:03:00,780
and again still no voltage here so you

73
00:02:59,400 --> 00:03:02,640
know this is that's the problem

74
00:03:00,780 --> 00:03:05,280
obviously you know you can't expect to

75
00:03:02,640 --> 00:03:07,739
see proper valid data if you don't have

76
00:03:05,280 --> 00:03:09,420
voltage going to your chip so the final

77
00:03:07,739 --> 00:03:11,819
thing that I basically decided to do

78
00:03:09,420 --> 00:03:15,239
here was I said well what if I set my

79
00:03:11,819 --> 00:03:18,360
benchtop power supply to 2.7 volts so

80
00:03:15,239 --> 00:03:23,480
that's what I did I just take this and I

81
00:03:18,360 --> 00:03:23,480
move it down to 2.7 volts

82
00:03:26,220 --> 00:03:29,940
and we've got 2.7 is going to be

83
00:03:28,319 --> 00:03:31,560
provided

84
00:03:29,940 --> 00:03:34,560
and I'm going to go ahead and see what

85
00:03:31,560 --> 00:03:36,659
happens if I provide that 2.7 volts to

86
00:03:34,560 --> 00:03:38,099
the system is it going to power the

87
00:03:36,659 --> 00:03:40,620
system on and have a bunch of

88
00:03:38,099 --> 00:03:43,200
backgrounds by flash traffic no it is

89
00:03:40,620 --> 00:03:45,959
not so that now confirms that okay I can

90
00:03:43,200 --> 00:03:48,900
provide 2.7 volts it won't start the CPU

91
00:03:45,959 --> 00:03:51,120
and maybe this voltage will make the

92
00:03:48,900 --> 00:03:52,560
Dediprog happy and it'll do the proper

93
00:03:51,120 --> 00:03:55,319
powering of the chip which it should

94
00:03:52,560 --> 00:03:58,140
have been doing anyways but it wasn't so

95
00:03:55,319 --> 00:04:01,080
now we just provide the voltage so let's

96
00:03:58,140 --> 00:04:03,480
start the analyzer let's start the deady

97
00:04:01,080 --> 00:04:05,159
prog detection it's again still set to

98
00:04:03,480 --> 00:04:08,099
2.7 volts

99
00:04:05,159 --> 00:04:10,920
we hit detect

100
00:04:08,099 --> 00:04:15,959
and we finally get a proper read on the

101
00:04:10,920 --> 00:04:18,180
thing so this is a w 25q 128fv and so it

102
00:04:15,959 --> 00:04:20,459
correctly identified it

103
00:04:18,180 --> 00:04:24,000
all right and so we could see that if we

104
00:04:20,459 --> 00:04:26,240
go here now to the 9f that we see coming

105
00:04:24,000 --> 00:04:26,240
in

106
00:04:26,520 --> 00:04:30,419
oops

107
00:04:28,199 --> 00:04:32,400
so we've got 9f coming in here

108
00:04:30,419 --> 00:04:34,560
then we've got the EF coming out it

109
00:04:32,400 --> 00:04:37,320
brings the chip select High

110
00:04:34,560 --> 00:04:40,400
and then it tries again the 9f

111
00:04:37,320 --> 00:04:42,840
and then we have

112
00:04:40,400 --> 00:04:44,759
ef-60e well you can see that clearly

113
00:04:42,840 --> 00:04:46,259
I've got you know some glitches going on

114
00:04:44,759 --> 00:04:48,419
in there that was why I was using the

115
00:04:46,259 --> 00:04:50,520
glitch filter before so if I were to

116
00:04:48,419 --> 00:04:53,040
just manually read this by eyeball we

117
00:04:50,520 --> 00:04:55,500
can see that this is zero this is one

118
00:04:53,040 --> 00:04:57,540
this is zero zero so that's the four and

119
00:04:55,500 --> 00:05:01,500
that's the zero and again we've got a

120
00:04:57,540 --> 00:05:04,580
glitch here but this is zero zero zero

121
00:05:01,500 --> 00:05:08,340
one and one

122
00:05:04,580 --> 00:05:09,900
zero zero zero so the data is not quite

123
00:05:08,340 --> 00:05:11,880
right there you know it was enough for

124
00:05:09,900 --> 00:05:14,759
the thing to actually identify it and

125
00:05:11,880 --> 00:05:16,440
you know properly recognize the chip but

126
00:05:14,759 --> 00:05:18,000
we should probably be using a glitch

127
00:05:16,440 --> 00:05:20,880
filter at this point so that it would

128
00:05:18,000 --> 00:05:22,560
you know identify more correctly but I

129
00:05:20,880 --> 00:05:24,479
guess the the point here is probably the

130
00:05:22,560 --> 00:05:27,060
logic analyzer was seeing you know the

131
00:05:24,479 --> 00:05:29,160
slightly incorrect version whereas the

132
00:05:27,060 --> 00:05:30,720
Dediprog was actually able to see the

133
00:05:29,160 --> 00:05:33,240
correct version on the line without the

134
00:05:30,720 --> 00:05:36,080
glitches so what we expect for this

135
00:05:33,240 --> 00:05:40,620
particular chip when it has a 9f input

136
00:05:36,080 --> 00:05:44,400
is we expect to see 4018 if it's running

137
00:05:40,620 --> 00:05:46,800
in single SPI mode and 6018 if it's

138
00:05:44,400 --> 00:05:48,600
running in Quad SPI mode so again if we

139
00:05:46,800 --> 00:05:51,360
go look at the data you know it kind of

140
00:05:48,600 --> 00:05:52,620
looks like uh sixth there but because of

141
00:05:51,360 --> 00:05:54,360
the glitches you know this would

142
00:05:52,620 --> 00:05:56,000
actually be zero

143
00:05:54,360 --> 00:05:59,759
one

144
00:05:56,000 --> 00:06:03,780
zero zero and so that's 40 and then this

145
00:05:59,759 --> 00:06:08,039
should be 18 so zero zero zero and then

146
00:06:03,780 --> 00:06:11,580
one and then one zero zero zero and so

147
00:06:08,039 --> 00:06:13,800
that's 18. all right so the thing was

148
00:06:11,580 --> 00:06:15,720
able to successfully identify it and you

149
00:06:13,800 --> 00:06:18,600
know again just to confirm for our own

150
00:06:15,720 --> 00:06:20,940
sake that we actually can fully read

151
00:06:18,600 --> 00:06:23,940
everything when the external power is

152
00:06:20,940 --> 00:06:26,160
applied then indeed we see the read

153
00:06:23,940 --> 00:06:27,960
progress succeeding and so we've got

154
00:06:26,160 --> 00:06:29,639
this eight megabyte chip and it is

155
00:06:27,960 --> 00:06:32,100
successfully reading it at the beginning

156
00:06:29,639 --> 00:06:35,039
we should see something like the Intel

157
00:06:32,100 --> 00:06:39,600
magic SPI flash descriptor signature of

158
00:06:35,039 --> 00:06:41,100
5A A5 f00f and the 16 bytes of F's at

159
00:06:39,600 --> 00:06:43,080
the very beginning so there we go I've

160
00:06:41,100 --> 00:06:45,360
showed you a little bit about you know

161
00:06:43,080 --> 00:06:47,699
how you can use the logic analyzer to

162
00:06:45,360 --> 00:06:50,819
see what's going wrong with attempts to

163
00:06:47,699 --> 00:06:52,860
read a SPI flash chip what valid

164
00:06:50,819 --> 00:06:55,919
attempts to read look like you know what

165
00:06:52,860 --> 00:06:58,740
you should see in terms of the jdeck

166
00:06:55,919 --> 00:07:01,020
identification command the 9f command

167
00:06:58,740 --> 00:07:02,759
and then you know how your data sheet

168
00:07:01,020 --> 00:07:04,979
will tell you what the expected values

169
00:07:02,759 --> 00:07:07,080
that you should see back are you know EF

170
00:07:04,979 --> 00:07:09,120
is specifically for Winbond again this

171
00:07:07,080 --> 00:07:10,440
is a Winbond data sheet if you're using

172
00:07:09,120 --> 00:07:12,180
a system with a completely different

173
00:07:10,440 --> 00:07:14,100
chip manufacturer you'll have a

174
00:07:12,180 --> 00:07:17,600
different manufacturer ID and different

175
00:07:14,100 --> 00:07:17,600
memory types and capacities

