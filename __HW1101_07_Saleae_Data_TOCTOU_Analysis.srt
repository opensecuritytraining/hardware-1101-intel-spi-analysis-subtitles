1
00:00:00,000 --> 00:00:04,980
now I want to talk about how these sort

2
00:00:02,399 --> 00:00:06,899
of visualizations can be used to try to

3
00:00:04,980 --> 00:00:09,240
find double fetch vulnerabilities a

4
00:00:06,899 --> 00:00:11,040
double fetch vulnerability or a TOCTOU

5
00:00:09,240 --> 00:00:13,500
vulnerability time of check time of use

6
00:00:11,040 --> 00:00:16,139
is when potentially the system is

7
00:00:13,500 --> 00:00:19,020
verifying some information at the time

8
00:00:16,139 --> 00:00:20,640
of check and then later on it uses it at

9
00:00:19,020 --> 00:00:22,740
the time of use but that information

10
00:00:20,640 --> 00:00:24,539
could have changed between those two

11
00:00:22,740 --> 00:00:27,000
points so there's a couple different

12
00:00:24,539 --> 00:00:29,039
contexts where you might have talked to

13
00:00:27,000 --> 00:00:32,099
attacks in the context of

14
00:00:29,039 --> 00:00:33,899
 UEFI firmware and Intel firmware the

15
00:00:32,099 --> 00:00:36,719
first one is that you could have a

16
00:00:33,899 --> 00:00:39,059
situation where the system is Reading in

17
00:00:36,719 --> 00:00:41,700
firmware and it's verifying it for

18
00:00:39,059 --> 00:00:44,700
purposes of like measured boot or secure

19
00:00:41,700 --> 00:00:47,640
Boot and then if it then later on in the

20
00:00:44,700 --> 00:00:49,559
graph reads it in again then there's a

21
00:00:47,640 --> 00:00:51,840
time period during which an attacker

22
00:00:49,559 --> 00:00:53,640
could have changed the data and it was

23
00:00:51,840 --> 00:00:55,199
verified and all good here and clean at

24
00:00:53,640 --> 00:00:57,180
the beginning but then it's dirty later

25
00:00:55,199 --> 00:00:58,980
on and attacker controlled and so if

26
00:00:57,180 --> 00:01:00,719
that data was code then that would mean

27
00:00:58,980 --> 00:01:03,059
the attacker would be able to to just

28
00:01:00,719 --> 00:01:04,860
straight inject code into the system and

29
00:01:03,059 --> 00:01:07,920
cause arbitrary code execution at boot

30
00:01:04,860 --> 00:01:10,619
time alternatively if it was data so

31
00:01:07,920 --> 00:01:12,360
just general UEFI data ranges is going

32
00:01:10,619 --> 00:01:14,580
to be up around here in these graphs

33
00:01:12,360 --> 00:01:18,060
which you could figure out by basically

34
00:01:14,580 --> 00:01:20,640
picking a given point and looking at

35
00:01:18,060 --> 00:01:22,979
that in a tool like UEFI tool you know

36
00:01:20,640 --> 00:01:25,560
you just select a point figure out what

37
00:01:22,979 --> 00:01:27,600
the actual literal value is for that so

38
00:01:25,560 --> 00:01:30,479
I'm having a little bit of difficulty

39
00:01:27,600 --> 00:01:32,100
with Precision because my mouse is blown

40
00:01:30,479 --> 00:01:34,200
up and so the pointer is not exactly

41
00:01:32,100 --> 00:01:37,079
where I think it is but so for instance

42
00:01:34,200 --> 00:01:40,680
you could go back into your CSV file go

43
00:01:37,079 --> 00:01:43,020
to 37137 see the actual literal value

44
00:01:40,680 --> 00:01:45,540
there converted to hex go into the hex

45
00:01:43,020 --> 00:01:48,540
dump use something like UEFITool to

46
00:01:45,540 --> 00:01:50,399
parse the the hex dump as a proper UEFI

47
00:01:48,540 --> 00:01:52,259
firmware file system and then you would

48
00:01:50,399 --> 00:01:54,840
see that you know in this range is

49
00:01:52,259 --> 00:01:56,700
generally where UEFI variables are so you

50
00:01:54,840 --> 00:01:59,340
could tell what any given variable is at

51
00:01:56,700 --> 00:02:01,680
any given address but the point is if

52
00:01:59,340 --> 00:02:03,720
you had a very variable something like

53
00:02:01,680 --> 00:02:05,640
the secure boot variable that says it's

54
00:02:03,720 --> 00:02:07,740
on one point but then it's like maybe

55
00:02:05,640 --> 00:02:09,840
checked again later on in the system to

56
00:02:07,740 --> 00:02:11,520
decide what it's going to do if it

57
00:02:09,840 --> 00:02:13,440
changes between the time of check and

58
00:02:11,520 --> 00:02:15,840
the time of use like if it's re-read if

59
00:02:13,440 --> 00:02:18,599
it's double fetched then an attacker

60
00:02:15,840 --> 00:02:20,940
could potentially control that as well

61
00:02:18,599 --> 00:02:23,640
so what we're looking for on this type

62
00:02:20,940 --> 00:02:26,220
of graph is anywhere that data is used

63
00:02:23,640 --> 00:02:28,560
and accessed and read and then it's

64
00:02:26,220 --> 00:02:31,379
re-read somewhere else along the line so

65
00:02:28,560 --> 00:02:33,900
we're looking for duplications along the

66
00:02:31,379 --> 00:02:35,760
horizontal axis so in this case you can

67
00:02:33,900 --> 00:02:37,560
see places where you might find

68
00:02:35,760 --> 00:02:39,120
duplication well this is all you know

69
00:02:37,560 --> 00:02:41,940
there's nothing going on in parallel

70
00:02:39,120 --> 00:02:44,700
here there's clearly a whole bunch of

71
00:02:41,940 --> 00:02:46,560
re-reading going on so basically the

72
00:02:44,700 --> 00:02:49,739
question we would have to investigate is

73
00:02:46,560 --> 00:02:51,599
is this variable data is this code data

74
00:02:49,739 --> 00:02:54,000
and even if it's code data just because

75
00:02:51,599 --> 00:02:56,040
it's re-read it doesn't necessarily mean

76
00:02:54,000 --> 00:02:58,080
that it's going to be re-executed right

77
00:02:56,040 --> 00:03:00,300
so it would take further reverse

78
00:02:58,080 --> 00:03:02,640
engineering to determine if a re-read of

79
00:03:00,300 --> 00:03:05,099
code like if we say this is some code

80
00:03:02,640 --> 00:03:07,680
module in the firmware if a reread if

81
00:03:05,099 --> 00:03:10,200
it's red here and then it's executed

82
00:03:07,680 --> 00:03:12,180
again you know after it's read the

83
00:03:10,200 --> 00:03:14,459
second time like so if it's executed and

84
00:03:12,180 --> 00:03:16,200
then re-read and then executed again so

85
00:03:14,459 --> 00:03:17,099
that again requires separate reverse

86
00:03:16,200 --> 00:03:19,739
engineering

87
00:03:17,099 --> 00:03:21,360
so this is just you know an example of

88
00:03:19,739 --> 00:03:23,280
how you do that you just kind of you

89
00:03:21,360 --> 00:03:26,340
know search around you look for places

90
00:03:23,280 --> 00:03:29,159
that you can potentially see horizontal

91
00:03:26,340 --> 00:03:30,900
duplication of data and so you know

92
00:03:29,159 --> 00:03:33,060
sometimes it may not be clear it's like

93
00:03:30,900 --> 00:03:34,920
okay is that exactly overlapping is it

94
00:03:33,060 --> 00:03:36,959
not and you just have to kind of you

95
00:03:34,920 --> 00:03:39,300
know eyeball it or select a point and

96
00:03:36,959 --> 00:03:42,120
then go back into your CSV file and

97
00:03:39,300 --> 00:03:44,519
select the point again so this is from

98
00:03:42,120 --> 00:03:46,440
you know my example system I'm going to

99
00:03:44,519 --> 00:03:48,360
pull up by a different system that you

100
00:03:46,440 --> 00:03:50,040
know I was looking at for attack

101
00:03:48,360 --> 00:03:52,019
and I'm going to look at that data

102
00:03:50,040 --> 00:03:54,720
instead so we looked something like this

103
00:03:52,019 --> 00:03:56,580
and so you know I would maybe zoom in on

104
00:03:54,720 --> 00:03:58,920
this and try to find anywhere that I see

105
00:03:56,580 --> 00:04:01,440
horizontal and this right here looks

106
00:03:58,920 --> 00:04:04,440
like a big sort of horizontal reread so

107
00:04:01,440 --> 00:04:07,680
let's zoom in closer on that keep the

108
00:04:04,440 --> 00:04:10,680
vertical axis so that it's more clear

109
00:04:07,680 --> 00:04:12,900
and then yes indeed this does look like

110
00:04:10,680 --> 00:04:14,640
it is clearly being read here and then

111
00:04:12,900 --> 00:04:16,979
re-read it's fetched and then double

112
00:04:14,640 --> 00:04:18,900
fetched and so if there's any sort of

113
00:04:16,979 --> 00:04:21,239
you know time of check if there's sort

114
00:04:18,900 --> 00:04:23,820
of Integrity verification going on at

115
00:04:21,239 --> 00:04:26,040
this point and if I can swap out the

116
00:04:23,820 --> 00:04:28,500
data between this point and this reread

117
00:04:26,040 --> 00:04:30,720
point then perhaps I can you know

118
00:04:28,500 --> 00:04:32,639
manipulate the the values to some

119
00:04:30,720 --> 00:04:35,040
beneficial way whether it's changing out

120
00:04:32,639 --> 00:04:36,840
code or changing out data before it's

121
00:04:35,040 --> 00:04:38,639
used again but again that's the kind of

122
00:04:36,840 --> 00:04:40,380
thing that requires reverse engineering

123
00:04:38,639 --> 00:04:43,620
to actually figure out you know what is

124
00:04:40,380 --> 00:04:45,960
this data and how is it used

125
00:04:43,620 --> 00:04:48,000
so that's part of the point of this sort

126
00:04:45,960 --> 00:04:50,460
of visualization is just to give you a

127
00:04:48,000 --> 00:04:53,040
sense of how you can use it in support

128
00:04:50,460 --> 00:04:54,840
of reverse engineering but again verse

129
00:04:53,040 --> 00:04:56,520
engineering is not the purpose of this

130
00:04:54,840 --> 00:04:59,780
particular walkthrough that will be

131
00:04:56,520 --> 00:04:59,780
covered in other areas

