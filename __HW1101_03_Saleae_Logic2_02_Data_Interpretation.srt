1
00:00:00,359 --> 00:00:05,640
okay now I've got the data imported into

2
00:00:03,000 --> 00:00:07,560
Logic on a system where I can use the

3
00:00:05,640 --> 00:00:09,300
custom analyzer which again you set in

4
00:00:07,560 --> 00:00:12,420
preferences and then custom low level

5
00:00:09,300 --> 00:00:13,920
analyzers so I can use the quad SPI

6
00:00:12,420 --> 00:00:15,599
analyzer from here

7
00:00:13,920 --> 00:00:18,840
so the first thing I usually like to do

8
00:00:15,599 --> 00:00:21,359
is order things with the quad lines

9
00:00:18,840 --> 00:00:23,220
ordered in ascending order just makes it

10
00:00:21,359 --> 00:00:28,980
a little easier for me to see so I'm

11
00:00:23,220 --> 00:00:30,539
going to put IO3 at the top IO2 IO1 and

12
00:00:28,980 --> 00:00:32,220
then I usually like to have the enable

13
00:00:30,539 --> 00:00:34,320
line at the bottom and the clock second

14
00:00:32,220 --> 00:00:36,420
from the bottom so we've got zero one

15
00:00:34,320 --> 00:00:38,940
two three and you can see that actually

16
00:00:36,420 --> 00:00:41,879
there's no data that's occurring on two

17
00:00:38,940 --> 00:00:43,800
and three and so that means that this is

18
00:00:41,879 --> 00:00:45,540
probably going to be using dual SPI not

19
00:00:43,800 --> 00:00:46,980
quad SPI because if it was quad you

20
00:00:45,540 --> 00:00:47,760
would expect to see data on all the

21
00:00:46,980 --> 00:00:50,039
lines

22
00:00:47,760 --> 00:00:51,360
so we've got enable on the bottom clock

23
00:00:50,039 --> 00:00:55,500
and then

24
00:00:51,360 --> 00:00:57,719
in PICO and POCI so IO0 and IO1

25
00:00:55,500 --> 00:01:00,719
so let's go ahead and zoom in on the

26
00:00:57,719 --> 00:01:04,940
first data and start to analyze it

27
00:01:00,719 --> 00:01:04,940
so again that was this line right here

28
00:01:05,640 --> 00:01:10,680
and so we zoom in and we see our clock

29
00:01:08,100 --> 00:01:13,439
oscillating we see the enable line going

30
00:01:10,680 --> 00:01:15,119
low so chip select is active low so when

31
00:01:13,439 --> 00:01:17,280
it goes low that means this chip is

32
00:01:15,119 --> 00:01:18,659
actually selected and now going to

33
00:01:17,280 --> 00:01:21,180
actually be used

34
00:01:18,659 --> 00:01:23,520
so the first type of analyzer we can use

35
00:01:21,180 --> 00:01:26,100
is the basic SPI analyzer and so that's

36
00:01:23,520 --> 00:01:28,500
just built in so you can go ahead and go

37
00:01:26,100 --> 00:01:31,380
and analyzers hit plus and type SPI

38
00:01:28,500 --> 00:01:33,360
you'll get the basics by analyzer and so

39
00:01:31,380 --> 00:01:35,340
here you can see it's asking for what is

40
00:01:33,360 --> 00:01:37,799
 MOSI what is me so what is clock what is

41
00:01:35,340 --> 00:01:39,720
enable and so that's why I included the

42
00:01:37,799 --> 00:01:42,240
the MISO and MOSI names here just to

43
00:01:39,720 --> 00:01:44,159
make it easy for me to map up okay this

44
00:01:42,240 --> 00:01:46,439
line is mostly this line is MISO this

45
00:01:44,159 --> 00:01:48,900
line is clocked and this is enable or

46
00:01:46,439 --> 00:01:50,520
chip select and so this won't be set by

47
00:01:48,900 --> 00:01:52,500
default for you that's just because I've

48
00:01:50,520 --> 00:01:54,420
already analyzed in this thing so you

49
00:01:52,500 --> 00:01:56,880
need to select all of these

50
00:01:54,420 --> 00:01:58,500
then in terms of these other things of

51
00:01:56,880 --> 00:02:00,360
what's the most significant bit how many

52
00:01:58,500 --> 00:02:02,340
bits per transfer et cetera each of

53
00:02:00,360 --> 00:02:04,500
those is going to be defined by the

54
00:02:02,340 --> 00:02:07,380
particulars of your particular SPI flash

55
00:02:04,500 --> 00:02:09,119
chip but typically the defaults should

56
00:02:07,380 --> 00:02:10,979
be good for most chips that you

57
00:02:09,119 --> 00:02:12,660
encounter if for some reason you're

58
00:02:10,979 --> 00:02:16,020
seeing completely invalid data then you

59
00:02:12,660 --> 00:02:17,580
may need to go consult your data sheet

60
00:02:16,020 --> 00:02:20,520
to see whether it uses a different

61
00:02:17,580 --> 00:02:22,440
setting for these so if we hit that then

62
00:02:20,520 --> 00:02:24,300
it's going to start analysis and

63
00:02:22,440 --> 00:02:26,220
Analysis can take a bit of time for the

64
00:02:24,300 --> 00:02:27,900
basics by flash it's very quick so it's

65
00:02:26,220 --> 00:02:29,760
not a big deal but for more advanced

66
00:02:27,900 --> 00:02:31,379
things it can take longer so we can see

67
00:02:29,760 --> 00:02:34,920
that for this first byte here it's going

68
00:02:31,379 --> 00:02:36,660
to be interpreted as 5A and then the

69
00:02:34,920 --> 00:02:41,459
data actually coming back will be

70
00:02:36,660 --> 00:02:44,040
interpreted as 5 3 well ff5346

71
00:02:41,459 --> 00:02:45,900
etc etc so to understand you know what

72
00:02:44,040 --> 00:02:47,400
does 5A even mean that's one of those

73
00:02:45,900 --> 00:02:50,099
things where you have to consult the

74
00:02:47,400 --> 00:02:52,800
datasheet and so if we look in the

75
00:02:50,099 --> 00:02:56,280
datasheet for byte5a we'll see that's

76
00:02:52,800 --> 00:02:58,920
the read sfdp register so serial flash

77
00:02:56,280 --> 00:03:00,540
Discovery parameter register contains

78
00:02:58,920 --> 00:03:03,360
information about device configurations

79
00:03:00,540 --> 00:03:06,239
so basically at the very first thing

80
00:03:03,360 --> 00:03:07,980
this this Intel chip does when the

81
00:03:06,239 --> 00:03:10,140
system is booting up is it queries the

82
00:03:07,980 --> 00:03:12,540
 SPI flash ship to find out about its

83
00:03:10,140 --> 00:03:14,879
configuration so that it knows you know

84
00:03:12,540 --> 00:03:16,560
what kind of options it supports how

85
00:03:14,879 --> 00:03:18,300
it's going to behave Etc

86
00:03:16,560 --> 00:03:22,019
and then the one thing I would say here

87
00:03:18,300 --> 00:03:23,760
is that it mentions that uh there are

88
00:03:22,019 --> 00:03:25,500
going to be eight dummy clocks that are

89
00:03:23,760 --> 00:03:27,659
required before the actual data comes

90
00:03:25,500 --> 00:03:30,959
out so therefore you know that initial

91
00:03:27,659 --> 00:03:32,819
you know FF is probably the dummy data

92
00:03:30,959 --> 00:03:35,340
followed by the real data but you have

93
00:03:32,819 --> 00:03:37,319
to double check you know the the 24 bits

94
00:03:35,340 --> 00:03:39,599
of address coming in followed by the

95
00:03:37,319 --> 00:03:42,540
dummy data to confirm for sure

96
00:03:39,599 --> 00:03:44,819
so indeed we see 5A and then we see a

97
00:03:42,540 --> 00:03:48,000
24-bit address so three bytes of address

98
00:03:44,819 --> 00:03:50,040
so it's reading address zero of the

99
00:03:48,000 --> 00:03:54,000
configuration register then we have

100
00:03:50,040 --> 00:03:56,720
eight bits of dummy data followed by the

101
00:03:54,000 --> 00:03:56,720
real data

102
00:04:00,480 --> 00:04:04,080
okay so that's cool and that's

103
00:04:02,459 --> 00:04:06,060
interesting we could then go interpret

104
00:04:04,080 --> 00:04:08,519
this and look into it more but we're

105
00:04:06,060 --> 00:04:10,620
going to keep moving for now just to see

106
00:04:08,519 --> 00:04:14,159
what other kind of stuff we see here so

107
00:04:10,620 --> 00:04:16,560
5A 5A 5A there's some obviously querying

108
00:04:14,159 --> 00:04:18,959
going on you can see that the chip

109
00:04:16,560 --> 00:04:21,060
select line is going high between these

110
00:04:18,959 --> 00:04:23,100
things basically you know turning off

111
00:04:21,060 --> 00:04:24,660
the chip And deactivating it before it

112
00:04:23,100 --> 00:04:27,060
reactivates it again

113
00:04:24,660 --> 00:04:28,919
okay so we're going to use this little

114
00:04:27,060 --> 00:04:31,740
thing right here which allows us to

115
00:04:28,919 --> 00:04:33,240
click over to find the next edge for a

116
00:04:31,740 --> 00:04:35,580
particular transition for a particular

117
00:04:33,240 --> 00:04:38,160
line so we got all these five A's we're

118
00:04:35,580 --> 00:04:39,900
going to click over and then right here

119
00:04:38,160 --> 00:04:43,560
after these

120
00:04:39,900 --> 00:04:46,680
we're now going to see a03 and so what

121
00:04:43,560 --> 00:04:50,100
is 0 3 again you always consult and read

122
00:04:46,680 --> 00:04:52,080
the phone manual so put in 0 3 H and

123
00:04:50,100 --> 00:04:55,020
you'll see that is a read data command

124
00:04:52,080 --> 00:04:56,880
and it has the form of 0 3 a 24-bit

125
00:04:55,020 --> 00:05:01,620
address and then the data will be coming

126
00:04:56,880 --> 00:05:04,560
out on data out which is the IO1 so data

127
00:05:01,620 --> 00:05:06,300
in on data in data out on data out and

128
00:05:04,560 --> 00:05:08,880
you know there don't seem to be any

129
00:05:06,300 --> 00:05:12,300
dummy clock cycles between these

130
00:05:08,880 --> 00:05:15,780
so we can go here and we see 0 3 and the

131
00:05:12,300 --> 00:05:18,000
address is zero zero zero zero one zero

132
00:05:15,780 --> 00:05:23,460
and then the data that's coming back is

133
00:05:18,000 --> 00:05:25,560
5A 85 F 0 0 f and you know if you took

134
00:05:23,460 --> 00:05:29,160
the architecture 4001 then you will

135
00:05:25,560 --> 00:05:31,440
recognize this as this is the magic data

136
00:05:29,160 --> 00:05:33,539
that must exist at offset 10 to

137
00:05:31,440 --> 00:05:36,240
basically say that this SPI flash chip

138
00:05:33,539 --> 00:05:38,460
is operating in flash descriptor mode so

139
00:05:36,240 --> 00:05:41,940
Intel chips have a special format that

140
00:05:38,460 --> 00:05:44,940
they expect here so Intel systems the

141
00:05:41,940 --> 00:05:47,160
the PCH expects that at offset 10 it

142
00:05:44,940 --> 00:05:49,080
should find this exact signature of data

143
00:05:47,160 --> 00:05:51,060
and if it finds that then it's going to

144
00:05:49,080 --> 00:05:52,979
interpret the subsequent data as being

145
00:05:51,060 --> 00:05:55,199
in flash descriptor mode if it doesn't

146
00:05:52,979 --> 00:05:57,000
find that such as on earlier systems

147
00:05:55,199 --> 00:05:59,400
it'll be operating in non-descriptor

148
00:05:57,000 --> 00:06:02,039
mode so this all drives that looks like

149
00:05:59,400 --> 00:06:03,240
same data to us that's exactly what we

150
00:06:02,039 --> 00:06:05,580
would expect

151
00:06:03,240 --> 00:06:07,800
now at this point I want to say that we

152
00:06:05,580 --> 00:06:09,300
can add another analyzer that will sort

153
00:06:07,800 --> 00:06:11,460
of start breaking apart instead of us

154
00:06:09,300 --> 00:06:14,220
having to look at each byte of data

155
00:06:11,460 --> 00:06:16,800
manually we can further add in the SPI

156
00:06:14,220 --> 00:06:19,380
flash analyzer that is the one that is

157
00:06:16,800 --> 00:06:21,479
built into the extensions right so under

158
00:06:19,380 --> 00:06:24,240
extensions and SPI flash you should have

159
00:06:21,479 --> 00:06:27,600
installed that and so then in analyzers

160
00:06:24,240 --> 00:06:29,639
you can do plus and SPI flash

161
00:06:27,600 --> 00:06:30,960
select that it's going to ask you about

162
00:06:29,639 --> 00:06:32,639
some more parameters it's going to say

163
00:06:30,960 --> 00:06:34,680
what's your input analyzer here we're

164
00:06:32,639 --> 00:06:36,780
going to select the existing SPI

165
00:06:34,680 --> 00:06:38,520
analyzer that we already had it's going

166
00:06:36,780 --> 00:06:40,319
to ask how many address bytes and we're

167
00:06:38,520 --> 00:06:42,539
going to say three so the system is

168
00:06:40,319 --> 00:06:45,120
using three byte addressing and then

169
00:06:42,539 --> 00:06:46,800
this other stuff about decoding or

170
00:06:45,120 --> 00:06:48,960
minimum and maximum address we can just

171
00:06:46,800 --> 00:06:51,240
leave that as default

172
00:06:48,960 --> 00:06:52,740
so when we do that we see it start

173
00:06:51,240 --> 00:06:54,840
chewing through the data over here it's

174
00:06:52,740 --> 00:06:56,639
going to take some time to process but

175
00:06:54,840 --> 00:06:59,699
we can see that it is now added an

176
00:06:56,639 --> 00:07:02,220
interpretation here of read from address

177
00:06:59,699 --> 00:07:05,400
10 and this next one is going to be read

178
00:07:02,220 --> 00:07:07,500
from address 14. and so this gives us a

179
00:07:05,400 --> 00:07:09,240
little more semantically meaningful view

180
00:07:07,500 --> 00:07:11,639
into what's going on you know read

181
00:07:09,240 --> 00:07:15,120
address 30. and then next we're going to

182
00:07:11,639 --> 00:07:16,800
have fast read 40. so if we zoom in on

183
00:07:15,120 --> 00:07:20,099
that we can see that the actual byte

184
00:07:16,800 --> 00:07:22,440
there is 0b instead of 0 3 and if we

185
00:07:20,099 --> 00:07:24,900
search for 0b here we're going to see

186
00:07:22,440 --> 00:07:26,699
that that's called Fast read and we can

187
00:07:24,900 --> 00:07:28,919
see that that has some you know dummy

188
00:07:26,699 --> 00:07:31,319
clock cycles here but it's you know just

189
00:07:28,919 --> 00:07:33,780
going to be eight and so it's going to

190
00:07:31,319 --> 00:07:36,960
interpret that correctly so

191
00:07:33,780 --> 00:07:39,539
can see fast read is eight zero zero

192
00:07:36,960 --> 00:07:41,220
zero zero that's uh four zero that's the

193
00:07:39,539 --> 00:07:43,500
address then we have you know some dummy

194
00:07:41,220 --> 00:07:45,120
clock cycles and then we have the actual

195
00:07:43,500 --> 00:07:47,400
data coming back so it looks like it's

196
00:07:45,120 --> 00:07:49,680
all zeros and in situations where you

197
00:07:47,400 --> 00:07:51,660
perhaps already have a dump of the SPI

198
00:07:49,680 --> 00:07:54,180
flash you can use that to sanity check

199
00:07:51,660 --> 00:07:56,280
your interpretation by you know going to

200
00:07:54,180 --> 00:08:00,360
the SPI flash dump you can see that

201
00:07:56,280 --> 00:08:03,060
offset 10 was 5A A5 F zero zero f

202
00:08:00,360 --> 00:08:05,759
exactly what we'd expect and offset 40

203
00:08:03,060 --> 00:08:09,780
starts with four zeros followed by zero

204
00:08:05,759 --> 00:08:14,940
one zero zero f e zero e and so zero

205
00:08:09,780 --> 00:08:16,680
zeros for them 0 1 0 0 f e zero e so all

206
00:08:14,940 --> 00:08:19,319
of this data that we're seeing in our

207
00:08:16,680 --> 00:08:21,419
logic analyzer it looks to be sane and

208
00:08:19,319 --> 00:08:23,580
looks to match the dump that we already

209
00:08:21,419 --> 00:08:24,960
have of course in some situations you

210
00:08:23,580 --> 00:08:26,520
may not already have a dump and you're

211
00:08:24,960 --> 00:08:28,560
going to use the logic analyzer to

212
00:08:26,520 --> 00:08:30,300
construct the dump and so in those sort

213
00:08:28,560 --> 00:08:31,979
of situations you know you have less

214
00:08:30,300 --> 00:08:34,860
flexibility to figure out whether or not

215
00:08:31,979 --> 00:08:36,839
the data is same okay so let's just keep

216
00:08:34,860 --> 00:08:38,940
you know moving along this enable line

217
00:08:36,839 --> 00:08:42,779
to find more commands we've got a fast

218
00:08:38,940 --> 00:08:46,620
read 80. we've got a fast read 100. we

219
00:08:42,779 --> 00:08:48,180
keep going fast read 140 and then now we

220
00:08:46,620 --> 00:08:50,279
all of a sudden have something that

221
00:08:48,180 --> 00:08:53,640
can't be interpreted so this just says

222
00:08:50,279 --> 00:08:56,519
187 and if we look at this first opcode

223
00:08:53,640 --> 00:08:59,120
byte it says that it is BB so let's look

224
00:08:56,519 --> 00:08:59,120
up BB

225
00:08:59,160 --> 00:09:05,880
and that appears to be a fast read dual

226
00:09:02,060 --> 00:09:08,279
so fast redual this is basically going

227
00:09:05,880 --> 00:09:11,100
to now be using two lines in parallel

228
00:09:08,279 --> 00:09:13,980
for data out as well as for the address

229
00:09:11,100 --> 00:09:17,160
in so there's an instruction BB and then

230
00:09:13,980 --> 00:09:19,080
the address is not just serially passed

231
00:09:17,160 --> 00:09:24,360
in it's passed in with the most

232
00:09:19,080 --> 00:09:26,459
significant bit in the do or the IO1

233
00:09:24,360 --> 00:09:28,200
line so this is no longer just data out

234
00:09:26,459 --> 00:09:30,600
this is no longer just data and these

235
00:09:28,200 --> 00:09:32,700
are basically being used in parallel so

236
00:09:30,600 --> 00:09:34,800
the most significant bit here the next

237
00:09:32,700 --> 00:09:36,779
most significant bit there so the

238
00:09:34,800 --> 00:09:39,839
address comes in it's still 24-bit

239
00:09:36,779 --> 00:09:42,420
address but it comes in in parallel and

240
00:09:39,839 --> 00:09:44,220
then furthermore the data out is going

241
00:09:42,420 --> 00:09:47,880
to come out in parallel with the most

242
00:09:44,220 --> 00:09:50,519
significant bit in the data out IO1 and

243
00:09:47,880 --> 00:09:52,860
the next most significant in IO0 then

244
00:09:50,519 --> 00:09:54,959
the next most significant in IO1 and so

245
00:09:52,860 --> 00:09:58,080
it's bouncing back and forth like that

246
00:09:54,959 --> 00:10:00,120
so this simple SPI flash analyzer can't

247
00:09:58,080 --> 00:10:01,860
properly analyze it now there is

248
00:10:00,120 --> 00:10:03,899
supposedly an option where if you add

249
00:10:01,860 --> 00:10:05,700
like a parallel interpretation and then

250
00:10:03,899 --> 00:10:07,080
you set this by flash analyzer to

251
00:10:05,700 --> 00:10:09,420
analyze the parallel interpretation

252
00:10:07,080 --> 00:10:11,880
instead of these by theoretically you

253
00:10:09,420 --> 00:10:13,740
know it will recognize the app code uh

254
00:10:11,880 --> 00:10:16,019
in practice on this particular system it

255
00:10:13,740 --> 00:10:18,420
didn't for dual SPI but it did for Quad

256
00:10:16,019 --> 00:10:20,220
 SPI anyways the point is the the

257
00:10:18,420 --> 00:10:22,019
interpretation is actually incorrect so

258
00:10:20,220 --> 00:10:24,000
this this plugin is not correctly

259
00:10:22,019 --> 00:10:26,399
interpreting it and that's why we needed

260
00:10:24,000 --> 00:10:29,100
this quad SPI plugin in order to

261
00:10:26,399 --> 00:10:30,959
correctly interpret things so now we're

262
00:10:29,100 --> 00:10:33,480
going to go ahead and add an analyzer

263
00:10:30,959 --> 00:10:35,160
and do the QSPI

264
00:10:33,480 --> 00:10:37,440
we have that it asks us some things

265
00:10:35,160 --> 00:10:39,480
what's the enable Okay so that was our

266
00:10:37,440 --> 00:10:42,959
chip select what's the clock that's our

267
00:10:39,480 --> 00:10:44,480
clock what's DQ0 so it's a quad SPI so

268
00:10:42,959 --> 00:10:47,279
it's calling them

269
00:10:44,480 --> 00:10:49,920
dq0123 so data quad and we're going to

270
00:10:47,279 --> 00:10:53,339
say that's our IO0 this is going to be

271
00:10:49,920 --> 00:10:56,779
our IO1 this is going to be our IO2 and

272
00:10:53,339 --> 00:10:56,779
this is going to be our IO3

273
00:10:57,000 --> 00:11:00,660
it's going to ask us about our clock

274
00:10:58,500 --> 00:11:01,980
polarity we can just assume the default

275
00:11:00,660 --> 00:11:03,720
for now unless the data looks like

276
00:11:01,980 --> 00:11:05,820
garbage it's going to ask us about the

277
00:11:03,720 --> 00:11:08,040
 SPI mode and there's extended dual and

278
00:11:05,820 --> 00:11:09,600
quad extended I believe means that it

279
00:11:08,040 --> 00:11:11,640
can be interpreted as either dual or

280
00:11:09,600 --> 00:11:13,200
quad whereas if you force it into dual

281
00:11:11,640 --> 00:11:14,820
or force it into quad it can only

282
00:11:13,200 --> 00:11:17,220
interpret as one of those so we're going

283
00:11:14,820 --> 00:11:20,100
to leave it as extended then it asks the

284
00:11:17,220 --> 00:11:21,720
number of clock dummy cycles so for that

285
00:11:20,100 --> 00:11:23,339
we're going to want to go back and you

286
00:11:21,720 --> 00:11:25,440
know look at the the type of commands

287
00:11:23,339 --> 00:11:27,300
that we're looking for if we look at the

288
00:11:25,440 --> 00:11:28,980
manual and we read the description here

289
00:11:27,300 --> 00:11:31,019
it's going to say that this is

290
00:11:28,980 --> 00:11:33,839
accomplished by adding four dummy clock

291
00:11:31,019 --> 00:11:35,339
 cycles after the 24-bit address so for

292
00:11:33,839 --> 00:11:37,560
this particular chip for this particular

293
00:11:35,339 --> 00:11:39,180
command it's expecting four dummy cycles

294
00:11:37,560 --> 00:11:41,459
different chips can have different

295
00:11:39,180 --> 00:11:44,279
number of dummy cycles as described on

296
00:11:41,459 --> 00:11:47,519
the website so we're going to set that

297
00:11:44,279 --> 00:11:49,740
to 4 and then the address size is 3 3

298
00:11:47,519 --> 00:11:52,260
bytes 24 bits and we're going to save

299
00:11:49,740 --> 00:11:54,240
and let that start interpreting things

300
00:11:52,260 --> 00:11:56,339
so now you can see that it added this

301
00:11:54,240 --> 00:11:58,740
right here and it's saying command BB is

302
00:11:56,339 --> 00:12:00,480
a dual IO fast read it's going to

303
00:11:58,740 --> 00:12:03,600
interpret the address right here and

304
00:12:00,480 --> 00:12:06,779
that's going to be address 1000 then we

305
00:12:03,600 --> 00:12:10,620
have four dummy clock cycles followed by

306
00:12:06,779 --> 00:12:12,300
the data coming in in parallel here

307
00:12:10,620 --> 00:12:14,040
so again if we went back and looked at

308
00:12:12,300 --> 00:12:17,220
the interpretation we would see that the

309
00:12:14,040 --> 00:12:19,740
most significant bit is on IO1 and lease

310
00:12:17,220 --> 00:12:21,839
and the next most on IO0 so if we wanted

311
00:12:19,740 --> 00:12:24,839
to just kind of eyeball that we'd say on

312
00:12:21,839 --> 00:12:27,000
the rising edge here this IO1 is the

313
00:12:24,839 --> 00:12:30,360
most significant bit so one and then

314
00:12:27,000 --> 00:12:33,240
this is a zero so one zero one zero

315
00:12:30,360 --> 00:12:36,360
that's going to be a and then again 1 0

316
00:12:33,240 --> 00:12:38,640
1 0 that's going to be a all right and

317
00:12:36,360 --> 00:12:40,560
again we can use our SPI flash dump in

318
00:12:38,640 --> 00:12:44,519
order to sanity check that this data at

319
00:12:40,560 --> 00:12:49,019
1000 is aa55 so let's go ahead and click

320
00:12:44,519 --> 00:12:52,160
over there go to offset 1000. and we see

321
00:12:49,019 --> 00:12:55,860
that indeed it is

322
00:12:52,160 --> 00:12:58,440
aa55000d a five five zero zero zero zero

323
00:12:55,860 --> 00:13:00,660
D so the data looks the same to me so

324
00:12:58,440 --> 00:13:02,579
you can see that the QSPI is going to

325
00:13:00,660 --> 00:13:04,560
take a while to chew through the data

326
00:13:02,579 --> 00:13:07,560
and give us the correct interpretation

327
00:13:04,560 --> 00:13:09,899
so go ahead and let that complete before

328
00:13:07,560 --> 00:13:12,480
you move on to the next section about

329
00:13:09,899 --> 00:13:15,660
exporting the data in a way that you can

330
00:13:12,480 --> 00:13:18,240
use this data to graph a view of what

331
00:13:15,660 --> 00:13:21,920
sort of access is the firmware is making

332
00:13:18,240 --> 00:13:21,920
to the SPI flash at Bhutan

