1
00:00:00,240 --> 00:00:05,400
once the QSPI data is done processing

2
00:00:03,179 --> 00:00:08,280
then we need to format the information

3
00:00:05,400 --> 00:00:11,639
here and then export it so you want to

4
00:00:08,280 --> 00:00:14,460
right click on this area which has the

5
00:00:11,639 --> 00:00:16,260
column headings and you want to select

6
00:00:14,460 --> 00:00:18,060
only the value

7
00:00:16,260 --> 00:00:20,039
so unselect everything and leave only

8
00:00:18,060 --> 00:00:22,080
value selected

9
00:00:20,039 --> 00:00:25,080
so when we do that then we can see that

10
00:00:22,080 --> 00:00:27,180
the command is listed there additionally

11
00:00:25,080 --> 00:00:29,340
if you want to not see these sort of

12
00:00:27,180 --> 00:00:31,920
empty locations at this point we can

13
00:00:29,340 --> 00:00:34,380
delete the things like the SPI flash

14
00:00:31,920 --> 00:00:36,059
analyzer and the SPI analyzer because

15
00:00:34,380 --> 00:00:37,800
we're not going to need them or be

16
00:00:36,059 --> 00:00:40,680
looking at them anymore and then you can

17
00:00:37,800 --> 00:00:43,079
see that this value is only the QSPI

18
00:00:40,680 --> 00:00:45,300
analysis so once you've got stuff

19
00:00:43,079 --> 00:00:46,920
looking like that then you click on the

20
00:00:45,300 --> 00:00:47,760
three dots here next to the search

21
00:00:46,920 --> 00:00:51,120
window

22
00:00:47,760 --> 00:00:52,739
and you're going to select export table

23
00:00:51,120 --> 00:00:54,660
then it's going to ask you if you want

24
00:00:52,739 --> 00:00:55,739
the visible columns or all columns and

25
00:00:54,660 --> 00:00:58,379
you're going to leave it with the

26
00:00:55,739 --> 00:01:00,899
default of only the visible ones and you

27
00:00:58,379 --> 00:01:03,059
want to export to CSV so all the default

28
00:01:00,899 --> 00:01:05,339
options are okay here and then you go

29
00:01:03,059 --> 00:01:07,799
ahead and hit export and you save the

30
00:01:05,339 --> 00:01:11,760
file I'm going to go ahead and save this

31
00:01:07,799 --> 00:01:15,000
as Saleae UP Squared dual SPI and save as

32
00:01:11,760 --> 00:01:16,860
a CSV file then you hit export and it's

33
00:01:15,000 --> 00:01:19,080
going to take a while to export that

34
00:01:16,860 --> 00:01:21,540
data in the next video we're going to

35
00:01:19,080 --> 00:01:23,400
see how to post-process that data from

36
00:01:21,540 --> 00:01:26,780
this sort of format into something

37
00:01:23,400 --> 00:01:29,520
that's appropriate for graphing

38
00:01:26,780 --> 00:01:31,860
the output file here of course will be

39
00:01:29,520 --> 00:01:34,020
extremely large because you're taking

40
00:01:31,860 --> 00:01:36,720
every single byte of data and turning it

41
00:01:34,020 --> 00:01:39,299
into you know many bytes of ASCII data

42
00:01:36,720 --> 00:01:41,340
that gets exported so as you saw when

43
00:01:39,299 --> 00:01:43,079
the file is done exporting it'll say

44
00:01:41,340 --> 00:01:45,600
export complete and then this little

45
00:01:43,079 --> 00:01:46,799
status box will disappear down there now

46
00:01:45,600 --> 00:01:49,520
you're ready to move on to

47
00:01:46,799 --> 00:01:49,520
post-processing

