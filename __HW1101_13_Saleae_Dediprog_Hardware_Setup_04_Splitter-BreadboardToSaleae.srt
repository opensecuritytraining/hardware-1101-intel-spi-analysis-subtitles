1
00:00:00,000 --> 00:00:05,160
so now I've got everything connected up

2
00:00:01,979 --> 00:00:08,040
I've got my Dediprog connected to the

3
00:00:05,160 --> 00:00:09,720
breadboard connected to a known good SPI

4
00:00:08,040 --> 00:00:11,940
flash chip that I know I can read with

5
00:00:09,720 --> 00:00:13,559
the ready proc and I was able to confirm

6
00:00:11,940 --> 00:00:14,880
that the Dediprog can still read this

7
00:00:13,559 --> 00:00:16,859
which means all my electrical

8
00:00:14,880 --> 00:00:19,020
connections are intact which is a good

9
00:00:16,859 --> 00:00:20,699
thing to sanity check every now and then

10
00:00:19,020 --> 00:00:23,039
so the next thing we need to do is we

11
00:00:20,699 --> 00:00:26,519
need to add in the Saleae logic analyzer

12
00:00:23,039 --> 00:00:28,560
to see what's going on with this correct

13
00:00:26,519 --> 00:00:31,140
successful identification of the chip

14
00:00:28,560 --> 00:00:33,000
And then reading from the chip so I've

15
00:00:31,140 --> 00:00:34,980
got my sailight set up in the same

16
00:00:33,000 --> 00:00:38,940
orientation as used elsewhere in the

17
00:00:34,980 --> 00:00:43,200
class we've got pin we've got wire zero

18
00:00:38,940 --> 00:00:46,140
connector 0 here so it goes zero one two

19
00:00:43,200 --> 00:00:48,480
three four five six seven Etc so

20
00:00:46,140 --> 00:00:51,120
basically I'm going to again skip the

21
00:00:48,480 --> 00:00:53,820
use of 0 and I'm going to have this pin

22
00:00:51,120 --> 00:00:56,579
1 connected to our sorry connector one

23
00:00:53,820 --> 00:00:58,620
connected to pin one on the SPI flash

24
00:00:56,579 --> 00:00:59,760
chip now there's two ways you can go

25
00:00:58,620 --> 00:01:03,420
about this

26
00:00:59,760 --> 00:01:05,339
either a you can use the probes that

27
00:01:03,420 --> 00:01:07,760
come with the Saleae little micro

28
00:01:05,339 --> 00:01:10,799
Grabber probes and you can essentially

29
00:01:07,760 --> 00:01:13,200
slightly lift this thing up so that it's

30
00:01:10,799 --> 00:01:15,000
still plugged in but it's just out

31
00:01:13,200 --> 00:01:18,000
enough that you can use the little

32
00:01:15,000 --> 00:01:21,659
Grabber to go ahead and grab onto that

33
00:01:18,000 --> 00:01:24,000
now this way is nice in the sense of

34
00:01:21,659 --> 00:01:26,100
it's quick and easy to connect up but

35
00:01:24,000 --> 00:01:28,320
it's not necessarily going to yield to

36
00:01:26,100 --> 00:01:30,000
the best electrical connections and kind

37
00:01:28,320 --> 00:01:32,340
of defeats the point of us having used

38
00:01:30,000 --> 00:01:34,740
this breadboard in the first place so if

39
00:01:32,340 --> 00:01:37,020
we want to be reliable about things then

40
00:01:34,740 --> 00:01:38,340
instead we're going to have to use a

41
00:01:37,020 --> 00:01:40,500
dedicated wire

42
00:01:38,340 --> 00:01:43,140
and so for instance we would connect

43
00:01:40,500 --> 00:01:45,119
this male to male wire here to this

44
00:01:43,140 --> 00:01:46,860
probe and then we would connect it on

45
00:01:45,119 --> 00:01:49,140
the breadboard to this outside edge that

46
00:01:46,860 --> 00:01:51,060
I left open so that now it'll have a

47
00:01:49,140 --> 00:01:53,399
nice strong electrical connection

48
00:01:51,060 --> 00:01:55,799
so you can choose to do it however you

49
00:01:53,399 --> 00:01:58,140
want to do I'm going to use these

50
00:01:55,799 --> 00:02:00,780
electrical these strong you know the the

51
00:01:58,140 --> 00:02:02,399
more likely to be reliable electrical

52
00:02:00,780 --> 00:02:04,079
connections but you can try the probes

53
00:02:02,399 --> 00:02:05,820
as well and see if that works for you so

54
00:02:04,079 --> 00:02:08,340
now you should basically take all of

55
00:02:05,820 --> 00:02:10,500
these connections from the Saleae to the

56
00:02:08,340 --> 00:02:12,000
breadboard and make all of those

57
00:02:10,500 --> 00:02:13,440
connections on your own so that

58
00:02:12,000 --> 00:02:15,000
everything is connected we've got the

59
00:02:13,440 --> 00:02:16,920
Saleae and the Dediprog connected to

60
00:02:15,000 --> 00:02:19,220
the breadboard connected to the

61
00:02:16,920 --> 00:02:19,220
motherboard

